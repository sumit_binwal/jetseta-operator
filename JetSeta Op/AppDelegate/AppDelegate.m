//
//  AppDelegate.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 12/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeVC.h"
#import "CharterListVC.h"
#import "TutorialVC.h"
#import "AppLocationManager.h"
#import "NotificationVC.h"
#import "AircraftListVC.h"
#import "LoginVC.h"
#import <GoogleMaps/GMSServices.h>
#import "MainMenuLeftVC.h"
#import "SWRevealViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "UpdateCharterVC.h"
#import "AddCharterVC.h"


#define GOOGLE_PLACE_API @"AIzaSyCVG30je8jlJu77HW5m9U2cFEThXpBYg3s"
@interface AppDelegate ()<SWRevealViewControllerDelegate>
{
    
}
@end

@implementation AppDelegate
@synthesize revealController,arrCountryCode,tabVC2;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [Fabric with:@[[Crashlytics class]]];

    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    [CommonFunction registerForRemoteNotification];
    [self updateUserPresentLocation];
    

    if ([NSUSERDEFAULTS objectForKey:kTUTORIALVISITED]) {
        if ([NSUSERDEFAULTS valueForKey:kUSERTOKEN])
        {
            [self changeRootViewController:YES aircraftPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue] charterPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_CHARTER]boolValue]];
        }
        else
        {
            [self changeRootViewController:NO aircraftPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue] charterPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_CHARTER]boolValue]];
        }
    }
    else
        
    {
        TutorialVC *tutVC = [[TutorialVC alloc]initWithNibName:NSStringFromClass([TutorialVC class]) bundle:nil];
        
        self.navigationController = [[UINavigationController alloc]initWithRootViewController:tutVC];
        
        [self.window setRootViewController:self.navigationController];
       // [self.window setBackgroundColor:[UIColor clearColor]];
        
    }
    //setup location manager...
    [self setUpLocationManager];
    //setup Google PlacePicker Setup.
    [self setNavigationProperties];
    
    
    [GMSServices provideAPIKey:GOOGLE_PLACE_API];

    [self.window setBackgroundColor:[UIColor clearColor]];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


-(void)changeRootViewController:(BOOL)isTabBar aircraftPublish:(BOOL)aircraft charterPublish:(BOOL)charter
{
    
    if (isTabBar)
    {
        
        if (self.window.rootViewController)
        {
            [self.navigationController popToRootViewControllerAnimated:NO];
            self.navigationController = nil;
            
        }
        
//        HomeVC *homeVC = [[HomeVC alloc]initWithNibName:NSStringFromClass([HomeVC class]) bundle:nil];
//        
//        homeVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
//        
        
        
        CharterListVC *clvc = [[CharterListVC alloc]initWithNibName:NSStringFromClass([CharterListVC class]) bundle:nil];
    
        
        
        clvc.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        
        
        NotificationVC *nfvc = [[NotificationVC alloc]initWithNibName:NSStringFromClass([NotificationVC class]) bundle:nil];
        
        nfvc.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        
        AircraftListVC *userVC = [[AircraftListVC alloc]initWithNibName:NSStringFromClass([AircraftListVC class]) bundle:nil];
        userVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        

            
        

//        
//        MainMenuLeftVC *leftDeck = [[MainMenuLeftVC alloc] initWithNibName:@"MainMenuLeftVC" bundle:nil];
//        
//        UINavigationController *leftDeckNavCon = [[UINavigationController alloc]initWithRootViewController:leftDeck];
//        SWRevealViewController *revealCon = [[SWRevealViewController alloc] initWithRearViewController:leftDeckNavCon frontViewController:tabVC1];
//        revealCon.delegate = self;
//        revealController = revealCon;
//        revealController.rearViewRevealWidth = 230.0f*SCREEN_XScale;

        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            clvc.tabBarItem.image = [CommonFunction imageResize:[UIImage imageNamed:@"TB_Home"] andResizeTo:CGSizeMake(25, 25)];
            clvc.tabBarItem.selectedImage = [CommonFunction imageResize:[UIImage imageNamed:@"TB_HomeActive"] andResizeTo:CGSizeMake(25, 25)];
            
            nfvc.tabBarItem.image = [CommonFunction imageResize:[UIImage imageNamed:@"TB_Notification"] andResizeTo:CGSizeMake(25, 25)];
            ;
            nfvc.tabBarItem.selectedImage = [CommonFunction imageResize:[UIImage imageNamed:@"TB_NotificationActive"] andResizeTo:CGSizeMake(25, 25)];
            ;
            userVC.tabBarItem.image = [CommonFunction imageResize:[UIImage imageNamed:@"TB_MyProfile"] andResizeTo:CGSizeMake(25, 25)];
            ;
            userVC.tabBarItem.selectedImage = [CommonFunction imageResize:[UIImage imageNamed:@"TB_MyProfileActive"] andResizeTo:CGSizeMake(25, 25)];
            ;
            
        }
        else
        {
            clvc.tabBarItem.image = [UIImage imageNamed:@"TB_Home"];
            clvc.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_HomeActive"];
            nfvc.tabBarItem.image = [UIImage imageNamed:@"TB_Notification"];
            nfvc.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_NotificationActive"];
            userVC.tabBarItem.image = [UIImage imageNamed:@"TB_MyProfile"];
            userVC.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_MyProfileActive"];
            
        }
        clvc.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        UINavigationController *tabVC1 = [[UINavigationController alloc]initWithRootViewController:clvc];
        tabVC2 = [[UINavigationController alloc]initWithRootViewController:nfvc];
        UINavigationController *tabVC3 = [[UINavigationController alloc]initWithRootViewController:userVC];
        tabVC1.navigationBarHidden = YES;
        tabVC2.navigationBarHidden = YES;
        tabVC3.navigationBarHidden = YES;
        
        self.mainTapBarCntroller = [[UITabBarController alloc]init];
        self.mainTapBarCntroller.tabBar.backgroundColor=[UIColor whiteColor];
        self.mainTapBarCntroller.tabBar.translucent = YES;
        
        [self.mainTapBarCntroller setViewControllers:@[tabVC1,tabVC2,tabVC3]];
        
        
        
        //make left view controller.
        //make left view controller.
        MainMenuLeftVC *leftDeck = [[MainMenuLeftVC alloc] initWithNibName:@"MainMenuLeftVC" bundle:nil];
        
        UINavigationController *leftDeckNavCon = [[UINavigationController alloc]initWithRootViewController:leftDeck];
        
        
        SWRevealViewController *revealCon = [[SWRevealViewController alloc] initWithRearViewController:leftDeckNavCon frontViewController:self.mainTapBarCntroller];
        revealCon.delegate = self;
        
        self.revealController = revealCon;
        

        self.revealController.rearViewRevealWidth = 230.0f*SCREEN_XScale;

        self.window.rootViewController = self.revealController;
        
        
        
        if (aircraft) {
        [self.mainTapBarCntroller setSelectedIndex:2];
        }
        else
        {
        [self.mainTapBarCntroller setSelectedIndex:0];
        }
    }
    else
    {
        if (APPDELEGATE.window.rootViewController)
        {
            for (NSInteger i=0; i<APPDELEGATE.mainTapBarCntroller.viewControllers.count; i++)
            {
                UIViewController *controller = APPDELEGATE.mainTapBarCntroller.viewControllers[i];
                controller = nil;
            }
            [self.mainTapBarCntroller setViewControllers:nil];
            self.mainTapBarCntroller = nil;
        }
        LoginVC *loginVC = [[LoginVC alloc]initWithNibName:NSStringFromClass([LoginVC class]) bundle:nil];
        
        self.navigationController = [[UINavigationController alloc]initWithRootViewController:loginVC];
        self.navigationController.navigationBarHidden = YES;
        
        [self.navigationController.view setBackgroundColor:[UIColor clearColor]];
        [self.window setRootViewController:self.navigationController];
    }
}

#pragma mark-Register for Device token

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *devicToken = [[[[deviceToken description]
                              stringByReplacingOccurrencesOfString: @"<" withString: @""]
                             stringByReplacingOccurrencesOfString: @">" withString: @""]
                            stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"Device_Token     -----> %@\n",devicToken);
    
    pushDeviceToken=devicToken;

}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UITabBar *tabBar = APPDELEGATE.mainTapBarCntroller.tabBar;
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    [tabBarItem2 setImage:[UIImage imageNamed:@"gray_bell_reddot_icon"]];
    [tabBarItem2 setSelectedImage:[UIImage imageNamed:@"blue_bell_reddot_icon"]];
    
    if (!(application.applicationState==UIApplicationStateActive)) {
    [self.mainTapBarCntroller setSelectedIndex:1];
    }
    
}

#pragma mark - Register Location manager
-(void)updateUserPresentLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestAlwaysAuthorization] ;
    [self.locationManager startUpdatingLocation];
}
-(void)setUpLocationManager
{
    if ([CLLocationManager locationServicesEnabled])
    {
        NSLog(@"globally enabled location service");
        [AppLocationManager sharedInstance];
    }
    else
    {
        NSLog(@"globally disabled location service");
        
        if (/* DISABLES CODE */  (&UIApplicationOpenSettingsURLString) != NULL)
        {
            [self showLocationEnableAlert];
        }
        else
        {
            // Present some dialog telling the user to open the settings app.
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enable location access from device settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
    }
}

-(void)showLocationEnableAlert
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enable location access from device settings. Do you want to open?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"No", nil];
    alert.tag = 111;
    [alert show];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    if (UIInterfaceOrientationPortrait) {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

#pragma mark - Static Method
+(AppDelegate*)getSharedInstance
{
    return (AppDelegate *) [[UIApplication sharedApplication]delegate];
}


-(void)getCountryCodeListingFromServerSide
{
    //http://192.168.0.131:7681/countries_codes
    NSString *url = [NSString stringWithFormat:@"countries_codes"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        if([operation.response statusCode]==200)
        {
            if ([[responseDict objectForKey:@"type"]boolValue])
            {
                if (arrCountryCode)
                {
                    arrCountryCode = nil;
                }
                arrCountryCode = [NSMutableArray array];
                arrCountryCode = [[responseDict objectForKey:@"data"]mutableCopy];
                
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if([operation.response statusCode]  == 400 )
         {
             NSLog(@"impo response %@",operation.response);
             
         }
     }];
}
-(void)setNavigationProperties
{
    UIColor *color = [UIColor colorWithRed:28/255.0 green:126/255.0 blue:231/255.0 alpha:1.0];
    [[UINavigationBar appearance] setBarTintColor:color];
    
    //int imageSize = 20;
    UIImage *myImage = [UIImage imageNamed:@"back_icon"]; //set your backbutton imagename
    UIImage *backButtonImage = [myImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    // now use the new backButtomImage
    [[UINavigationBar appearance] setBackIndicatorImage:backButtonImage];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:backButtonImage];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-1000, -1000) forBarMetrics:UIBarMetricsDefault];
    
   [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:33.0],NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
    }
    else
    {
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:22.0],NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
    }
    
 }

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    // NSLog( @"%@: %@", NSStringFromSelector(_cmd), [self stringFromFrontViewPosition:position]);
    if(position == FrontViewPositionRight) //check the where to move
    {
        UINavigationController *viewController = revealController.frontViewController;
        if ([viewController isKindOfClass:[UITabBarController class]])
        {
            self.mainTapBarCntroller = (UITabBarController *) viewController;
            
            NSArray *arr = self.mainTapBarCntroller.viewControllers;
            UINavigationController *nav = [arr objectAtIndex:self.mainTapBarCntroller.selectedIndex];
            
            if ([nav.visibleViewController isKindOfClass:[UpdateCharterVC class]])
            {
                NSLog(@"hgfhghf");
                [(UpdateCharterVC *)nav.visibleViewController removeKeyboard];
            }
            else if ([nav.visibleViewController isKindOfClass:[AddCharterVC class]])
            {
                NSLog(@"hgfhghf");
                [(AddCharterVC *)nav.visibleViewController removeKeyboard];
            }
        }
        
    }
}

@end
