//
//  CommonFunction.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "CommonFunction.h"
#import "LoginVC.h"
#import "HomeVC.h"
#import "NotificationVC.h"
#import "CharterListVC.h"
#import "AircraftListVC.h"
#import "MainMenuLeftVC.h"
#import "MyProfileVC.h"

@interface CommonFunction ()<SWRevealViewControllerDelegate>
{
    SWRevealViewController *revealController;
}
@end
@implementation CommonFunction

#pragma mark - Hide/Unhide Navigation Bar

+(void) setNavigationBar:(UINavigationController*)navController
{
    //[[UIBarButtonItem appearance] setTintColor:[UIColor clearColor]];
    UINavigationBar *navBar=navController.navigationBar;
    navController.navigationBar.barTintColor=[UIColor colorWithRed:205.0f/255.0f green:4.0f/255.0f blue:121.0f/255.0f alpha:1];
    navBar.tintColor=[UIColor whiteColor];
    
    UIImage *img=[UIImage imageNamed:@"top_navigation_bg"];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-1000, -1000) forBarMetrics:UIBarMetricsDefault];

    [navBar setBackgroundImage:img forBarMetrics:UIBarMetricsDefault];
    [navController setNavigationBarHidden:FALSE];
    if (IPAD) {
        

        
        [navController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:22],NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
        
    }
else
{
    [navController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:18*SCREEN_XScale],NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil]];

}


    [navController.navigationBar setTranslucent:NO];
    
    
    //   UIView *statusView=[[UIView alloc]initWithFrame:CGRectMake(0, -[UIApplication sharedApplication].statusBarFrame.size.height,[UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
    //    [statusView setBackgroundColor:[UIColor colorWithRed:185.0f/255.0f green:0/255.0f blue:36.0f/255.0f alpha:1]];
    //    [navBar addSubview:statusView];
    
}



+(NSString*)convertCustomeDate:(NSString *)currentDateString
{
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:currentDateString];

    NSLocale *deLocale = [NSLocale currentLocale];
    dateFormat.locale=deLocale;
    // Convert date object to desired output format
    
    [dateFormat setDateFormat:@"MMM dd YYYY"];
    currentDateString = [dateFormat stringFromDate:date];
    return currentDateString;
    
//    [dateFormat setDateFormat:@"hh:mm aa"];
//    currentDateString = [dateFormat stringFromDate:date];
//    strTime=currentDateString;

}
+(NSString*)convertCustomeTime:(NSString *)currentDateString
{
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:currentDateString];
    
    NSLocale *deLocale = [NSLocale currentLocale];
    dateFormat.locale=deLocale;
    // Convert date object to desired output format
    
    
        [dateFormat setDateFormat:@"hh:mm aa"];
        currentDateString = [dateFormat stringFromDate:date];
        return currentDateString;
    
}

+(NSDate*)convertDate:(NSString *)currentDateString
{
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:currentDateString];
    
    return date;
}

+(BOOL)isArivalDateIsComesAfterDepartureDate:(NSString *)strArrival departureDate:(NSString *)strDeparture
{
    BOOL isFound = NO;
    
    NSDate *departDate = [CommonFunction convertDate:strDeparture];
    NSDate *arrivalDate = [CommonFunction convertDate:strArrival];
    
    if ([departDate compare:arrivalDate] == NSOrderedDescending)
    {
        NSLog(@"departDate is later than arrivalDate");
        isFound = NO;
        
    }
    else if ([departDate compare:arrivalDate] == NSOrderedAscending)
    {
        NSLog(@"departDate is earlier than arrivalDate");
        isFound = YES;
    }
    else
    {
        NSLog(@"dates are the same");
        isFound = NO;
    }

    return isFound;
}

+(void)hideNavigationBarFromController:(UIViewController *)controller
{
    
    if (controller!=nil) {
        
        [controller.navigationController setNavigationBarHidden:YES];
    }
    else
    {
        //This is going to hide navigation of Screen those are not in tabbarcontroller
        [APPDELEGATE.navigationController setNavigationBarHidden:YES];
    }
}

+(void)unHideNavigationBarFromController:(UIViewController *)controller
{
    if (controller!=nil) {
        
        [controller.navigationController setNavigationBarHidden:NO];
    }
    else
    {
        //This is going to unhide navigation of Screen those are not in tabbarcontroller
        [APPDELEGATE.navigationController setNavigationBarHidden:NO];
    }
}

+(void)changeRootViewController:(BOOL)isTabBar aircraftPublish:(BOOL)aircraft charterPublish:(BOOL)charter
{
    if (isTabBar)
    {
        if (APPDELEGATE.window.rootViewController)
        {
            [APPDELEGATE.navigationController popToRootViewControllerAnimated:NO];
            APPDELEGATE.navigationController = nil;
        }
        
        HomeVC *homeVC = [[HomeVC alloc]initWithNibName:NSStringFromClass([HomeVC class]) bundle:nil];
        homeVC.tabBarItem.image = [UIImage imageNamed:@"TB_Home"];
        homeVC.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_HomeActive"];
        homeVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        
        CharterListVC *clvc = [[CharterListVC alloc]initWithNibName:NSStringFromClass([CharterListVC class]) bundle:nil];
        clvc.tabBarItem.image = [UIImage imageNamed:@"TB_Home"];
        clvc.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_HomeActive"];
        clvc.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        NotificationVC *nfvc = [[NotificationVC alloc]initWithNibName:NSStringFromClass([NotificationVC class]) bundle:nil];
        nfvc.tabBarItem.image = [UIImage imageNamed:@"TB_Notification"];
        nfvc.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_NotificationActive"];
        nfvc.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        
        AircraftListVC *userVC = [[AircraftListVC alloc]initWithNibName:NSStringFromClass([AircraftListVC class]) bundle:nil];
        userVC.tabBarItem.image = [UIImage imageNamed:@"TB_MyProfile"];
        userVC.tabBarItem.selectedImage = [UIImage imageNamed:@"TB_MyProfileActive"];
        userVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
        UINavigationController *tabVC1;
        if (aircraft || charter) {
            tabVC1 = [[UINavigationController alloc]initWithRootViewController:homeVC];
            
           
        }
        else
        {
            tabVC1 = [[UINavigationController alloc]initWithRootViewController:clvc];
        }
        
        UINavigationController *tabVC2 = [[UINavigationController alloc]initWithRootViewController:nfvc];
        UINavigationController *tabVC3 = [[UINavigationController alloc]initWithRootViewController:userVC];
        tabVC1.navigationBarHidden = YES;
        tabVC2.navigationBarHidden = YES;
        tabVC3.navigationBarHidden = YES;
        
        APPDELEGATE.mainTapBarCntroller = [[UITabBarController alloc]init];
        APPDELEGATE.mainTapBarCntroller.tabBar.backgroundColor=[UIColor whiteColor];
        APPDELEGATE.mainTapBarCntroller.tabBar.translucent = NO;
        
        [APPDELEGATE.mainTapBarCntroller setViewControllers:@[tabVC1,tabVC2,tabVC3]];
        
        
        if (APPDELEGATE.window)
        {
            NSLog(@"window alive");
        }
        
        if (APPDELEGATE.mainTapBarCntroller)
        {
            NSLog(@"tabbar alive");
        }
        
        [APPDELEGATE.window setRootViewController:APPDELEGATE.mainTapBarCntroller];
        
        [APPDELEGATE.mainTapBarCntroller setSelectedIndex:0];
        
    }
    else
    {
        if (APPDELEGATE.window.rootViewController)
        {
            for (NSInteger i=0; i<APPDELEGATE.mainTapBarCntroller.viewControllers.count; i++)
            {
                UIViewController *controller = APPDELEGATE.mainTapBarCntroller.viewControllers[i];
                controller = nil;
            }
            [APPDELEGATE.mainTapBarCntroller setViewControllers:nil];
            APPDELEGATE.mainTapBarCntroller = nil;
        }
        
        LoginVC *loginVC = [[LoginVC alloc]initWithNibName:NSStringFromClass([LoginVC class]) bundle:nil];
        
        APPDELEGATE.navigationController = [[UINavigationController alloc]initWithRootViewController:loginVC];
        APPDELEGATE.navigationController.navigationBarHidden = YES;

        
        [APPDELEGATE.navigationController.view setBackgroundColor:[UIColor clearColor]];
        [APPDELEGATE.window setRootViewController:APPDELEGATE.navigationController];
    
    }
    [APPDELEGATE.window setBackgroundColor:[UIColor clearColor]];
        [APPDELEGATE.window makeKeyAndVisible];
    
}

#pragma mark - Register Remote Notification
+(void)registerForRemoteNotification
{
    //This code will work in iOS 8.0 xcode 6.0 or later
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeNewsstandContentAvailability| UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
}
#pragma mark - Check Reachblity
+(BOOL) reachabiltyCheck
{
    
    BOOL status =YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    Reachability * reach = [Reachability reachabilityForInternetConnection];
    
    
    if([reach currentReachabilityStatus]==0)
    {
        status = NO;
        //NSLog(@"network not connected");
    }
    
    reach.reachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            // blockLabel.text = @"Block Says Reachable";
        });
    };
    
    reach.unreachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            //  blockLabel.text = @"Block Says Unreachable";
        });
    };
    
    [reach startNotifier];
    return status;
}

+(BOOL)reachabilityChanged:(NSNotification*)note
{
    BOOL status =YES;
    //NSLog(@"reachabilityChanged");
    
    Reachability * reach = [note object];
    
    if([reach isReachable])
    {
        //notificationLabel.text = @"Notification Says Reachable"
        status = YES;
        //NSLog(@"NetWork is Available");
    }
    else
    {
        status = NO;
        /*
         CustomAlert *alert=[[CustomAlert alloc]initWithTitle:@"There was a small problem" message:@"The network doesn't seem to be responding, please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
         */
    }
    return status;
}



#pragma mark - MBProgressHud Methods
+ (void)removeActivityIndicator {
    [MBProgressHUD hideHUDForView:APPDELEGATE.window animated:YES];
}

+ (void)showActivityIndicatorWithText:(NSString *)text {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:APPDELEGATE.window animated:YES];

//    hud.label.text = @"Loading";
}



+(NSString *) convertTimeStampToDate :(NSDate *) date {
    //hh:mm aa
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale currentLocale];
    [formatter setDateFormat:@"dd MMM YYYY"];
    
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}
+(NSString *) convertTimeStampToTime :(NSDate *) date {
    //hh:mm aa
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale currentLocale];
    [formatter setDateFormat:@"hh:mm aa"];
    
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}


+ (BOOL)isValueNotEmpty:(NSString*)aString{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    aString = [aString stringByTrimmingCharactersInSet:whitespace];
    
    if (aString == nil || [aString length] == 0){
        
        return NO;
    }
    return YES;
}

+ (BOOL)isValidMobileNumber:(NSString*)number
{
    //    number=[number stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //    if ([number length]<5 || [number length]>17) {
    //        return FALSE;
    //    }
    //    NSString *Regex = @"^([0-9]*)$";
    //    NSPredicate *mobileTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex];
    //
    //    return [mobileTest evaluateWithObject:number];
    NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:number];
    
    BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
    return stringIsValid;
}
+(NSString *)trimSpaceInString:(NSString *)mainstr
{
    NSCharacterSet *whitespace=[NSCharacterSet whitespaceAndNewlineCharacterSet];
    mainstr=[mainstr stringByTrimmingCharactersInSet:whitespace];
    mainstr=[mainstr stringByReplacingOccurrencesOfString:@"  " withString:@""];
    return mainstr;
}

+(BOOL)IsValidEmail:(NSString *)checkString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate{
    [[[UIAlertView alloc] initWithTitle:@"Jetseta"
                                message:aMsg
                               delegate:delegate
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil, nil] show];
}

#pragma mark UIAlertView
+ (NSString *)showAlertMsgWIthTextField:(UITextField *)txtFld
{
    NSInteger intVwTag=txtFld.tag;
    NSString *strAlrtMsg;
    switch (intVwTag) {
        case 0:
        {
            if (!txtFld.text.length>0) {
                strAlrtMsg=@"Please select aircraft type.";
                return strAlrtMsg;
                break;
                
            }
        }
        case 1:
        {
                        if (!txtFld.text.length>0) {
            strAlrtMsg=@"Please enter aircraft model.";
            return strAlrtMsg;
            break;
                        }
        }
        case 2:
        {
                        if (!txtFld.text.length>0) {
            strAlrtMsg=@"Please enter aircraft registration registration (No ID).";
            return strAlrtMsg;
            break;
                        }
        }
        case 3:
        {
                        if (!txtFld.text.length>0) {
            strAlrtMsg=@"Please enter manufacturer serial number";
            return strAlrtMsg;
            break;
                        }
        }
        case 4:
        {
                        if (!txtFld.text.length>0) {
            strAlrtMsg=@"Please select aircraft capacity.";
            return strAlrtMsg;
            break;
                        }
        }
        case 5:
        {
                        if (!txtFld.text.length>0) {
            strAlrtMsg=@"Please enter average speed of aircraft.";
            return strAlrtMsg;
            break;
                        }
        }
        case 6:
        {
                        if (!(txtFld.text.length>0)) {
            strAlrtMsg=@"Please enter base location of aircraft.";
            return strAlrtMsg;
            break;
                        }
        }
        case 7:
        {
                        if (!(txtFld.text.length>0)) {
            strAlrtMsg=@"Please enter price per hour.";
            return strAlrtMsg;
            break;
                        }
        }
        case 8:
        {
                        if (!txtFld.text.length>0) {
            strAlrtMsg=@"Please enter aircraft description.";
            return strAlrtMsg;
            break;
                        }
        }
            
    }
    return strAlrtMsg;
}

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController useAsDelegate:(BOOL)isDelegate dismissBlock:(void(^)())dismissBlock
{
    if (NSClassFromString(@"UIAlertController"))
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Jetseta"
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       if (dismissBlock)
                                                       {
                                                           dismissBlock();
                                                       }
                                                       [alertController dismissViewControllerAnimated:YES completion:^{
                                                       }];
                                                       
                                                   }];
        
        [alertController addAction:ok];
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        if (isDelegate)
        {
            [[[UIAlertView alloc]initWithTitle:title message:message delegate:viewController cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        else
        {
            [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
    }
}



+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg BtnTitle:(NSString*)btnName withDelegate:(id)delegate{
    [[[UIAlertView alloc] initWithTitle:@"Jetseta"
                                message:aMsg
                               delegate:delegate
                      cancelButtonTitle:btnName
                      otherButtonTitles:nil, nil] show];
}
+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg{
    [self alertTitle:@"Jetseta" withMessage:aMsg withDelegate:self];
}

+ (void)alertTitle:(NSString*)aTitle withMessage:(NSString*)aMsg withDelegate:(id)delegate withTag:(int)tag{
    UIAlertView *alrtVw=[[UIAlertView alloc] initWithTitle:@"Jetseta"
                                                   message:aMsg
                                                  delegate:delegate
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil, nil];
    
    alrtVw.tag=tag;
    [alrtVw show];
    
    
}

#pragma mark - Make UIView Rounded

+(void)setRoundedView:(UIView *)roundedView toDiameter:(float)newSize;
{
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
}


#pragma mark -
#pragma mark Resize Image
+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
    CGFloat scale = [[UIScreen mainScreen]scale];
    /*You can remove the below comment if you dont want to scale the image in retina   device .Dont forget to comment UIGraphicsBeginImageContextWithOptions*/
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark - Create Giff


+(NSURL *)createGiffFromImageArray:(NSArray *)imageArray andDelayTime:(CGFloat)delayTime
{
    NSDictionary *fileProperties = @{
                                     (__bridge id)kCGImagePropertyGIFDictionary: @{
                                             (__bridge id)kCGImagePropertyGIFLoopCount: @0, // 0 means loop forever
                                             }
                                     };
    
    NSDictionary *frameProperties = @{
                                      (__bridge id)kCGImagePropertyGIFDictionary: @{
                                              (__bridge id)kCGImagePropertyGIFDelayTime: [NSNumber numberWithFloat:delayTime], // a float (not double!) in seconds, rounded to centiseconds in the GIF data
                                              }
                                      };
    
    
    // NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
    
    [self removeDirectories];
    [self createFolder];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
    
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",kAPPFOLDERNAME]];
    
    NSString *fullPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"giffplug.gif"]]; //add our image to the path
    
    
    
    //  NSURL  *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil];
    NSURL   *fileURL = [NSURL fileURLWithPath:fullPath isDirectory:NO];
    
    CGImageDestinationRef destination = CGImageDestinationCreateWithURL((__bridge CFURLRef)fileURL, kUTTypeGIF, imageArray.count, NULL);
    
    for (NSDictionary *dict in imageArray) {
        UIImage *img = dict[@"image"];
        
        CFDictionaryRef options = (__bridge CFDictionaryRef) @{
                                                               (id) kCGImageSourceCreateThumbnailWithTransform : @YES,
                                                               (id) kCGImageSourceCreateThumbnailFromImageAlways : @YES,
                                                               (id) kCGImageSourceThumbnailMaxPixelSize : @(1269),
                                                               (id) kCGImageSourceShouldCache : [NSNumber numberWithBool:false]
                                                               };
        
        CGImageSourceRef src = CGImageSourceCreateWithData((CFDataRef)UIImageJPEGRepresentation(img, 0.8), NULL);
        
        CGImageRef ref = CGImageSourceCreateThumbnailAtIndex(src, 0, options);
        
         CFRelease(src);
        CGImageDestinationAddImage(destination, ref, (CFDictionaryRef)frameProperties);
        CGImageRelease(ref);
    }
    
    CGImageDestinationSetProperties(destination, (__bridge CFDictionaryRef)fileProperties);
    
    
    
    if (!CGImageDestinationFinalize(destination)) {
        CFRelease(destination);
        NSLog(@"failed to finalize image destination");
        return nil;
    }
    CFRelease(destination);
    
    
    
    NSLog(@"url=%@", fileURL);
    return fileURL;
}



+ (void)removeDirectories
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",kAPPFOLDERNAME]];
    
    BOOL isDir;
    if ([[NSFileManager defaultManager]fileExistsAtPath:dataPath isDirectory:&isDir])
    {
        [[NSFileManager defaultManager]removeItemAtPath:dataPath error:&error];
    }
}

+(void)createFolder
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",kAPPFOLDERNAME]];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    // if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    
}
@end
