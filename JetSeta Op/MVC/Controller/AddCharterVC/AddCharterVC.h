//
//  AddCharterVC.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 14/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AddCharterVC : UIViewController<UITextFieldDelegate>



@property(nonatomic,strong)NSString *strCharterLegID;
@property(nonatomic,strong)NSString *strAircraftName;
@property(nonatomic,strong)NSString *strAircraftId;

-(void)removeKeyboard;
@end
