//
//  AddCharterVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 14/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "AddCharterVC.h"
#import "AddAircraftCell.h"
#import <GoogleMaps/GMSServices.h>
#import <GoogleMaps/GMSPlacePicker.h>
#import "KIPLPickerView.h"
#import "LegListingVC.h"
#import "LoginVC.h"

@interface AddCharterVC ()<UIGestureRecognizerDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,KIPLDelegate,sendCharterID>
{
    IBOutlet UIBarButtonItem *cncleBtnDatePckr;
    IBOutlet UIBarButtonItem *dnBtnDatePckr;
    IBOutlet UIBarButtonItem *cncleBtn;
    IBOutlet UITableView *tblVw;
    IBOutlet UIBarButtonItem *dnBtn;
    NSMutableDictionary *dictParam;
    NSMutableArray *arrTitles;
    IBOutlet UITableView *tbleAirportVw;
    
    IBOutlet UIView *vwToolbar;
    
    UITextField *currentActiveTextField;
    
    NSString *strDate;
    NSString *strTime;
    
    NSString *strArrivingDate;
    NSString *strArrivingTime;
    
    
    NSString *strSouceAirportName;
    NSString *strSouceAirportID;
    
    NSString *strDestinationAirportName;
    NSString *strDestinationAirportID;
    
    //Get Selected Cell IndexPath For Source n Destination Cell
    NSInteger selectedCellIndexpath;
    
    //Flag for Arrival DateTime Text Field Is Visible or Not
    BOOL isArrivalTextFldIsVisible;
    
    NSMutableArray *arrAircraft;
    NSMutableArray *arrCharterType;
    
    IBOutlet UIView *vwDate;
    IBOutlet UIImageView *imgAvailBg;
    NSDate *dtArrivalCriteria;
    
    
    //Status Flag For Avaible n NotAvaible
    NSString *strStatusFlag;
    
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIView *vwSliderBg;
    IBOutlet UIButton *btnAvailUnavail;
    NSString *strAirCraftTxt;
    NSString *strAirCraftID;
    NSString *strCharterType;
    NSString *strCharterTypeID;
    IBOutlet UIView *vwDateCntainer;
    NSMutableArray *arrAirportList;
    
    NSString *sourceLatitude;
    NSString *destinationLatitude;
    
    UIBarButtonItem *barBtn;
    
    
    UIPickerView *pickerView;
    UIDatePicker *datePickerIpad;
    UIDatePicker *datePickerTime;
    
}
@property(strong,nonatomic)GMSPlacePicker *placePicker;
@end

@implementation AddCharterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if(IPAD) {
        pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 320, 206)];
        pickerView.delegate = self;
        pickerView.dataSource = self;
        [btnAvailUnavail.titleLabel setFont:[UIFont fontWithName:btnAvailUnavail.titleLabel.font.fontName size:28.0f]];
        datePickerIpad = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 44, 320, 206)];
        datePickerIpad.datePickerMode=UIDatePickerModeDate;
        datePickerIpad.minimumDate=[NSDate date];
        
        datePickerTime = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 44, 320, 206)];
        datePickerTime.datePickerMode=UIDatePickerModeTime;
    }
    
    [self setUpView];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)setUpView
{
    
    [CommonFunction setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Add Charter"];
    
    btnAvailUnavail.frame = CGRectMake(imgAvailBg.frame.origin.x, imgAvailBg.frame.origin.y, btnAvailUnavail.frame.size.width, btnAvailUnavail.frame.size.height);
    
    UIBarButtonItem *barBtn1=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_icon"] style:UIBarButtonItemStyleDone target:self action:@selector(leftButtonClicked:)];
    
    [self.navigationItem setLeftBarButtonItem:barBtn1];
    
    barBtn =[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"AC_imgCheck"] style:UIBarButtonItemStyleDone target:self action:@selector(rightBtnClicked:)];
    
    [self.navigationItem setRightBarButtonItem:barBtn];
    
    //Initially Set Status As 0 Not Avaible
    strStatusFlag=@"1";
    
    //Fill Titles Array Value
    arrTitles = [[NSMutableArray alloc]initWithObjects:@"Select your aircraft",@"Select type of charter",@"Enter departure location of aircraft",@"Enter destination",@"Select date of departure",@"Enter time of departure",@"Enter price per hour", nil];
    
    dictParam=[[NSMutableDictionary alloc]init];
    
    
    //Register Tap Gesture
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapClicked:)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textfieldDidChange) name:UITextFieldTextDidChangeNotification object:nil];
    
}

-(void)viewWillAppear:(BOOL)animate
{
    
    [dnBtn setTintColor:[UIColor blueColor]];
    [cncleBtn setTintColor:[UIColor blueColor]];
    [dnBtnDatePckr setTintColor:[UIColor blueColor]];
    [cncleBtnDatePckr setTintColor:[UIColor blueColor]];
    
    [super viewWillAppear:YES];
    if ([self.strCharterLegID intValue]>0) {
        strAirCraftID=[NSString stringWithFormat:@"%@",self.strAircraftId];
        [dictParam setObject:strAirCraftID forKey:@"aircraft_id"];
        
    }
    [tblVw reloadData];
    
    [APPDELEGATE.mainTapBarCntroller.tabBar setHidden:YES];
    [self getAircraftAPI];
    [self getCharterType];
    [self registerForKeyboardNotifications];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [APPDELEGATE.mainTapBarCntroller.tabBar setHidden:NO];
    [dictParam removeAllObjects];
    
    strDate=@"";
    strTime=@"";
    strSouceAirportName=@"";
    strSouceAirportID=@"";
    strDestinationAirportName=@"";
    strDestinationAirportID=@"";
    selectedCellIndexpath=@"";
    strStatusFlag=@"1";
    strAirCraftTxt=@"";
    strAirCraftID=@"";
    strCharterType=@"";
    strCharterTypeID=@"";
    
    
    
    [self unregisterForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -
#pragma mark UIPickerView Delegate & Datasource
-(void)kiplPickerViewDidDismissWithSelectedRow:(NSInteger)selectedRow PickerVwTag:(NSInteger)tag
{
    if (tag==1) {
        NSString *storeName=[NSString stringWithFormat:@"%@",arrCharterType[selectedRow][@"name"]];
        strCharterType = storeName;
        if ([strCharterType isEqualToString:@"Full Charter"]) {
            isArrivalTextFldIsVisible=true;
            arrTitles = [[NSMutableArray alloc]initWithObjects:@"Select your aircraft",@"Select type of charter",@"Enter departure location of aircraft",@"Enter destination",@"Select date of departure",@"Enter time of departure",@"Select date of Return",@"Enter time of Return",@"Enter price per hour", nil];
            [tblVw reloadData];
        }
        else
        {
            isArrivalTextFldIsVisible=false;
            arrTitles = [[NSMutableArray alloc]initWithObjects:@"Select your aircraft",@"Select type of charter",@"Enter departure location of aircraft",@"Enter destination",@"Select date of departure",@"Enter time of departure",@"Enter price per hour", nil];
            [tblVw reloadData];
        }
        strCharterTypeID=[NSString stringWithFormat:@"%@",arrCharterType[selectedRow][@"id"]];
        [dictParam setObject:strCharterTypeID forKey:@"charter_type"];
        [tblVw reloadData];
    }
    else
    {
        NSString *storeName=[NSString stringWithFormat:@"%@",arrAircraft[selectedRow][@"name"]];
        strAirCraftTxt = storeName;
        strAirCraftID = [NSString stringWithFormat:@"%@",arrAircraft[selectedRow][@"id"]];
        [dictParam setObject:strAirCraftID forKey:@"aircraft_id"];
        
        [tblVw reloadData];
    }
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag==0) {
        return arrAircraft.count;
    }
    else
    {
        return arrCharterType.count;
    }
    
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag==0) {
        NSString *storeName=[NSString stringWithFormat:@"%@",arrAircraft[row][@"name"]];
        return storeName;
    }
    else
    {
        NSString *storeName=[NSString stringWithFormat:@"%@",arrCharterType[row][@"name"]];
        return storeName;
        
    }
}

#pragma mark - UIGesture Delegate Method
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    NSLog(@"%@",NSStringFromClass([touch.view class]));
    return [NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"];
}


#pragma mark - TextFieldValidate
-(BOOL)validateTextField
{
    NSString *strDateTime=[NSString stringWithFormat:@"%@",dictParam[@"departure_datetime"]];
    NSString *strArrivingDateTime=[NSString stringWithFormat:@"%@",dictParam[@"arrival_datetime"]];
//    NSString *strManufacturer=[NSString stringWithFormat:@"%@",dictParam[@"manufacturer"]];
//    NSString *strName=[NSString stringWithFormat:@"%@",dictParam[@"name"]];
//    NSString *strRegID=[NSString stringWithFormat:@"%@",dictParam[@"registration_id"]];
//    NSString *strCapacity=[NSString stringWithFormat:@"%@",dictParam[@"capacity"]];
//    NSString *strSpeed=[NSString stringWithFormat:@"%@",dictParam[@"speed"]];
//    NSString *strDescription=[NSString stringWithFormat:@"%@",dictParam[@"description"]];
    NSString *strPrice=[NSString stringWithFormat:@"%@",dictParam[@"price_per_hour"]];
    NSLog(@"%@",self.strCharterLegID);
    if (!dictParam[@"aircraft_id"]||strAirCraftID.length==0)
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please select aircraft." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    else if (!dictParam[@"charter_type"]||(strCharterTypeID.length==0))
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please select charter type." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    else if ((!dictParam[@"source_airport"]||(strSouceAirportID.length==0))&&(!dictParam[@"source_timezone_latitude"]))
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please select departure airport." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    else  if ((!dictParam[@"destination_airport"]||(strDestinationAirportID.length==0))&&(!dictParam[@"destination_timezone_latitude"]))
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please select destination airport." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    else if([dictParam[@"destination_airport"]isEqualToString:dictParam[@"source_airport"]] && ([dictParam objectForKey:@"destination_airport"] || [dictParam objectForKey:@"source_airport"]))
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please select different destination location." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return NO;
    }
    else if((!([dictParam objectForKey:@"destination_airport"] || [dictParam objectForKey:@"source_airport"]))   &&    (([dictParam[@"source_timezone_latitude"]isEqualToString:dictParam[@"destination_timezone_latitude"]]) && ([dictParam[@"source_timezone_longitude"]isEqualToString:dictParam[@"destination_timezone_longitude"]])))
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please select different destination location." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return NO;
    }
    else if (!dictParam[@"departure_datetime"]||(strDateTime.length==0))
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please select departure date and time of charter." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    else  if ((!dictParam[@"arrival_datetime"]||(strArrivingDateTime.length==0))&&isArrivalTextFldIsVisible)
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please select Return date and time of charter." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    else if (!dictParam[@"price_per_hour"]||(strPrice.length==0))
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please enter price per hour." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    
    return YES;
}

#pragma mark - UserDefine Method
-(void)datePickerValueChanged:(UIDatePicker *)datePicker1
{
    if (selectedCellIndexpath==4||selectedCellIndexpath==6) {
        if (selectedCellIndexpath==4) {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"MMM dd YYYY"];
            NSString *date = [dateFormat stringFromDate:datePicker1.date];
            strDate=date;
            dtArrivalCriteria=datePicker1.date;
            [tblVw reloadData];
        }
        else
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"MMM dd YYYY"];
            NSString *date = [dateFormat stringFromDate:datePicker1.date];
            
            strArrivingDate=date;
            [tblVw reloadData];
        }
    } else {
        if (selectedCellIndexpath==5||selectedCellIndexpath==7) {
            if (selectedCellIndexpath==5) {
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"hh:mm aa"];
                NSString *date = [dateFormat stringFromDate:datePicker1.date];
                strTime=date;
                [tblVw reloadData];
            }
            else
            {
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"hh:mm aa"];
                NSString *date = [dateFormat stringFromDate:datePicker1.date];
                strArrivingTime=date;
                [tblVw reloadData];
            }
        }
    }
}
- (IBAction)toolbarDoneBtnClicked:(id)sender {
    
    if(IPAD) {
        
        [self kiplPickerViewDidDismissWithSelectedRow:[pickerView selectedRowInComponent:0] PickerVwTag:pickerView.tag]
        ;        [self dismissViewControllerAnimated:YES completion:nil];
        
    } else {
        
        for (NSInteger i=0; i<arrTitles.count; i++)
        {
            AddAircraftCell *cell = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            if(cell.txtDiscriptionView)
            {
                if (cell.txtDiscriptionView.tag == currentActiveTextField.tag+1)
                {
                    [cell.txtDiscriptionView becomeFirstResponder];
                    return;
                }
            }
            
            if (cell.txtField.tag == currentActiveTextField.tag+1)
            {
                [cell.txtField becomeFirstResponder];
                return;
            }
        }
    }
}

- (void)openPickerViewIpad:(NSIndexPath *)indexPath :(id)pickerView :(UITextField *)textField
{
    CGRect rectOfCellInTableView = [tblVw rectForRowAtIndexPath:indexPath];
    CGRect rectOfCellInSuperview = [tblVw convertRect:rectOfCellInTableView toView:[tblVw superview]];
    
    UIViewController * popoverContentVC = [[UIViewController alloc] init];
    popoverContentVC.preferredContentSize = CGSizeMake(320, 250);
    
    UIView *popoverView = [[UIView alloc] init];
    popoverView.backgroundColor = [UIColor whiteColor];
    [popoverView addSubview:pickerView];
    
    popoverContentVC.view = popoverView;
    popoverContentVC.modalPresentationStyle = UIModalPresentationPopover;
    
    UIPopoverPresentationController *popController = [popoverContentVC popoverPresentationController];
    popController.delegate=self;
    popController.sourceView = self.view;
    
    UIToolbar *toolBarIpad = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,320.0f,44.0f)];
    toolBarIpad.barTintColor = [UIColor grayColor];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self  action:@selector(toolbarDoneBtnClicked:)];
    
    [doneButton setTintColor:[UIColor whiteColor]];
    
    [toolBarIpad setItems:[NSArray arrayWithObjects:doneButton,nil]];
    [popoverView addSubview:toolBarIpad];
    
    //    if(selectedCellIndexpath == 4 || selectedCellIndexpath == 5) {
    //        popController.sourceRect = CGRectMake(-210, rectOfCellInSuperview.origin.y+80, rectOfCellInSuperview.size.width, rectOfCellInSuperview.size.height);
    //
    //        popController.permittedArrowDirections = UIPopoverArrowDirectionDown;
    //
    //    } else {
    
    popController.sourceRect = CGRectMake(-210, rectOfCellInSuperview.origin.y, rectOfCellInSuperview.size.width, rectOfCellInSuperview.size.height);
    popController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    //    }
    
    
    [self presentViewController:popoverContentVC animated:YES completion:nil];
    
}

-(void)sendCharterID:(NSString *)strCharterID aircraftId:(NSString *)aircrftID
{
    self.strCharterLegID=[NSString stringWithFormat:@"%@",strCharterID];
}

-(IBAction)leftButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)rightBtnClicked:(id)sender

{
    if (strTime.length>0 && strDate.length>0) {
        //departure_datetime
        
        
        NSString *currentDateString=[NSString stringWithFormat:@"%@ %@",strDate,strTime];
        NSLog(@"currentDateString: %@", currentDateString);
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd yyyy hh:mm aa"];
        NSDate *date = [dateFormat dateFromString:currentDateString];
        NSLocale *deLocale = [NSLocale currentLocale];
        dateFormat.locale=deLocale;
        // Convert date object to desired output format
        
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        currentDateString = [dateFormat stringFromDate:date];
        NSLog(@"%@",currentDateString);
        
        [dictParam setObject:currentDateString forKey:@"departure_datetime"];
    }
    if (strArrivingTime.length>0 && strArrivingDate.length>0) {
        //departure_datetime
        
        
        NSString *currentDateString=[NSString stringWithFormat:@"%@ %@",strArrivingDate,strArrivingTime];
        NSLog(@"currentDateString: %@", currentDateString);
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd yyyy hh:mm aa"];
        NSDate *date = [dateFormat dateFromString:currentDateString];
        NSLocale *deLocale = [NSLocale currentLocale];
        dateFormat.locale=deLocale;
        // Convert date object to desired output format
        
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        currentDateString = [dateFormat stringFromDate:date];
        NSLog(@"%@",currentDateString);
        
        [dictParam setObject:currentDateString forKey:@"arrival_datetime"];
        if ([dictParam[@"arrival_datetime"]isEqualToString:dictParam[@"departure_datetime"]]) {
            [CommonFunction showAlertWithTitle:@"" message:@"Departure and Return time should not be same." onViewController:self useAsDelegate:NO dismissBlock:nil];
            return;
        }
    }
    if (![self isNotNull:self.strCharterLegID]) {
        self.strCharterLegID=@"";
    }
    [dictParam setObject:self.strCharterLegID forKey:@"charter_id"];
    
    [dictParam setObject:strStatusFlag forKey:@"status"];
    [dictParam setObject:@"1" forKey:@"showalert"];
    
    NSLog(@"%@",dictParam);
    if ([self validateTextField]) {
        
        if ([CommonFunction reachabiltyCheck]) {
            self.navigationController.navigationBar.userInteractionEnabled = NO;
            [CommonFunction showActivityIndicatorWithText:@""];
            [self adCharterAPI];
        }
        else
        {
            [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection." onViewController:self useAsDelegate:NO dismissBlock:nil];
        }
        
    }
    
}
-(void)viewDatePicker:(NSInteger)indepath
{
    [self.view endEditing:YES];
    vwDate.alpha=1;
    CGRect VwframeSet=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+vwSliderBg.frame.size.height);
    vwDate.frame = VwframeSet;
    if (indepath==4||indepath==6) {
        if (indepath==4) {
            datePicker.minimumDate=[NSDate date];
        }
        else if(indepath==6)
        {
            datePicker.minimumDate=dtArrivalCriteria;
        }
        datePicker.datePickerMode=UIDatePickerModeDate;
        
    }
    else
    {
        
        datePicker.datePickerMode=UIDatePickerModeTime;
        
        
    }
    
    if(IPAD) {
        //        UIDatePicker *datePicker1 = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 44, 320, 206)];
        
        [datePickerIpad addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        [datePickerTime addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        if (indepath==4||indepath==6) {
            
            //            datePicker1.datePickerMode=UIDatePickerModeDate;
            //            datePicker1.minimumDate=[NSDate date];
            if (indepath==4) {
                datePickerIpad.minimumDate=[NSDate date];
            }
            else if(indepath==6)
            {
                datePickerIpad.minimumDate=dtArrivalCriteria;
            }
            [self datePickerValueChanged:datePickerIpad];
            
            [self openPickerViewIpad:[NSIndexPath indexPathForRow:indepath inSection:0] :datePickerIpad :nil];
            
        } else {
            //datePicker1.datePickerMode=UIDatePickerModeTime;
            [self datePickerValueChanged:datePickerTime];
            
            [self openPickerViewIpad:[NSIndexPath indexPathForRow:indepath inSection:0] :datePickerTime :nil];
        }
        
        
    } else {
        
        [vwDateCntainer setFrame:CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, 30.0f)];
        
        [self.navigationController.view addSubview:vwDate];
        
        vwDateCntainer.transform = CGAffineTransformMakeTranslation(0,vwDateCntainer.frame.size.height);
        
        vwDateCntainer.alpha = 0.0f;
        //        self.containerView.transform = CGAffineTransformMakeScale(0.85, 0.85);
        [UIView animateWithDuration:0.15
                              delay:0
                            options:(7 << 16) // note: this curve ignores durations
                         animations:^{
                             vwDateCntainer.alpha = 1.0;
                             // set transform before frame here...
                             vwDateCntainer.transform = CGAffineTransformIdentity;
                         }
                         completion:^(BOOL finished) {
                             [UIView animateWithDuration:.1 animations:^{
                                 //       [self.pickerView reloadAllComponents];
                                 vwDateCntainer .transform = CGAffineTransformIdentity;
                             }];
                         }];
    }
    
}

#pragma mark - IBAction Btn Method
-(IBAction)otherBtnClicked :(UIButton*)sender
{
    [self getUserLocation:sender.tag];
}
- (IBAction)toolbarCancleBtnClicked:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)tollbarDoneBtnClicked:(id)sender {
    [self.view endEditing:YES];
}

-(IBAction)singleTapClicked:(id)sender
{
    [self.view endEditing:YES];
    tblVw.userInteractionEnabled=YES;
    [tblVw setScrollEnabled:YES];
    tblVw.alpha=1;
    [tbleAirportVw setHidden:YES];
}
- (IBAction)dateDoneBtnClicked:(id)sender {
    
    if (selectedCellIndexpath==4||selectedCellIndexpath==6) {
        
        if (selectedCellIndexpath==4) {
            [UIView animateWithDuration:0.15
                                  delay:0
                                options:(7 << 16) // note: this curve ignores durations
                             animations:^{
                                 //                         self.scrollerContentView.alpha = 0.0;
                                 vwDateCntainer.alpha = 0.0f;
                                 vwDate.alpha=0.0f;
                                 // set transform before frame here...
                                 vwDateCntainer.transform = CGAffineTransformMakeTranslation(0,vwDateCntainer.frame.size.height);
                             }
                             completion:^(BOOL finished) {
                                 [vwDate removeFromSuperview];
                                 NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                                 [dateFormat setDateFormat:@"MMM dd YYYY"];
                                 NSString *date = [dateFormat stringFromDate:datePicker.date];
                                 dtArrivalCriteria=datePicker.date;
                                 
                                 strDate=date;
                                 
                                 [tblVw reloadData];
                             }];
        }
        if (selectedCellIndexpath==6) {
            [UIView animateWithDuration:0.15
                                  delay:0
                                options:(7 << 16) // note: this curve ignores durations
                             animations:^{
                                 //                         self.scrollerContentView.alpha = 0.0;
                                 vwDateCntainer.alpha = 0.0f;
                                 vwDate.alpha=0.0f;
                                 // set transform before frame here...
                                 vwDateCntainer.transform = CGAffineTransformMakeTranslation(0,vwDateCntainer.frame.size.height);
                             }
                             completion:^(BOOL finished) {
                                 [vwDate removeFromSuperview];
                                 NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                                 [dateFormat setDateFormat:@"MMM dd YYYY"];
                                 NSString *date = [dateFormat stringFromDate:datePicker.date];
                                 strArrivingDate=date;
                                 
                                 [tblVw reloadData];
                             }];
        }
        
        
    }
    else
    {
        if (selectedCellIndexpath==5) {
            [UIView animateWithDuration:0.15
                                  delay:0
                                options:(7 << 16) // note: this curve ignores durations
                             animations:^{
                                 //                         self.scrollerContentView.alpha = 0.0;
                                 vwDateCntainer.alpha = 0.0f;
                                 vwDate.alpha=0.0f;
                                 // set transform before frame here...
                                 vwDateCntainer.transform = CGAffineTransformMakeTranslation(0,vwDateCntainer.frame.size.height);
                             }
                             completion:^(BOOL finished) {
                                 [vwDate removeFromSuperview];
                                 NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                                 [dateFormat setDateFormat:@"hh:mm aa"];
                                 NSString *date = [dateFormat stringFromDate:datePicker.date];
                                 strTime=date;
                                 [tblVw reloadData];
                             }];
            
        }
        if (selectedCellIndexpath==7) {
            [UIView animateWithDuration:0.15
                                  delay:0
                                options:(7 << 16) // note: this curve ignores durations
                             animations:^{
                                 //                         self.scrollerContentView.alpha = 0.0;
                                 vwDateCntainer.alpha = 0.0f;
                                 vwDate.alpha=0.0f;
                                 // set transform before frame here...
                                 vwDateCntainer.transform = CGAffineTransformMakeTranslation(0,vwDateCntainer.frame.size.height);
                             }
                             completion:^(BOOL finished) {
                                 [vwDate removeFromSuperview];
                                 NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                                 [dateFormat setDateFormat:@"hh:mm aa"];
                                 NSString *date = [dateFormat stringFromDate:datePicker.date];
                                 strArrivingTime=date;
                                 [tblVw reloadData];
                             }];
        }
        
    }
    
    
    
}
- (IBAction)dateCancelBtnClicked:(id)sender {
    
    [UIView animateWithDuration:0.15
                          delay:0
                        options:(7 << 16) // note: this curve ignores durations
                     animations:^{
                         //                         self.scrollerContentView.alpha = 0.0;
                         vwDateCntainer.alpha = 0.0f;
                         vwDate.alpha=0.0f;
                         // set transform before frame here...
                         vwDateCntainer.transform = CGAffineTransformMakeTranslation(0,vwDateCntainer.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         [vwDate removeFromSuperview];
                     }];
}
- (IBAction)availUnavailBtnClicked:(id)sender {
    UIButton *btn=(UIButton *)sender;
    
    if (btn.tag==0) {
        

        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationOptionTransitionCurlUp
                         animations:^{
                             btnAvailUnavail.transform=CGAffineTransformMakeTranslation(vwSliderBg.frame.size.width/2, 0);
                             
                         }
                         completion:^(BOOL finished){
                             strStatusFlag=@"0";
                            
                            [btnAvailUnavail setBackgroundImage:[UIImage imageNamed:@"AC_ntAvaibleBtn"] forState:UIControlStateNormal];

                             [btnAvailUnavail setTitle:@"Not available" forState:UIControlStateNormal];
                           
                             
                             NSLog(@"%@",dictParam);
                         }];
        btn.tag=1;
        
    }
    else if (btn.tag==1)
    {
        
        
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationOptionTransitionCurlUp
                         animations:^{
                             btnAvailUnavail.transform=CGAffineTransformIdentity;
                             
                         }
                         completion:^(BOOL finished){
                             strStatusFlag=@"1";
                             
                             [btnAvailUnavail setTitle:@"Available" forState:UIControlStateNormal];
                             [btnAvailUnavail setBackgroundImage:[UIImage imageNamed:@"AC_avaibleBtn"] forState:UIControlStateNormal];
                             NSLog(@"%@",dictParam);
                         }];
        btn.tag=0;
        
    }
    
}


#pragma mark - TextField Delegate Metods

-(void)textfieldDidChange
{
    UITextField *sourceTextfield = (UITextField *)[[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]] viewWithTag:2];
    UITableViewCell *CellTwo = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    if (sourceTextfield.text.length==0&& [tblVw.visibleCells containsObject:CellTwo]) {
        
        [dictParam removeObjectForKey:@"source_airport"];
        sourceLatitude= @"";
        strSouceAirportID = @"";
        strSouceAirportName = @"";
        [dictParam removeObjectForKey:@"source_timezone_latitude"];
        [tblVw setScrollEnabled:YES];
        //  else if ((!dictParam[@"source_airport"]&&(strSouceAirportID.length==0))||(!dictParam[@"source_timezone_latitude"]&&(sourceLatitude.length==0)))
    }
    
    
    //  else  if ((!dictParam[@"destination_airport"]&&(strDestinationAirportID.length==0))||(!dictParam[@"destination_timezone_latitude"]&&(destinationLatitude.length==0)))
    
    UITableViewCell *CellThree = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    UITextField *destinationTextfield = (UITextField *)[[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]] viewWithTag:3];
    if (destinationTextfield.text.length==0 && [tblVw.visibleCells containsObject:CellThree]) {
        
        [dictParam removeObjectForKey:@"destination_airport"];
        destinationLatitude= @"";
        strDestinationAirportID = @"";
        strDestinationAirportName = @"";
        [dictParam removeObjectForKey:@"destination_timezone_latitude"];
        [tblVw setScrollEnabled:YES];

    }

    UITableViewCell *CellLast = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:arrTitles.count-1 inSection:0]];

    UITextField *priceTextfield = (UITextField *)[[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:arrTitles.count-1 inSection:0]] viewWithTag:arrTitles.count-1];
    if (priceTextfield.text.length==0 && [tblVw.visibleCells containsObject:CellLast]) {
        
        [dictParam removeObjectForKey:@"price_per_hour"];
    }

    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag==0||textField.tag==1)
    {
        if (textField.tag == 0)
        {
            if (arrAircraft.count == 0)
            {
                [CommonFunction showActivityIndicatorWithText:@""];
                [self getAircraftAPI];
                return NO;
            }
        }
        else if (textField.tag == 1)
        {
            if (arrCharterType.count == 0)
            {
                [CommonFunction showActivityIndicatorWithText:@""];
                [self getCharterType];
                return NO;
            }
        }
        
        if (textField.tag==0) {
            currentActiveTextField=textField;
            //            if (!self.strCharterID.length>0) {
            if(IPAD) {
                
                [self.view endEditing:YES];
                pickerView.tag = 0;
                [pickerView reloadAllComponents];
                [self openPickerViewIpad:[NSIndexPath indexPathForRow:textField.tag inSection:0] :pickerView :textField];
                
            }
            else
            {
                [self openPickerView:0];
            }
            
            //            }
            return NO;
        }
        if (textField.tag==1) {
            
            currentActiveTextField=textField;
            if(IPAD) {
                
                [self.view endEditing:YES];
                pickerView.tag = 1;
                [pickerView reloadAllComponents];
                [self openPickerViewIpad:[NSIndexPath indexPathForRow:textField.tag inSection:0] :pickerView :textField];
                
            }
            else
            {
                [self openPickerView:1];
            }
            return NO;
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:@"Please select aircraft type first."];
        }
        
    }
    else if (textField.tag==2||textField.tag==3)
    {
        selectedCellIndexpath=textField.tag;
        
    }
    if (textField.tag>3) {
        if (isArrivalTextFldIsVisible) {
            if (textField.tag==4||textField.tag==5 ||textField.tag==6||textField.tag==7)
            {
                currentActiveTextField =textField;
                selectedCellIndexpath=textField.tag;
                [self viewDatePicker:textField.tag];
                return NO;
            }
            
        }
        else
        {
            if (textField.tag==4||textField.tag==5)
            {
                currentActiveTextField =textField;
                selectedCellIndexpath=textField.tag;
                [self viewDatePicker:textField.tag];
                return NO;
            }
        }
        
    }
    
    currentActiveTextField=textField;
    return YES;
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag==2 || textField.tag==3) {
        
        [tbleAirportVw setHidden:NO];
        CGRect cellRect = [tblVw rectForRowAtIndexPath:[NSIndexPath indexPathForRow:textField.tag inSection:0]];
        
        
        cellRect.size.height=80;
        [tblVw setScrollEnabled:NO];
        textField.alpha=1;
        cellRect = CGRectOffset(cellRect, -tblVw.contentOffset.x, -tblVw.contentOffset.y+75);
        if (IPAD) {
            cellRect.origin.y=cellRect.origin.y+50;
        }
        [tbleAirportVw setFrame:cellRect];
        
        NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        [self getAirportName:searchStr];
        
    }
    if (isArrivalTextFldIsVisible) {
        if (textField.tag==8) {
            return !([textField.text length]>20-1 && [string length] > range.length);
        }
    }
    else
    {
        if(textField.tag==6)
        {
            return !([textField.text length]>20-1 && [string length] > range.length);
            
        }
    }
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    switch (textField.tag) {
        case 4:
        {
            [dictParam setObject:textField.text forKey:@"manufacturer"];
            break;
        }
        case 5:
        {
            [dictParam setObject:textField.text forKey:@"speed"];
            break;
        }
        case 6:
        {
            if ([textField.text integerValue]>0) {
            [dictParam setObject:textField.text forKey:@"price_per_hour"];
                
            }
            else
            {
                textField.text=@"";
            }
            
            break;
        }
        case 7:
        {
            if ([textField.text integerValue]>0) {
                [dictParam setObject:textField.text forKey:@"price_per_hour"];
                
            }
            else
            {
                textField.text=@"";
            }
            break;
        }
        case 8:
        {
            if ([textField.text integerValue]>0) {
                [dictParam setObject:textField.text forKey:@"price_per_hour"];
                
            }
            else
            {
                textField.text=@"";
            }
            break;
        }
    }
    NSLog(@"%@",dictParam);
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    for (NSInteger i=0; i<arrTitles.count; i++)
    {
        AddAircraftCell *cell = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (cell.txtField.tag == textField.tag+1)
        {
            [cell.txtField becomeFirstResponder];
            return YES;
        }
    }
    return YES;
}


#pragma mark UIKeyBoard Activity Handling
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    
}


// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets;
    if (currentActiveTextField.tag==2||currentActiveTextField.tag==3) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+100, 0.0);
    }
    else
    {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    }
    tblVw.contentInset = contentInsets;
    tblVw.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, currentActiveTextField.frame.origin) ) {
        [tblVw scrollRectToVisible:currentActiveTextField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    tblVw.contentInset = contentInsets;
    tblVw.scrollIndicatorInsets = contentInsets;
    [tblVw scrollRectToVisible:CGRectZero animated:YES];
}

- (void)openPickerView:(int)tag
{
    CGRect VwframeSet=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+vwSliderBg.frame.size.height);
    [self.navigationController.view endEditing:YES];
    KIPLPickerView *pickerview;
    if (tag==0) {
        if (strAirCraftID.length>0) {
            NSInteger selctedIndx=[[arrAircraft valueForKey:@"name"] indexOfObject:strAirCraftTxt];
            pickerview = [[KIPLPickerView alloc]initWithFrame:VwframeSet delegate:self datasource:self pickrVwTag:tag selectedInde:(int)selctedIndx];
        }
        else
        {
            pickerview = [[KIPLPickerView alloc]initWithFrame:VwframeSet delegate:self datasource:self pickrVwTag:tag selectedInde:0];
        }
    }
    else
    {
        if (strCharterTypeID.length>0) {
            NSInteger selctedIndx=[[arrCharterType valueForKey:@"name"] indexOfObject:strCharterType];
            pickerview = [[KIPLPickerView alloc]initWithFrame:VwframeSet delegate:self datasource:self pickrVwTag:tag selectedInde:(int)selctedIndx];
        }
        else
        {
            pickerview = [[KIPLPickerView alloc]initWithFrame:VwframeSet delegate:self datasource:self pickrVwTag:tag selectedInde:0];
        }
    }
    
    [pickerview updatePickerView];
    [self.navigationController.view addSubview:pickerview];
}




#pragma mark - TableView Delegate Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==tbleAirportVw) {
        return arrAirportList.count;
    }
    else
    {
        return arrTitles.count;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==tbleAirportVw) {
        return 40.0f;
    }
    else
    {
        if (indexPath.row==0) {
            return 125*SCREEN_YScale;
        }
        else
        {
            return 72*SCREEN_YScale;
        }
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tbleAirportVw) {
        
        NSInteger cellValue = 4;
        NSString *cellIdentifier=@"AirportCell";
        
        AddAircraftCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AddAircraftCell class]) owner:self options:nil][cellValue];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    else
    {
        NSInteger cellValue = 0;
        NSString *cellIdentifier=@"AddAircraftCell";
        
        if (indexPath.row==0) {
            cellIdentifier=@"AddCharterCell";
            cellValue=3;
        }
        
        
        AddAircraftCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AddAircraftCell class]) owner:self options:nil][cellValue];
        }
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(AddAircraftCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tbleAirportVw) {
        cell.lblTextFieldName.text=arrAirportList[indexPath.row][@"name"];
    }
    else
    {
        [cell.btnOther setHidden:YES];
        cell.txtField.text=@"";
        cell.lblTextFieldName.text=[arrTitles objectAtIndex:indexPath.row];
        [cell.imgBottomArrow setImage:nil];
        [cell.lblKm setText:@""];
        cell.btnOther.tag=indexPath.row;
        [cell.btnOther addTarget:self action:@selector(otherBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if (indexPath.row==0||indexPath.row==1) {
            cell.txtField.placeholder=@"Select";
            [cell.imgBottomArrow setImage:[UIImage imageNamed:@"AAC_bottomArrow"]];
            if (indexPath.row==0) {
                NSLog(@"%@",self.strCharterLegID);
                if (strAirCraftTxt.length>0) {
                    [cell.txtField setText:strAirCraftTxt];
                }
                
                else if (self.strAircraftName.length>0)
                {
                    
                    
                    [cell.txtField setText:self.strAircraftName];
                    //                    [cell.txtField.userInteractionEnabled=NO];
                    cell.userInteractionEnabled=NO;
                }
            }
            else
            {
                if (strCharterType.length>0) {
                    [cell.txtField setText:strCharterType];
                }
                else
                {
                    [cell.txtField setText:@""];
                }
            }
        }
        else if (indexPath.row==2)
        {
            [cell.btnOther setHidden:NO];
            if (strSouceAirportName.length>0)
            {
                
                cell.txtField.text=strSouceAirportName;
            }
        }
        else if (indexPath.row==3)
        {
            [cell.btnOther setHidden:NO];
            if (strDestinationAirportName.length>0)
            {
                cell.txtField.text=strDestinationAirportName;
            }
        }
        
        else if (indexPath.row==4)
        {
            if (strDate.length>0) {
                cell.txtField.text=strDate;
            }
            cell.txtField.placeholder=@"MM DD YYYY";
        }
        else if (indexPath.row==5)
        {
            if (strTime.length>0) {
                cell.txtField.text=strTime;
            }
        }
        else if (indexPath.row>=6)
        {
            
            if (isArrivalTextFldIsVisible) {
                
                
                if (indexPath.row==6) {
                    if (strArrivingDate.length>0) {
                        cell.txtField.text=strArrivingDate;
                    }
                    cell.txtField.placeholder=@"MM DD YYYY";
                }
                else if (indexPath.row==7)
                {
                    if (strArrivingTime.length>0) {
                        cell.txtField.text=strArrivingTime;
                    }
                    cell.txtField.placeholder=@"Select";
                }
                
                else if (indexPath.row==8) {
                    cell.txtField.keyboardType=UIKeyboardTypeDecimalPad;
                    cell.txtField.inputAccessoryView=vwToolbar;
                    cell.txtField.placeholder=@"0.00";
                    cell.lblKm.text=@"USD";
                    NSString *strPric=dictParam[@"price_per_hour"];
                    if (dictParam[@"price_per_hour"]||strPric.length>0) {
                        cell.txtField.text=dictParam[@"price_per_hour"];
                    }
                }
                
            }
            else
            {
                cell.txtField.keyboardType=UIKeyboardTypeDecimalPad;
                cell.txtField.inputAccessoryView=vwToolbar;
                cell.txtField.placeholder=@"0.00";
                cell.lblKm.text=@"USD";
                NSString *strPric=dictParam[@"price_per_hour"];
                if (dictParam[@"price_per_hour"]||strPric.length>0) {
                    cell.txtField.text=dictParam[@"price_per_hour"];
                }
            }
        }
        else
        {
            cell.txtField.placeholder=@"Enter";
        }
        cell.txtField.delegate=self;
        cell.txtField.tag=indexPath.row;
        
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tbleAirportVw) {
        {
            
            if (selectedCellIndexpath==2) {
                strSouceAirportName=arrAirportList[indexPath.row][@"name"];
                strSouceAirportID=[NSString stringWithFormat:@"%@",arrAirportList[indexPath.row][@"id"]];
                
                [dictParam setObject:strSouceAirportID forKey:@"source_airport"];
                [dictParam setObject:strSouceAirportName forKey:@"source_airport_name"];
                [dictParam setObject:@"" forKey:@"source_timezone_latitude"];
                [dictParam setObject:@"" forKey:@"source_timezone_longitude"];
                
                [tbleAirportVw setHidden:YES];
                [tblVw setScrollEnabled:YES];
                [tblVw reloadData];
                
            }
            else
            {
                strDestinationAirportName=arrAirportList[indexPath.row][@"name"];
                strDestinationAirportID=[NSString stringWithFormat:@"%@",arrAirportList[indexPath.row][@"id"]];
                [dictParam setObject:strDestinationAirportID forKey:@"destination_airport"];
                [dictParam setObject:strDestinationAirportName forKey:@"destination_airport_name"];
                [dictParam setObject:@"" forKey:@"destination_timezone_latitude"];
                [dictParam setObject:@"" forKey:@"destination_timezone_longitude"];
                [tbleAirportVw setHidden:YES];
                [tblVw setScrollEnabled:YES];
                [tblVw reloadData];
            }
        }
    }
}



#pragma mark - WebService API
-(void)getAircraftAPI
{
    
    NSString *url = [NSString stringWithFormat:@"aircraft_list/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
    //http://192.168.0.131:7682/aircraft_list
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
        {
            arrAircraft=[[NSMutableArray alloc]init];
            arrAircraft=[responseDict objectForKey:@"data"];
            [tblVw reloadData];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }
                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  
                                                  [self getAircraftAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}
-(void)getCharterType
{
    
    NSString *url = [NSString stringWithFormat:@"charter_types"];
    //http://192.168.0.131:7682/aircraft_list
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
        {
            arrCharterType=[[NSMutableArray alloc]init];
            arrCharterType=[responseDict objectForKey:@"data"];
            [tblVw reloadData];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  
                                                  [self getAircraftAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}
-(void)getAirportName:(NSString *)strName;
{
    
    NSString *url = [NSString stringWithFormat:@"airports_list"];
    //http://192.168.0.131:7682/aircraft_list
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    NSMutableDictionary *param =[[NSMutableDictionary alloc]initWithObjectsAndKeys:strName,@"keyword", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
        {
            arrAirportList=[[NSMutableArray alloc]init];
            arrAirportList=[responseDict objectForKey:@"data"];
            [tblVw scrollsToTop];
            [tbleAirportVw reloadData];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  
                                                  [self getAircraftAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}



-(void)adCharterAPI
{
    
    NSString *url = [NSString stringWithFormat:@"add_leg/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
    //    http://192.168.0.131:7684/POST /create_charter/{SID}
    
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:dictParam withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
            
        {
            if ([[responseDict objectForKey:@"type"]boolValue]) {
                
                //                    LegListingVC *llvc=[[LegListingVC alloc]initWithNibName:NSStringFromClass([LegListingVC class]) bundle:nil];
                //                    llvc.dictCharterDetail=[responseDict mutableCopy];
                // [dictParam setObject:@"1" forKey:@"showalert"];
                //                Per seat charge
                //                Charge for the no of selected seat by customer
                //                    Total cost of charter (Excluding Admin’s commission)
                
                if ([dictParam[@"showalert"] boolValue]) {
                    
                    
                    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:strCharterType message:responseDict[@"messageToShow"] preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        
                        
                    }]];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        [dictParam setObject:@"0" forKey:@"showalert"];
                        
                        if ([CommonFunction reachabiltyCheck]) {
                            self.navigationController.navigationBar.userInteractionEnabled = NO;
                            [CommonFunction showActivityIndicatorWithText:@""];
                            [self adCharterAPI];
                        }
                        else
                        {
                            [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection. " onViewController:self useAsDelegate:NO dismissBlock:nil];
                        }
                        
                    }]];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                else
                {
                    [CommonFunction showAlertWithTitle:@"Jetseta" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:YES dismissBlock:^{
                        
//                        [self.navigationController popToRootViewControllerAnimated:YES];
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    
                }
                self.navigationController.navigationBar.userInteractionEnabled = YES;
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                [CommonFunction showAlertWithTitle:@"Jetseta" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:NO dismissBlock:nil];            }
            self.navigationController.navigationBar.userInteractionEnabled = YES;
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
            self.navigationController.navigationBar.userInteractionEnabled = YES;
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          self.navigationController.navigationBar.userInteractionEnabled = YES;
                                          [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  
                                                  [self adCharterAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

-(void)getUserLocation:(int)indexPath
{
    [self.view endEditing:YES];
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(CurrentLatitude, CurrentLongitude);
    
    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001);
    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001);
    GMSCoordinateBounds *viewport = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
                                                                         coordinate:southWest];
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:viewport];
    _placePicker = [[GMSPlacePicker alloc] initWithConfig:config];
    
    [CommonFunction setNavigationBar:self.navigationController];
    
    [_placePicker pickPlaceWithCallback:^(GMSPlace *place, NSError *error) {
        if (error != nil) {
            NSLog(@"Pick Place error %@", [error localizedDescription]);
            return;
        }
        
        if (place != nil)
        {
            
            if (indexPath==2) {
                //strAddress = place.formattedAddress;
                if (strSouceAirportName) {
                    NSString *strLatitude=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
                    NSString *strLongitude=[NSString stringWithFormat:@"%f",place.coordinate.longitude];
                    [dictParam setObject:strSouceAirportName forKey:@"source_airport_name"];
                    [dictParam setObject:strLatitude forKey:@"source_timezone_latitude"];
                    [dictParam setObject:strLongitude forKey:@"source_timezone_longitude"];
                    [dictParam removeObjectForKey:@"source_airport"];
                    
                    
                    //     [_dictFbProfileInfo setObject:strAddress forKey:@"location"];
                    
                }
                strSouceAirportName = place.name;
                sourceLatitude=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
                if (strSouceAirportName) {
                    NSString *strLatitude=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
                    NSString *strLongitude=[NSString stringWithFormat:@"%f",place.coordinate.longitude];
                    [dictParam setObject:strSouceAirportName forKey:@"source_airport_name"];
                    [dictParam setObject:strLatitude forKey:@"source_timezone_latitude"];
                    [dictParam setObject:strLongitude forKey:@"source_timezone_longitude"];
                    [dictParam removeObjectForKey:@"source_airport"];
                    
                    
                }
                
                [tblVw reloadData];
                NSLog(@"Place name %@", place.name);
                NSLog(@"%f",place.coordinate.latitude);
                NSLog(@"%f",place.coordinate.longitude);
                NSLog(@"Place address %@", place.formattedAddress);
                NSLog(@"Place attributions %@", place.attributions.string);
                
            }
            else
            {
                //strAddress = place.formattedAddress;
                if (strDestinationAirportName) {
                    NSString *strLatitude=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
                    NSString *strLongitude=[NSString stringWithFormat:@"%f",place.coordinate.longitude];
                    [dictParam setObject:strDestinationAirportName forKey:@"destination_airport_name"];
                    [dictParam setObject:strLatitude forKey:@"destination_timezone_latitude"];
                    [dictParam setObject:strLongitude forKey:@"destination_timezone_longitude"];
                    [dictParam removeObjectForKey:@"destination_airport"];
                    
                    
                }
                strDestinationAirportName = place.name;
                destinationLatitude=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
                if (strDestinationAirportName) {
                    NSString *strLatitude=[NSString stringWithFormat:@"%f",place.coordinate.latitude];
                    NSString *strLongitude=[NSString stringWithFormat:@"%f",place.coordinate.longitude];
                    [dictParam setObject:strDestinationAirportName forKey:@"destination_airport_name"];
                    [dictParam setObject:strLatitude forKey:@"destination_timezone_latitude"];
                    [dictParam setObject:strLongitude forKey:@"destination_timezone_longitude"];
                    [dictParam removeObjectForKey:@"destination_airport"];
                }
                
                [tblVw reloadData];
                
            }
        } else {
            NSLog(@"No place selected");
        }
    }];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

-(void)removeKeyboard
{
    [currentActiveTextField resignFirstResponder];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [tblVw setContentOffset:CGPointZero animated:YES];
    });
    
}

@end
