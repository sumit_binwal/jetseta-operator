//
//  AircraftListVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 13/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "AircraftListVC.h"
#import "AddAircraftVC.h"
#import "AircraftListCell.h"
#import "SWRevealViewController.h"
#import "LoginVC.h"
#import "AircraftDetailVC.h"
@interface AircraftListVC ()
{
    
    IBOutlet UIBarButtonItem *barbNT;
    IBOutlet UIView *vwToolbar;
    IBOutlet UILabel *lblErrorMsg;
    IBOutlet UITableView *tblAircraftList;
    NSMutableArray *arrAircraftList;
    IBOutlet UIButton *btnAddAircraft;
    IBOutlet UILabel *lblAIrcraftMsg;
       int currentPage,totalpost;
    BOOL isNextPage;
    
}
@end

@implementation AircraftListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
   

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
       
      APPDELEGATE.mainTapBarCntroller.tabBar.hidden=NO;
    currentPage=1;
    isNextPage=TRUE;
    
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        [self getAircraftListAPI];
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection. " onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
}


-(void)setupView
{
    [CommonFunction setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"My Aircraft"];
    
    
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"AL_Menu"]style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;

    
    UIBarButtonItem *barBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"AL_AddButton"] style:UIBarButtonItemStyleDone target:self action:@selector(addButtonClicked:)];

    [self.navigationItem setRightBarButtonItem:barBtn];
    
    if(IPAD)
    {
        btnAddAircraft.titleLabel.font=[UIFont fontWithName:btnAddAircraft.titleLabel.font.fontName size:24];
        [lblAIrcraftMsg setFont:[UIFont fontWithName:lblAIrcraftMsg.font.fontName size:30]];
        
    }
    else
    {
        
        btnAddAircraft.titleLabel.font=[UIFont fontWithName:btnAddAircraft.titleLabel.font.fontName size:btnAddAircraft.titleLabel.font.pointSize*SCREEN_XScale];
        [lblAIrcraftMsg setFont:[UIFont fontWithName:lblAIrcraftMsg.font.fontName size:lblAIrcraftMsg.font.pointSize*SCREEN_XScale]];
    }

}
#pragma mark - UserDefine Method
-(IBAction)menuButtonClicked:(id)sender
{
//    LoginVC *fufs=[[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil];
//    APPDELEGATE.navigationController=[[UINavigationController alloc]initWithRootViewController:fufs];
//    self.navigationController.navigationBarHidden=YES;
//    [APPDELEGATE.window.window setRootViewController:self.navigationController];
//    [NSUSERDEFAULTS removeObjectForKey:@"previousSelectedVC"];
//    [NSUSERDEFAULTS removeObjectForKey:kUSERTOKEN];
//    
//    [CommonFunction changeRootViewController:NO aircraftPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue] charterPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_CHARTER]boolValue]];

    
    
}

-(IBAction)addButtonClicked:(id)sender
{
    AddAircraftVC *acvc=[[AddAircraftVC alloc]initWithNibName:NSStringFromClass([AddAircraftVC class]) bundle:nil];
    [self presentViewController:acvc animated:YES completion:nil];

}
#pragma mark - TableView Delegate Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return   arrAircraftList.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

        return 208*SCREEN_YScale;
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
  NSString * cellIdentifier=@"AircraftListCell";
    
    AircraftListCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[AircraftListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(AircraftListCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

    if ([self isNotNull:arrAircraftList[indexPath.row][@"aircraft_type"]]) {
    cell.lblDiscription.text=arrAircraftList[indexPath.row][@"aircraft_type"];        
    }
    cell.lblLocation.text=arrAircraftList[indexPath.row][@"address"];
    cell.lblNumberofUser.text=[NSString stringWithFormat:@"%@",arrAircraftList[indexPath.row][@"capacity"]];
    cell.lblTitle.text=arrAircraftList[indexPath.row][@"name"];
    [cell.imgAircraftImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",arrAircraftList[indexPath.row][@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    cell.lblPrice.text=[NSString stringWithFormat:@"%@/hr",arrAircraftList[indexPath.row][@"price_per_hour"]];
    [cell.vwLegBg setHidden:YES];
    
    if (indexPath.row == [arrAircraftList count]-1)
    {
        //         [CommonFunction showActivityIndicatorWithText:@""];
        [self getAircraftListAPI];
    }

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AircraftDetailVC *advc=[[AircraftDetailVC alloc]initWithNibName:NSStringFromClass([AircraftDetailVC class]) bundle:nil];
    advc.strAircraftID=arrAircraftList[indexPath.row][@"id"];
    [self.navigationController pushViewController:advc animated:YES];
  }
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ((touch.view == self.view))
    {
        return NO;
    }
    return YES;
}
#pragma mark - WebService API
-(void)getAircraftListAPI
{
    if (currentPage==1) {
        
        if (!isNextPage) {
            [CommonFunction removeActivityIndicator];

            return;
        }
    }
    else
    {
        if(totalpost==[arrAircraftList count])
        {
            [CommonFunction removeActivityIndicator];
            return;
        }
    }
    NSString *url = [NSString stringWithFormat:@"my_aircrafts/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
//    http://192.168.0.131:7684/my_aircrafts/83a595e384926272ad5cf8a48208d486
    
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    NSMutableDictionary *param =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithUnsignedInteger:currentPage],@"page", nil];

    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        [CommonFunction removeActivityIndicator];
        
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        else if([operation.response statusCode]==200)
        {
            
                NSArray *arrListing = [responseDict valueForKey:@"charters"];
                [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"is_publish_aircraft"] forKey:kPUBLISH_AIRCRAFT];
                [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"is_publish_charter"] forKey:kPUBLISH_CHARTER];
                [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"published_aircraft_num"] forKey:kPUBLISH_COUNT];
                [NSUSERDEFAULTS synchronize];

                if (arrListing.count>0)
                {
                    [btnAddAircraft setHidden:YES];
                    [lblAIrcraftMsg setHidden:YES];
                    lblErrorMsg.text=@"";
                
                    if (arrListing.count > 0)
                    {
                        if (currentPage == 1)
                            {
                                if (arrAircraftList.count > 0)
                                    {
                                        [arrAircraftList removeAllObjects];
                                    }
                                if (arrAircraftList == nil)
                                    {
                                        arrAircraftList = [NSMutableArray array];
                                    }
                                arrAircraftList = [arrListing mutableCopy];
                            }
                        else
                            {
                                [arrAircraftList addObjectsFromArray:arrListing];
                            }
                    }
                
                    [tblAircraftList reloadData];
                    isNextPage=[[responseDict objectForKey:@"nextpage"]boolValue];
                
                    if ([[responseDict objectForKey:@"nextpage"]boolValue]) {
                        currentPage++;
                    }
                    else
                    {
                        totalpost=(int)arrAircraftList.count;
                        return;
                    }
                }
                else
                {
                    [arrAircraftList removeAllObjects];
                    [tblAircraftList reloadData];
                    lblErrorMsg.text=@"";
                    [btnAddAircraft setHidden:NO];
                    [lblAIrcraftMsg setHidden:NO];
                }
           
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  
                                                  [self getAircraftListAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}
@end
