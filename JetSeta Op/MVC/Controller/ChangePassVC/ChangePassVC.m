//
//  ChangePassVC.m
//  JetSeta
//
//  Created by Suchita Bohra on 20/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//
#import "UtilityClass.h"
#import "ChangePassVC.h"
#import "ChangePassCell.h"
#import "LoginVC.h"

//#import "JetSeta_Constants.h"
NSUserDefaults *userDefaults;
@interface ChangePassVC ()
{
    IBOutlet UITableView *tblViewChangePass;
    UITextField *currentTextField;
    NSMutableDictionary *dictChangePass;
}

@end

@implementation ChangePassVC

#pragma mark - ViewLifeCycle Method

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    FIX_IOS_7_LAY_OUT_ISSUE;
    //back_icon
    self.navigationItem.title = @"Change password";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ques_check_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(btnTickClicked)];
    
    UIBarButtonItem *barBtn1=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_icon"] style:UIBarButtonItemStyleDone target:self action:@selector(leftButtonClicked:)];
    
        self.navigationItem.leftBarButtonItem = barBtn1;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"AC_imgCheck"]style:UIBarButtonItemStylePlain target:self action:@selector(btnTickClicked)];
        self.navigationItem.rightBarButtonItem = revealButtonItem;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeKeyboard)];
    
    [self.view addGestureRecognizer:tapGesture];
    dictChangePass = [NSMutableDictionary dictionary];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unRegisterForKeyboardNotifications];
}

#pragma mark - Memory Management Method
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableView DataSource and Delegate.

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0.0f;
    rowHeight = (70 * [AppDelegate getSharedInstance].window.bounds.size.height)/568;
    return rowHeight;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 3;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *changePassCell   = @"ChangePassCell";
    
    ChangePassCell *cell = (ChangePassCell *)[tableView dequeueReusableCellWithIdentifier:changePassCell];
    
    if (cell == nil)
    {
        cell = (ChangePassCell*) [[[NSBundle mainBundle] loadNibNamed:@"ChangePassCell" owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    cell.txtTxt.placeholder = @"Enter";
    cell.txtTxt.secureTextEntry = YES;
    cell.txtTxt.tag = indexPath.row;
    cell.txtTxt.delegate = self;
    
    if (indexPath.row == 0)
    {
        cell.lblPlaceHolderName.text = @"Current password";
        cell.txtTxt.returnKeyType = UIReturnKeyNext;
        if ([dictChangePass objectForKey:@"password"])
        {
            cell.txtTxt.text = [dictChangePass objectForKey:@"password"];
        }
    }
    else if (indexPath.row == 1)
    {
        cell.lblPlaceHolderName.text = @"New password";
        cell.txtTxt.returnKeyType = UIReturnKeyNext;
        if ([dictChangePass objectForKey:@"new_password"])
        {
            cell.txtTxt.text = [dictChangePass objectForKey:@"new_password"];
        }
    }
    else if (indexPath.row == 2)
    {
        cell.lblPlaceHolderName.text = @"Confirm new password";
        cell.txtTxt.returnKeyType = UIReturnKeyDone;
        if ([dictChangePass objectForKey:@"conf_password"])
        {
            cell.txtTxt.text = [dictChangePass objectForKey:@"conf_password"];
        }
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        cell.lblPlaceHolderName.font = [UIFont fontWithName:@"Lato-Bold" size:24.0];
        cell.txtTxt.font = [UIFont fontWithName:@"Lato-Regular" size:29.0];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    //    SWRevealViewController *revealController = self.revealViewController;
}

#pragma mark -
#pragma mark UIKeyBoard Activity Handling
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unRegisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    tblViewChangePass.contentInset = contentInsets;
    tblViewChangePass.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, currentTextField.frame.origin) ) {
        [tblViewChangePass scrollRectToVisible:currentTextField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    tblViewChangePass.contentInset = contentInsets;
    tblViewChangePass.scrollIndicatorInsets = contentInsets;
    [tblViewChangePass scrollRectToVisible:CGRectZero animated:YES];
    
}

#pragma mark -
#pragma mark UITextField Delegate Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    currentTextField = textField;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self manageChangePassInfoDictionary:textField];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger tag = textField.tag + 1;
    
    if (textField.tag == 2)
    {
        [self removeKeyboard];
    }
    else
    {
        ChangePassCell *cell = (ChangePassCell *) [tblViewChangePass cellForRowAtIndexPath:[NSIndexPath indexPathForRow:tag inSection:0]];
        [cell.txtTxt becomeFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (!(([string isEqualToString:@""])))
    {//not a backspace, else `characterAtIndex` will crash.
        unichar unicodevalue = [string characterAtIndex:0];
        if (unicodevalue == 55357) {
            return NO;
        }
        else if ([string isEqualToString:@" "])
        {
            return NO;
        }
        return YES;
    }
    return YES;
}


#pragma mark - UserDefined Methods
-(IBAction)leftButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)manageChangePassInfoDictionary:(UITextField *)tF
{
    if (tF.tag == 0)
    {
        [dictChangePass setObject:tF.text forKey:@"password"];
    }
    else if (tF.tag == 1)
    {
        [dictChangePass setObject:tF.text forKey:@"new_password"];
    }
    else if (tF.tag == 2)
    {
        [dictChangePass setObject:tF.text forKey:@"conf_password"];
    }
}

-(void)removeKeyboard
{
    [currentTextField resignFirstResponder];
    [tblViewChangePass scrollRectToVisible:CGRectZero animated:YES];
    
}

-(BOOL)validateSignup
{
    NSString *errormsg;
    BOOL error=FALSE;
    
    NSString *strPass = [dictChangePass objectForKey:@"password"];
    NSString *strNewPass = [dictChangePass objectForKey:@"new_password"];
    NSString *strConPass = [dictChangePass objectForKey:@"conf_password"];
    
    UITextField *tf;
    NSInteger cellTag = 0;
    if (![UtilityClass isValueNotEmpty:strPass])
    {
        errormsg = @"Please enter your password to continue.";
        error=TRUE;
        cellTag = 0;
    }
    else if (!error && strPass.length < 6)
    {
        errormsg = @"Please enter your password to continue (It should have at least 6 characters)";
        error=TRUE;
        cellTag = 0;
    }
    else if (!error && ![UtilityClass isValueNotEmpty:strNewPass])
    {
        errormsg = @"Please enter your new password to continue. ";
        error=TRUE;
        cellTag = 1;
    }
    else if (!error && strNewPass.length < 6)
    {
        errormsg = @"Please enter your new password to continue. (It should have at least 6 characters).";
        error=TRUE;
        cellTag = 1;
    }
    else if (!error && ![UtilityClass isValueNotEmpty:strConPass])
    {
        errormsg = @"Please enter your confirmed password to continue.";
        error=TRUE;
        cellTag = 2;
    }
    else if (!error && ![strConPass isEqualToString:strNewPass])
    {
        errormsg = @"New Password and confirmed password do not match.";
        error=TRUE;
        cellTag = 2;
    }
    
    ChangePassCell *cell = (ChangePassCell *) [tblViewChangePass cellForRowAtIndexPath:[NSIndexPath indexPathForRow:cellTag inSection:0]];
    
    tf = cell.txtTxt;
    [tf becomeFirstResponder];
    if(error)
    {
        [UtilityClass showAlertWithTitle:@"" message:errormsg onViewController:self useAsDelegate:NO dismissBlock:^{
            
        }];
        return FALSE;
    }
    return TRUE;
}

-(void)btnTickClicked
{
    NSLog(@"createAccountBtnClicked");
    if (![CommonFunction reachabiltyCheck])
    {
        return;
    }
    
    [currentTextField resignFirstResponder];
    
    if ([self validateSignup])
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self sendRequestForChangePasswordToServer];
    }
}

-(void)sendRequestForChangePasswordToServer
{
    BOOL error = NO;
    NSString *errormsg = @"";
    NSInteger cellTag = 0;
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    if ([dictChangePass objectForKey:@"password"])
    {
        [param setObject:[dictChangePass objectForKey:@"password"] forKey:@"old_password"];
    }
    else
    {
        errormsg = @"Please enter your password to continue.";
        error=TRUE;
        cellTag = 0;
    }
    
    if ([dictChangePass objectForKey:@"new_password"])
    {
         [param setObject:[dictChangePass objectForKey:@"new_password"] forKey:@"password"];
    }
    else
    {
        errormsg = @"Please enter your new password to continue. ";
        error=TRUE;
        cellTag = 1;
    }
    
    if(error)
    {
        [UtilityClass showAlertWithTitle:@"" message:errormsg onViewController:self useAsDelegate:NO dismissBlock:^{
            
        }];
        
    }
    else
    {

        
        NSString *url = [NSString stringWithFormat:@"change_password/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
        
        NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
        
        ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
        
        [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
            NSLog(@"responce dict %@",responseDict);
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            if(responseDict == Nil)        {

                [CommonFunction alertTitle:@"" withMessage:@"Server error"];
            }
            
            else if([operation.response statusCode]==200)
            {
                if ([[responseDict valueForKey:@"type"]boolValue])
                {
                    [UtilityClass showAlertWithTitle:@"" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:YES dismissBlock:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
                else
                {
                    [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
                }
                
            }
            else
            {
                [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
            }
        }
        withFailure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [CommonFunction removeActivityIndicator];
             
             if([operation.response statusCode]  == 426)
             {
                 NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                 NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                 [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                     
                     NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                 }];
             }

             else if([operation.response statusCode]  == 400 )
             {
                 NSLog(@"impo response %@",operation.response);
                 
                                               [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
             }
         }];
        
    }
    
}

@end
