//
//  CountryCodeVC.m
//  JetSeta
//
//  Created by Suchita Bohra on 15/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import "CountryCodeVC.h"
#import "SignupCellName.h"

#import "CountryCodeCell.h"
//#import "SignupVC.h"
//#import "ProfileFbVC.h"
#import "AppDelegate.h"
#import "UpdateProfileVC.h"
#import "LoginVC.h"

@interface CountryCodeVC ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    IBOutlet UITableView *tblViewCountryCode;
    IBOutlet UITextField *txtSearchField;
    NSMutableArray *arrCountryCode, *arrSearchResult;
    BOOL isSearched;
    UITextField *currentTF;
}

-(IBAction)backBtnClicked:(id)sender;
-(IBAction)checkMarkBtnnClicked:(id)sender;

@end

@implementation CountryCodeVC

#pragma mark - ViewLifeCycle Method
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    FIX_IOS_7_LAY_OUT_ISSUE;
    self.navigationItem.title = @"Select Country Code";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"AC_imgCheck"] style:UIBarButtonItemStylePlain target:self action:@selector(btnCheckMarkClicked)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        txtSearchField.font = [UIFont fontWithName:@"Lato-Regular" size:27.0f];
    }
    
    if ([AppDelegate getSharedInstance].arrCountryCode.count)
    {
        arrCountryCode = nil;
        arrCountryCode = [NSMutableArray array];
        arrCountryCode = [[AppDelegate getSharedInstance].arrCountryCode mutableCopy];
        [tblViewCountryCode reloadData];
    }
    else
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self getCountryCodeListingFromServerSide];
    }
}

#pragma mark - MemoryManagement Method
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDelegate and DataSource

#pragma mark - UITableView DataSource and Delegate.

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0.0f;
    rowHeight = (46 * [AppDelegate getSharedInstance].window.frame.size.height) / 600;
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    NSInteger rowCount = 0.0;
    if (isSearched)
    {
        rowCount = arrSearchResult.count;
    }
    else
    {
        rowCount = arrCountryCode.count;
    }
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //for name..
    static NSString *signupCellName   = @"CountryCodeCell";
    CountryCodeCell *cell = (CountryCodeCell *)[tableView dequeueReusableCellWithIdentifier:signupCellName];
    
    if (cell == nil)
    {
        cell = (CountryCodeCell*)[[[NSBundle mainBundle] loadNibNamed:@"CountryCodeCell" owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
//    {
//        code = "+226";
//        countryName = "Burkina Faso";
//        id = 16;
//    }
    NSDictionary *dict;
    if (isSearched)
    {
        dict = [arrSearchResult objectAtIndex:indexPath.row];
    }
    else
    {
        dict = [arrCountryCode objectAtIndex:indexPath.row];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        cell.lblCountryName.font = [UIFont fontWithName:@"Lato-Regular" size:27.0f];
        cell.lblCountryCode.font = [UIFont fontWithName:@"Lato-Regular" size:27.0f];
    }
    
    cell.lblCountryName.text = [dict objectForKey:@"countryName"];
    cell.lblCountryCode.text = [dict objectForKey:@"code"];
    
    if ([dict objectForKey:@"isChecked"])
    {
        cell.imgChkMark.hidden = NO;
    }
    else
    {
        cell.imgChkMark.hidden = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [currentTF resignFirstResponder];
    
    if (isSearched)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"isChecked == '1'"]];
        NSArray *filteredArray = [arrSearchResult filteredArrayUsingPredicate:predicate];
        
        if (filteredArray.count)
        {
            NSInteger index = [arrSearchResult indexOfObject:filteredArray[0]];
            NSMutableDictionary *dict = [arrSearchResult objectAtIndex:index];
            [dict removeObjectForKey:@"isChecked"];
            [arrSearchResult replaceObjectAtIndex:index withObject:dict];
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:index inSection:0], nil] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        NSMutableDictionary *dict = [arrSearchResult objectAtIndex:indexPath.row];
        [dict setObject:@"1" forKey:@"isChecked"];
        [arrSearchResult replaceObjectAtIndex:indexPath.row withObject:dict];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"isChecked == '1'"]];
        NSArray *filteredArray = [arrCountryCode filteredArrayUsingPredicate:predicate];
        
        if (filteredArray.count)
        {
            NSInteger index = [arrCountryCode indexOfObject:filteredArray[0]];
            NSMutableDictionary *dict = [arrCountryCode objectAtIndex:index];
            [dict removeObjectForKey:@"isChecked"];
            [arrCountryCode replaceObjectAtIndex:index withObject:dict];
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:index inSection:0], nil] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        NSMutableDictionary *dict = [arrCountryCode objectAtIndex:indexPath.row];
        [dict setObject:@"1" forKey:@"isChecked"];
        [arrCountryCode replaceObjectAtIndex:indexPath.row withObject:dict];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

#pragma mark - UserDefined Methods

-(void)getCountryCodeListingFromServerSide
{
    //http://192.168.0.131:7681/countries_codes
    NSString *url = [NSString stringWithFormat:@"countries_codes"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        if(responseDict==Nil)        {
            [CommonFunction showAlertWithTitle:@"" message:@"Server error" onViewController:self useAsDelegate:NO dismissBlock:nil];
            
        }
        
        else if([operation.response statusCode]==200)
        {
            
                if ([[responseDict objectForKey:@"type"]boolValue])
                {
                    if (arrCountryCode)
                    {
                        arrCountryCode = nil;
                    }
                    arrCountryCode = [NSMutableArray array];
                    arrCountryCode = [[responseDict objectForKey:@"data"]mutableCopy];
                
                    [tblViewCountryCode reloadData];
                }
            
        }
        else
        {

            [CommonFunction showAlertWithTitle:@"" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:NO dismissBlock:nil];

        }
    }
    withFailure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         [CommonFunction removeActivityIndicator];
         
         if([operation.response statusCode]  == 426)
         {
             NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
             NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
             [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                 
                 NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
             }];
         }

         else if([operation.response statusCode]  == 400 )
         {
             NSLog(@"impo response %@",operation.response);
             
             [CommonFunction showAlertWithTitle:@"" message:[operation.responseObject objectForKey:@"response"] onViewController:self useAsDelegate:NO dismissBlock:nil];
             
         }
     }];
}

#pragma mark - UITextfield Delegate Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    currentTF = textField;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)textDidChange:(id)sender
{
//    {
//        code = "+226";
//        countryName = "Burkina Faso";
//        id = 16;
//    }
    UITextField *txt = (UITextField *) sender;
    
    NSString *text = txt.text;
    if ([text isEqualToString:@""] && text.length == 0){
        NSLog(@"text length zero");
        isSearched = NO;
    }else{
        NSLog(@"having text");
        isSearched = YES;
        arrSearchResult = [NSMutableArray array];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(countryName BEGINSWITH[cd] %@) OR (code BEGINSWITH[cd] %@)",txt.text, txt.text];
        
//        NSLog(@"%@",[arrCountryCode filteredArrayUsingPredicate:predicate]);
        
        arrSearchResult = (NSMutableArray*)[[arrCountryCode filteredArrayUsingPredicate:predicate]mutableCopy];
        
        NSLog(@"arrSearchResult:: %@",arrSearchResult);
    }
    
    [tblViewCountryCode reloadData];
}

-(void)btnCheckMarkClicked
{
    NSArray *filteredArray = nil;
    if (isSearched)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"isChecked == '1'"]];
        filteredArray = [arrSearchResult filteredArrayUsingPredicate:predicate];
    }
    else
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"isChecked == '1'"]];
        filteredArray = [arrCountryCode filteredArrayUsingPredicate:predicate];
    }
    
    if (filteredArray.count)
    {
        NSArray *arr = self.navigationController.viewControllers;
        for (UIViewController *obj in arr)
        {

        if ([obj isKindOfClass:[UpdateProfileVC class]])
            {
                UpdateProfileVC *objProfile = (UpdateProfileVC *) obj;
                objProfile.strCountryCode = [[filteredArray objectAtIndex:0]objectForKey:@"code"];
                [self.navigationController popToViewController:objProfile animated:YES];
                break;
            }
        }
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"Jetseta" message:@"Select at least one country code to continue." onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
}

-(IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)checkMarkBtnnClicked:(id)sender
{
    [self btnCheckMarkClicked];
}

@end
