//
//  ForgotPassVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 12/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "ForgotPassVC.h"

@interface ForgotPassVC ()<UIGestureRecognizerDelegate>
{
    IBOutlet UITextField *txtEmailAddress;
    
}

@end

@implementation ForgotPassVC
#pragma mark - LifeCycle Method
- (void)viewDidLoad {
    [super viewDidLoad];

    [CommonFunction setNavigationBar:self.navigationController];
        [self.navigationItem setTitle:@"Forgot Password"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
  
    [self setUpView];
    self.navigationController.navigationBarHidden=NO;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
        self.navigationController.navigationBarHidden=YES;
}
#pragma mark - Initial View Setup Method
-(void)setUpView
{
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureBtnClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
}


#pragma mark - IBAction Button Method
-(void)tapGestureBtnClicked
{
    [self.view endEditing:YES];
}

- (IBAction)submitBtnClicked:(id)sender {
    [self.view endEditing:YES];
    if ([CommonFunction isValueNotEmpty:txtEmailAddress.text]) {
        if ([CommonFunction IsValidEmail:txtEmailAddress.text]) {
            if ([CommonFunction reachabiltyCheck]) {
                [CommonFunction showActivityIndicatorWithText:@""];
                [self forgotPasswordAPI];
            }
            else
            {
                [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection." onViewController:self useAsDelegate:NO dismissBlock:nil];

            }
        }
        else
        {
            [CommonFunction showAlertWithTitle:@"" message:@"Please enter valid email address." onViewController:self useAsDelegate:NO dismissBlock:nil];
            
        }
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please enter email address." onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
}


#pragma mark - WebService API
-(void)forgotPasswordAPI
{
    
    NSString *url = [NSString stringWithFormat:@"forgot_password"];
    //http://192.168.0.131:7682/forgot_password
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunction trimSpaceInString:txtEmailAddress.text],@"email", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        
        else if([operation.response statusCode]==200)
            
        {
            [CommonFunction showAlertWithTitle:@"JetSeta" message:responseDict[@"message"] onViewController:self useAsDelegate:NO dismissBlock:nil];
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
                                                  
                                                  [self forgotPasswordAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
