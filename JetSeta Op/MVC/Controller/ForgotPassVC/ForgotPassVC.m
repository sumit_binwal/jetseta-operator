//
//  ForgotPassVC.m
//  JetSeta
//
//  Created by Suchita Bohra on 13/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import "ForgotPassVC.h"
#import "LoginVC.h"

@interface ForgotPassVC ()
{
    IBOutlet UIScrollView *scrollViewForgotVc;
    IBOutlet UITextField *txtEmail;
    IBOutlet UIButton *btnSubmit;
    IBOutlet UILabel *lblHeaderTitle;
    IBOutlet UILabel *lblHeaderDesc;
    IBOutlet UILabel *lblEmailPlaceholder;
}
-(IBAction)btnSumbitClicked:(id)sender;
-(IBAction)dismissBtnClicked:(id)sender;

@end

@implementation ForgotPassVC
#pragma mark - ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self SetIBOutletUserInterface];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //    self.navigationItem.title = @"Forgot Password";
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

#pragma mark - MemoryManagement Method
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UserDefined Method
-(void)SetIBOutletUserInterface
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblHeaderTitle.font = [UIFont fontWithName:@"Lato-Regular" size:25.0];
        lblHeaderDesc.font = [UIFont fontWithName:@"Lato-Regular" size:17.0];
        lblEmailPlaceholder.font = [UIFont fontWithName:@"Lato-Bold" size:22.0];
        [btnSubmit.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:25.0]];
    }
}

#pragma mark - UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self autoScrolTextField:textField onScrollView:scrollViewForgotVc];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [scrollViewForgotVc setContentOffset:CGPointMake(0.0f, 0.0f) animated:TRUE];
    
    return YES;
}

- (void) autoScrolTextField: (UITextField *) textField onScrollView: (UIScrollView *) scrollView
{
    float slidePoint = 0.0f;
    float keyBoard_Y_Origin = self.view.bounds.size.height - 250.0f; //216
    float textFieldButtomPoint = textField.superview.frame.origin.y + (textField.frame.origin.y + textField.frame.size.height);
    
    if (keyBoard_Y_Origin < textFieldButtomPoint - scrollView.contentOffset.y) {
        slidePoint = textFieldButtomPoint - keyBoard_Y_Origin + 10.0f;
        CGPoint point = CGPointMake(0.0f, slidePoint);
        scrollView.contentOffset = point;
    }
}

#pragma mark - UserDefined Methods

-(void)removeKeyboard
{
    [txtEmail resignFirstResponder];
    [self scrollToNormalView];
}

-(void) scrollToNormalView
{
    [scrollViewForgotVc setContentOffset:CGPointMake(0.0f, 0.0f) animated:NO];
}


#pragma mark - IBAction Method
-(IBAction)btnSumbitClicked:(id)sender
{
    if (![CommonFunction reachabiltyCheck])
    {
        return;
    }
    
    if ([self validateForGotPass])
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self sendRequestForgotPassToServer];
    }
}

-(IBAction)dismissBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)validateForGotPass
{
    NSString *errormsg;
    BOOL error=FALSE;
    
    UITextField *tf;
    if (![CommonFunction isValueNotEmpty:txtEmail.text]) {
        errormsg = @"Please enter your email to continue.";
        error=TRUE;
        tf=txtEmail;
        [tf becomeFirstResponder];
    }
    else if (!error && ![CommonFunction IsValidEmail:txtEmail.text])
    {
        errormsg = @"Enter your correct email";
        error=TRUE;
        tf=txtEmail;
        [tf becomeFirstResponder];
    }
    if(error)
    {
        //        [CommonFunctions messageAlert:@"" title:errormsg delegate:self];
        [CommonFunction showAlertWithTitle:@"" message:errormsg onViewController:self useAsDelegate:NO dismissBlock:^{
            
        }];
        return FALSE;
    }
    return TRUE;
}

-(void)sendRequestForgotPassToServer
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setObject:txtEmail.text forKey:@"email"];
    
    NSString *url = [NSString stringWithFormat:@"forgot_password"];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    //    {"facebook_id":"43434333443","device_token":"434434343"}
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"Server error" withMessage:@"" withDelegate:nil];
            
        }
        else if([operation.response statusCode]==200)
        {
            if ([[responseDict valueForKey:@"type"]boolValue])
            {
                [CommonFunction showAlertWithTitle:@"" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:NO dismissBlock:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            else
            {
                [CommonFunction showAlertWithTitle:@"" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:NO dismissBlock:nil];
            }
        }
        else
        {
            [CommonFunction showAlertWithTitle:@"" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:NO dismissBlock:nil];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [CommonFunction removeActivityIndicator];
          [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         if([operation.response statusCode]  == 426)
         {
             NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
             NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
             [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                 
                 NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
             }];
         }

         else if([operation.response statusCode]  == 400 )
         {
             NSLog(@"impo response %@",operation.response);
             
             [CommonFunction showAlertWithTitle:@"" message:[operation.responseObject objectForKey:@"response"] onViewController:self useAsDelegate:NO dismissBlock:nil];
             
         }
     }];
}

@end
