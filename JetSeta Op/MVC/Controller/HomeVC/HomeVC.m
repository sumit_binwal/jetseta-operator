//
//  LoginVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 12/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "HomeVC.h"
#import "AddAircraftVC.h"
#import "AircraftListVC.h"
#import "AddCharterVC.h"
#import "LegListingVC.h"
#import "CharterListVC.h"
#import "LoginVC.h"
@interface HomeVC ()<UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    //UITextfield Outlet
    IBOutlet UIButton *btnPublish;
    IBOutlet UITextField *txtEmailAddress;
    IBOutlet UITextField *txtPassword;
    
    IBOutlet UILabel *lblHomeMsg;
    IBOutlet UIButton *btnAdd;
    
    IBOutlet UIButton *btnLogin;
}
@end

@implementation HomeVC
@synthesize strPublish;
#pragma mark - ViewController Life Cycle Method

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupView];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self getAircraftStatus];
}

#pragma mark - SetupView
-(void)setupView
{
    [CommonFunction setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Home"];
    
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"AL_Menu"]style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureBtnClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    
    
    if(IPAD)
    {
        btnPublish.titleLabel.font=[UIFont fontWithName:btnPublish.titleLabel.font.fontName size:24];
        [lblHomeMsg setFont:[UIFont fontWithName:lblHomeMsg.font.fontName size:30]];

    }
    else
    {

        btnPublish.titleLabel.font=[UIFont fontWithName:btnPublish.titleLabel.font.fontName size:btnPublish.titleLabel.font.pointSize*SCREEN_XScale];
        [lblHomeMsg setFont:[UIFont fontWithName:lblHomeMsg.font.fontName size:lblHomeMsg.font.pointSize*SCREEN_XScale]];
    }
}

#pragma mark - IBAction Button
-(void)tapGestureBtnClicked
{
    [self.view endEditing:YES];
}
- (IBAction)addLegBtnClicked:(id)sender {
    LegListingVC *acvc=[[LegListingVC alloc]initWithNibName:NSStringFromClass([LegListingVC class]) bundle:nil];
    self.tabBarController.tabBar.hidden = YES;
    
    [self.navigationController pushViewController:acvc animated:YES];
    acvc.hidesBottomBarWhenPushed = YES;

//    [self presentViewController:acvc animated:YES completion:nil];

}
- (IBAction)addCharterBtnClicked:(id)sender {
    
}

- (IBAction)publishAirCraftBtnClicked:(id)sender {
    
    if (btnPublish.tag==0) {
        AddAircraftVC *acvc=[[AddAircraftVC alloc]initWithNibName:NSStringFromClass([AddAircraftVC class]) bundle:nil];
        [self presentViewController:acvc animated:YES completion:nil];

    }
    else
    {
        AddCharterVC *acvc=[[AddCharterVC alloc]initWithNibName:NSStringFromClass([AddCharterVC class]) bundle:nil];
        
        [self.navigationController pushViewController:acvc animated:YES];
    }
    


}
- (IBAction)aircraftListingBtnCLicked:(id)sender {
    
    AircraftListVC *alvc=[[AircraftListVC alloc]initWithNibName:NSStringFromClass([AircraftListVC class]) bundle:nil];
    [self.navigationController pushViewController:alvc animated:YES];
}


#pragma mark - WebServiceApi
-(void)getAircraftStatus
{
    
    NSString *url = [NSString stringWithFormat:@"my_profile/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
    //   http://192.168.0.131:7682/my_profile/83a595e384926272ad5cf8a48208d486
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
        {
           
                [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"data"] valueForKey:@"is_publish_aircraft"] forKey:kPUBLISH_AIRCRAFT];
                [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"data"] valueForKey:@"is_publish_charter"] forKey:kPUBLISH_CHARTER];
                [NSUSERDEFAULTS synchronize];
            
                BOOL aircraft=[[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue];
                BOOL charter=[[NSUSERDEFAULTS valueForKey:kPUBLISH_CHARTER]boolValue];
            
                if (aircraft && charter) {
                
                    [APPDELEGATE changeRootViewController:YES aircraftPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue] charterPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_CHARTER]boolValue]];
                }
                else
                    {
                        if (aircraft)
                        {
                            [btnPublish setTitle:@"   Create charter" forState:UIControlStateNormal];
                            btnPublish.tag=1;

                        }
                        else
                        {
                            btnPublish.tag=0;
                        }
                    }

        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                                    [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  
                                                  [self getAircraftStatus];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}
/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
