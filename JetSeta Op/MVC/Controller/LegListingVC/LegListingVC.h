//
//  LegListingVC.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 14/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol sendCharterID <NSObject>

-(void)sendCharterID:(NSString *)strCharterID aircraftId:(NSString *)aircrftID;
@end
@interface LegListingVC : UIViewController
{
    
}
@property(nonatomic,strong)NSMutableDictionary *dictCharterDetail;
@property(nonatomic,assign)id delegate;
@property(nonatomic,strong)NSString *strCharterID;

@property(nonatomic, readwrite) BOOL isComesFromMyCharterListing;
@end
