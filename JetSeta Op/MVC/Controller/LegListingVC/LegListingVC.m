//
//  LegListingVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 14/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "LegListingVC.h"
#import "LegListingCell.h"
#import "AddCharterVC.h"
#import "ImageCollectionCell.h"
#import "AircraftDetailVC.h"
#import "UpdateCharterVC.h"
#import "LoginVC.h"

@interface LegListingVC ()<UITableViewDataSource,UITableViewDelegate>
{
    
    IBOutlet UIPageControl *pgCntrol;
    IBOutlet UILabel *lblLegsCount;
    IBOutlet UILabel *lblAIrcraftName;
    IBOutlet UITableView *tblVw;
    NSMutableArray *arrLegs;
    IBOutlet UILabel *lblLegs;
    NSMutableArray *arrPhotos;
    IBOutlet UIButton *btnAddLegs;
    IBOutlet UICollectionView *collectionImgVw;
    
    UIBarButtonItem *rightDeleteBtn;
}
@end

@implementation LegListingVC
@synthesize delegate,strCharterID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
    
    rightDeleteBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"delete_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(deleteCharterBtnClicked)];
    
    self.navigationItem.rightBarButtonItem = rightDeleteBtn;
    
    [self getCharterDetail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [APPDELEGATE.mainTapBarCntroller.tabBar setHidden:YES];
    
    if (IPAD) {
        [lblLegs setFont:[UIFont fontWithName:lblLegs.font.fontName size:27]];
        [lblLegsCount setFont:[UIFont fontWithName:lblLegsCount.font.fontName size:27]];
        [lblAIrcraftName setFont:[UIFont fontWithName:lblAIrcraftName.font.fontName size:27]];
        btnAddLegs.titleLabel.font=[UIFont fontWithName:btnAddLegs.titleLabel.font.fontName size:22];
    }
    else
    {
        [lblLegs setFont:[UIFont fontWithName:lblLegs.font.fontName size:15*SCREEN_XScale]];
        [lblLegsCount setFont:[UIFont fontWithName:lblLegsCount.font.fontName size:15*SCREEN_XScale]];
        [lblAIrcraftName setFont:[UIFont fontWithName:lblAIrcraftName.font.fontName size:15*SCREEN_XScale]];
        btnAddLegs.titleLabel.font=[UIFont fontWithName:btnAddLegs.titleLabel.font.fontName size:12*SCREEN_XScale];
    }
 
    if ([CommonFunction reachabiltyCheck]) {
        
        [CommonFunction showActivityIndicatorWithText:@""];
        [self getCharterDetail];
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection" onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
}

-(void)setUpView
{
    [CommonFunction setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Charter Details"];
    UIBarButtonItem *barBtn1=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_icon"] style:UIBarButtonItemStyleDone target:self action:@selector(leftButtonClicked:)];
    
    [self.navigationItem setLeftBarButtonItem:barBtn1];
    
    
    
    [collectionImgVw registerNib:[UINib nibWithNibName:NSStringFromClass([ImageCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:@"ImageCollectionCell"];
    tblVw.estimatedRowHeight=70*SCREEN_YScale;
    tblVw.rowHeight = UITableViewAutomaticDimension;


}

#pragma mark - IBAction Method
- (IBAction)leftBtnClicked:(UIButton *)sender {
    
    if (pgCntrol.currentPage==arrPhotos.count) {
        
    }
    else
    {
        if (pgCntrol.currentPage<=0) {
            
        }
        else
        {
        [collectionImgVw setContentOffset:CGPointMake(collectionImgVw.frame.size.width*(pgCntrol.currentPage-1), 0) animated:YES];
        }
        
        
    }
}
- (IBAction)infoBtnClicked:(id)sender {
    AircraftDetailVC *advc=[[AircraftDetailVC alloc]initWithNibName:NSStringFromClass([AircraftDetailVC class]) bundle:nil];
    advc.strAircraftID=_dictCharterDetail[@"aircraft"][0][@"aircraft_id"];
    [self.navigationController pushViewController:advc animated:YES];
    
    
}
- (IBAction)rightBtnClicked:(UIButton *)sender {
    
    if (pgCntrol.currentPage==arrPhotos.count-1) {
        
    }
    else
    {
        [collectionImgVw setContentOffset:CGPointMake(collectionImgVw.frame.size.width*(pgCntrol.currentPage+1), 0) animated:YES];
        
    }
    
  }

- (IBAction)addLegBtnClicked:(id)sender {
    
    AddCharterVC *acvc=[[AddCharterVC alloc]initWithNibName:NSStringFromClass([AddCharterVC class]) bundle:nil];
    acvc.strCharterLegID=[[[_dictCharterDetail objectForKey:@"aircraft"]objectAtIndex:0]objectForKey:@"charter_id"];
    acvc.strAircraftName=lblAIrcraftName.text;
    [delegate sendCharterID:[[[_dictCharterDetail objectForKey:@"aircraft"]objectAtIndex:0]objectForKey:@"charter_id"] aircraftId:[[[_dictCharterDetail objectForKey:@"aircraft"]objectAtIndex:0]objectForKey:@"aircraft_id"]];
    acvc.strAircraftId=[[[_dictCharterDetail objectForKey:@"aircraft"]objectAtIndex:0]objectForKey:@"aircraft_id"];
    [self.navigationController pushViewController:acvc animated:YES];
//        [delegate sendCharterID:[[[_dictCharterDetail objectForKey:@"aircraft"]objectAtIndex:0]objectForKey:@"charter_id"]];

}

#pragma mark - UICollectionView Delegate
#pragma mark - UICollection View Delegate Method
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return arrPhotos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectioView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCollectionCell   *cell = [collectioView dequeueReusableCellWithReuseIdentifier:@"ImageCollectionCell" forIndexPath:indexPath];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(ImageCollectionCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    pgCntrol.currentPage=indexPath.row;
    [cell.imgVw setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",arrPhotos[indexPath.row][@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    return CGSizeMake(self.view.frame.size.width,150*SCREEN_YScale);
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - UserDefineMethod
-(IBAction)leftButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - TableView Delegate Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return   arrLegs.count;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    return 70*SCREEN_YScale;
//    
//    
//}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString * cellIdentifier=@"LegListingCell";
    
    LegListingCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[LegListingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(LegListingCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.lblCharter.text=[arrLegs[indexPath.row][@"charter_type_value"]uppercaseString];
    cell.lblPrice.text=[NSString stringWithFormat:@"%@ /hr",arrLegs[indexPath.row][@"price_per_hour"]];
    cell.lblTitle.text=[NSString stringWithFormat:@"%@-%@",arrLegs[indexPath.row][@"source_airport"],arrLegs[indexPath.row][@"destination_airport"]];
    cell.lblTotalCost.text=[NSString stringWithFormat:@"Total Cost(USD) : %@",arrLegs[indexPath.row][@"total_price"]];
    if ([arrLegs[indexPath.row][@"status"]boolValue]) {
        cell.lblAvlble.text=@"AVAILABLE";
        [cell.lblAvlble setTextColor:[UIColor colorWithRed:100.0f/255.0f green:198.0f/255.0f blue:62.0f/255.0f alpha:1]];
    }
    else
    {
        cell.lblAvlble.text=@"Not available";
        [cell.lblAvlble setTextColor:[UIColor colorWithRed:240.0f/255.0f green:101.0f/255.0f blue:32.0f/255.0f alpha:1]];
    }
    
    NSString *currentDateString=[NSString stringWithFormat:@"%@",arrLegs[indexPath.row][@"departure_datetime"]];
    NSLog(@"currentDateString: %@", currentDateString);
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    NSDate *date = [dateFormat dateFromString:currentDateString];

    // Convert date object to desired output format
    
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    cell.lblMnth.text=[dateFormat stringFromDate:date];
    
    NSDateFormatter *timeFormate=[[NSDateFormatter alloc]init];
    
    [timeFormate setDateFormat:@"hh:mm a"];
    cell.lbltime.text=[timeFormate stringFromDate:date];
    
    NSLog(@"%@",_dictCharterDetail);

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UpdateCharterVC *ucvc=[[UpdateCharterVC alloc]initWithNibName:NSStringFromClass([UpdateCharterVC class]) bundle:nil];
    ucvc.strCharterLegID=arrLegs[indexPath.row][@"id"];
    ucvc.strAircraftName=_dictCharterDetail[@"aircraft"][0][@"name"];
    ucvc.strLegID=arrLegs[indexPath.row][@"id"];
    ucvc.dictData=arrLegs[indexPath.row];
    [self.navigationController pushViewController:ucvc animated:YES];
}

#pragma mark - WebService API
-(void)getCharterDetail
{
    
    NSString *url = [NSString stringWithFormat:@"charter_detail/%@/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN],strCharterID];
    //    http://192.168.0.131:7684/charter_detail/83a595e384926272ad5cf8a48208d486/2
    
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
        {
            if (_isComesFromMyCharterListing)
            {
                if ([[responseDict objectForKey:@"aircraft"]isKindOfClass:[NSArray class]])
                {
                    if ([[[[responseDict objectForKey:@"aircraft"]objectAtIndex:0]objectForKey:@"is_booked"]boolValue])
                    {
//                        1=user can't delete charter
                        self.navigationItem.rightBarButtonItem = nil;
                    }
                    else
                    {
//                        0=user can delete charter
                        self.navigationItem.rightBarButtonItem = rightDeleteBtn;

                    }
                }
                else if ([[responseDict objectForKey:@"aircraft"]isKindOfClass:[NSDictionary class]])
                {
                    if ([[[responseDict objectForKey:@"aircraft"]objectForKey:@"is_booked"]boolValue])
                    {
//                        1=user can't delete charter
                        self.navigationItem.rightBarButtonItem = nil;
                    }
                    else
                    {
//                        0=user can delete charter
                        self.navigationItem.rightBarButtonItem = rightDeleteBtn;
                    }
                }
            }
            
                arrPhotos=[[NSMutableArray alloc]init];
                arrLegs=[[NSMutableArray alloc]init];
                self.dictCharterDetail=[responseDict mutableCopy];
                arrLegs=_dictCharterDetail[@"legs"];
                arrPhotos=_dictCharterDetail[@"aircraft_images"];
                
                lblLegsCount.text=[NSString stringWithFormat:@"(%lu)",(unsigned long)arrLegs.count];
                if (arrLegs.count>1) {
                    lblLegs.text=@"Trips";
                }
                else
                {
                    lblLegs.text=@"Trip";
                }
                lblAIrcraftName.text=[_dictCharterDetail[@"aircraft"][0][@"name"]uppercaseString];
                pgCntrol.numberOfPages=arrPhotos.count;
                [tblVw reloadData];
                
                
                arrPhotos=[arrPhotos mutableCopy];
                NSArray *tempArray= [arrPhotos sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES]]];
                [arrPhotos removeAllObjects];
                [arrPhotos addObjectsFromArray:[tempArray mutableCopy]];
                
                [collectionImgVw reloadData];
                
                tblVw.estimatedRowHeight=70*SCREEN_YScale;
                tblVw.rowHeight = UITableViewAutomaticDimension;
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                                    [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  
                                                  [self getCharterDetail];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void)deleteCharterBtnClicked
{
    if ([CommonFunction reachabiltyCheck])
    {
        [CommonFunction showActivityIndicatorWithText:@""];
        
        
        //    GET /delete_charter/{SID}/{charter_id}
        NSString *url = [NSString stringWithFormat:@"delete_charter/%@/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN], strCharterID];
        
        NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
        
        ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
        
        [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
            NSLog(@"responce dict %@",responseDict);
            
            [CommonFunction removeActivityIndicator];
            
            if(responseDict==Nil)
            {
                [CommonFunction alertTitle:@"" withMessage:@"Server error"];
            }
            else if([operation.response statusCode]==200)
            {
                [CommonFunction showAlertWithTitle:@"" message:@"Charter deleted successfully." onViewController:self useAsDelegate:NO dismissBlock:^{
                    
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            else
            {
                [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
            }
        }
                                          withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              
                                              [CommonFunction removeActivityIndicator];
                                              
                                              if([operation.response statusCode]  == 426)
                                              {
                                                  NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                                  NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                                  [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                      
                                                      NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                                  }];
                                              }

                                              else if([operation.response statusCode]  == 400 )
                                              {
                                                  NSLog(@"impo response %@",operation.response);
                                                  
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                                  
                                              }
                                              else{
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                                  
                                              }
                                              
                                          }];
        
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection. " onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
}

@end
