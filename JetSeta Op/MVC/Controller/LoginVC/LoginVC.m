//
//  LoginVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 12/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "LoginVC.h"
#import "ForgotPassVC.h"
#import "UtilityClass.h"
#import "TutorialVC.h"
@interface LoginVC ()<UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    //UITextfield Outlet
    
    IBOutlet UILabel *lblHeaderTitle;
    IBOutlet UILabel *lblHeaderDesc;
    IBOutlet UILabel *lblEmailPlaceholder;
    IBOutlet UILabel *lblPassPlaceholder;
    IBOutlet UILabel *lblText;
    IBOutlet UIButton *btnForgotPassword;
    IBOutlet UIButton *btnLogin;
    IBOutlet UIButton *btnFbLogin;
    IBOutlet UIButton *btnSignup;
    
    IBOutlet UITextField *txtEmailAddress;
    IBOutlet UITextField *txtPassword;
}
@end

@implementation LoginVC

#pragma mark - ViewController Life Cycle Method

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    [self dummyDataFill];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self SetIBOutletUserInterface];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UserDefined Methods
-(void)dummyDataFill
{
#if TARGET_IPHONE_SIMULATOR
    txtEmailAddress.text=@"operator@gmail.com";
    txtPassword.text=@"123456";
#else
    //DLog(@"This is device mode....");
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You denied to use notification. Please on notification from your phone's settings." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //        [alert show];
#endif
}

-(void)SetIBOutletUserInterface
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lblHeaderTitle.font = [UIFont fontWithName:@"Lato-Regular" size:25.0];
        lblHeaderDesc.font = [UIFont fontWithName:@"Lato-Regular" size:17.0];
        lblEmailPlaceholder.font = [UIFont fontWithName:@"Lato-Bold" size:22.0];
        lblPassPlaceholder.font = [UIFont fontWithName:@"Lato-Bold" size:22.0];
        lblText.font = [UIFont fontWithName:@"Lato-Regular" size:25.0];
        
        [btnForgotPassword.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:22.0]];
        [btnLogin.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:27.0]];
        [btnFbLogin.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:22.0]];
        [btnSignup.titleLabel setFont:[UIFont fontWithName:@"Lato-Bold" size:25.0]];
    }
}
-(void)setupView
{
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureBtnClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
}

#pragma mark - IBAction Button
-(void)tapGestureBtnClicked
{
    [self.view endEditing:YES];
}

- (IBAction)loginBtnClicked:(id)sender {
    if ([self validateTextField]) {
        if ([CommonFunction reachabiltyCheck]) {
            [CommonFunction showActivityIndicatorWithText:@""];

            [self loginAPI];
        }
        else
        {
            [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection. " onViewController:self useAsDelegate:nil dismissBlock:nil];
        }
    }
}
- (IBAction)forgotPasswordBtnClicked:(id)sender {
    [self.view endEditing:YES];
//  [CommonFunction showActivityIndicatorWithText:@""];
    ForgotPassVC *fpvc=[[ForgotPassVC alloc]initWithNibName:NSStringFromClass([ForgotPassVC class]) bundle:nil];
    [self.navigationController pushViewController:fpvc animated:YES];
}


#pragma mark - UITextField Delegate Method
-(BOOL)validateTextField
{
    if (![CommonFunction isValueNotEmpty:txtEmailAddress.text]) {
        [CommonFunction showAlertWithTitle:nil message:@"Please enter email address." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return NO;
    }
    else if (![CommonFunction IsValidEmail:txtEmailAddress.text]) {
        [CommonFunction showAlertWithTitle:nil message:@"Please enter valid email address." onViewController:self useAsDelegate:nil dismissBlock:nil];
        return NO;
    }
    else if (![CommonFunction isValueNotEmpty:txtPassword.text]) {
        [CommonFunction showAlertWithTitle:nil message:@"Please enter password." onViewController:self useAsDelegate:nil dismissBlock:nil];
        return NO;
    }
    else if (txtPassword.text.length<6) {
        [CommonFunction showAlertWithTitle:nil message:@"Password must be at least six characters long." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return NO;
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([txtEmailAddress isFirstResponder])
    {
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage])
        {
            return NO;
        }
    }
    else if ([txtPassword isFirstResponder])
    {
        if([string isEqualToString:@" "])
        {
            return NO;
        }
    }


    return YES;
   
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtEmailAddress) {
        [txtPassword becomeFirstResponder];
    }
    else if (textField==txtPassword)
    {
        [txtPassword resignFirstResponder];
    }
    return YES;
}

#pragma mark - WebService API
-(void)loginAPI
{
    NSString *url = [NSString stringWithFormat:@"login"];
    //http://192.168.0.131:7682/login
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunction trimSpaceInString:txtEmailAddress.text],@"email",[CommonFunction trimSpaceInString:txtPassword.text],@"password",pushDeviceToken,@"device_token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
        {
            if ([[responseDict valueForKey:@"type"]boolValue]) {
                
                [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"sid"] forKey:kUSERTOKEN];
                [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"data"] valueForKey:@"is_publish_aircraft"] forKey:kPUBLISH_AIRCRAFT];
                NSString *strFullName=[NSString stringWithFormat:@"%@ %@",[[responseDict valueForKey:@"data"] valueForKey:@"first_name"],[[responseDict valueForKey:@"data"] valueForKey:@"last_name"]];
                [NSUSERDEFAULTS setObject:strFullName forKey:kOperatorName];
                [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"data"] valueForKey:@"is_publish_charter"] forKey:kPUBLISH_CHARTER];
                [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"data"] valueForKey:@"published_aircraft_num"] forKey:kPUBLISH_COUNT];
    
                [UtilityClass saveUserInfoWithDictionary: [responseDict objectForKey:@"data"]];
                [NSUSERDEFAULTS synchronize];
                
               
                [APPDELEGATE changeRootViewController:YES aircraftPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue] charterPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_CHARTER]boolValue]];

            }

            else
            {
                [CommonFunction showAlertWithTitle:@"Jetseta" message:responseDict[@"message"] onViewController:self useAsDelegate:NO dismissBlock:nil];
            }
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                  [CommonFunction removeActivityIndicator];
                                           NSLog(@"impo response %@",operation.responseObject);
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }
                                          else if([operation.response statusCode]  == 400 ){
                                             
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
                                                  
                                                  [self loginAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
