//
//  MainMenuLeftVC.m
//  JetSeta
//
//  Created by Suchita Bohra on 15/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import "MainMenuLeftVC.h"
#import "SWRevealViewController.h"
#import "WebViewVC.h"
//#import "JetSeta_Constants.h"
#import "LoginVC.h"
#import "TutorialVC.h"
#import "MainMenuCell.h"
#import "SettingVC.h"
#import "CharterListVC.h"
#import <MessageUI/MessageUI.h>
@interface MainMenuLeftVC ()<UITableViewDataSource, UITableViewDelegate,SWRevealViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    IBOutlet UITableView *tblViewMenuLeft;
    NSArray *arrMenuLeft;
    IBOutlet UILabel *lblOperatorName;
    IBOutlet UILabel *lblWelcome;
    IBOutlet UILabel *lblVersin;
}

@end

@implementation MainMenuLeftVC

#pragma mark - ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden=YES;
    lblOperatorName.text=[[NSUSERDEFAULTS valueForKey:kOperatorName]capitalizedString];
    arrMenuLeft = [[NSArray alloc]initWithObjects:@"Home",@"Settings", @"Terms and Conditions", @"Privacy", @"About Us", @"contact us", @"TAKE A TOUR",@"log out", nil];
    
    
    if (IPAD) {
        [lblOperatorName setFont:[UIFont fontWithName:lblOperatorName.font.fontName size:38.0f]];
        [lblWelcome setFont:[UIFont fontWithName:lblWelcome.font.fontName size:22.0f]];
    }
    else
    {
        [lblOperatorName setFont:[UIFont fontWithName:lblOperatorName.font.fontName size:21.0f*SCREEN_XScale]];
        [lblWelcome setFont:[UIFont fontWithName:lblWelcome.font.fontName size:12.0f*SCREEN_XScale]];
    }
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    lblVersin.text=[NSString stringWithFormat:@"V %@",appVersion];
}


#pragma mark - MemoryManagement Method

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:NO];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:YES];
}

#pragma mark - UITableView DataSource and Delegate.

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 50.0f*SCREEN_YScale;
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return arrMenuLeft.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *mainMenuLeft   = @"MainMenuCell";
    MainMenuCell *cell = (MainMenuCell *)[tableView dequeueReusableCellWithIdentifier:mainMenuLeft];
    
    if (cell == nil)
    {
        cell = [[MainMenuCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:mainMenuLeft];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.backgroundColor=[UIColor clearColor];
    
    //    cell.textLabel.text = ;
    
    cell.lblMenuItem.text=[[arrMenuLeft objectAtIndex:indexPath.row] uppercaseString];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tblViewMenuLeft reloadData];
    MainMenuCell *cell=[tblViewMenuLeft cellForRowAtIndexPath:indexPath ];
    
    [cell.lblMenuItem setTextColor:[UIColor colorWithRed:18.0f/255.0f green:111.0f/255.0f blue:211.0f/255.0f alpha:1]];
    
    if (IPAD) {
        [cell.lblMenuItem setFont:[UIFont fontWithName:@"Lato-Bold" size:26.0f]];
    }
    else
    {
        [cell.lblMenuItem setFont:[UIFont fontWithName:@"Lato-Bold" size:15.0f*SCREEN_XScale]];
    }
    
    
    cell.backgroundColor=[UIColor whiteColor];
    WebViewVC *wvc=[[WebViewVC alloc]initWithNibName:NSStringFromClass([WebViewVC class]) bundle:nil];
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:wvc];
    SWRevealViewController *revealController = [self revealViewController];
    [revealController tapGestureRecognizer];
    [revealController panGestureRecognizer];
    
    switch (indexPath.row) {
        case 0:
        {
            CharterListVC *wvc=[[CharterListVC alloc]initWithNibName:NSStringFromClass([CharterListVC class]) bundle:nil];
            UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:wvc];
            
            [revealController pushFrontViewController:APPDELEGATE.mainTapBarCntroller animated:YES];
            break;
        }
        case 1:
        {
            SettingVC *wvc=[[SettingVC alloc]initWithNibName:NSStringFromClass([SettingVC class]) bundle:nil];
            UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:wvc];
            
            [revealController pushFrontViewController:navController animated:YES];
            
            break;
        }
        case 2:
        {
            wvc.strUrl=@"https://myjetseta.com/pages/terms";
            wvc.strNavigationTitle=@"Terms and conditions";
            
            [revealController pushFrontViewController:navController animated:YES];
            
            break;
        }
        case 3:
        {
            wvc.strUrl=@"https://myjetseta.com/pages/privacy";
            wvc.strNavigationTitle=@"Privacy";
            
            [revealController pushFrontViewController:navController animated:YES];
            break;
        }
        case 4:
        {
            wvc.strUrl=@" https://myjetseta.com/pages/about_us";
            wvc.strNavigationTitle=@"About Us";
            
            [revealController pushFrontViewController:navController animated:YES];
            
            break;
        }
        case 5:
        {
            
            MFMailComposeViewController *comp=[[MFMailComposeViewController alloc]init];
            [comp setMailComposeDelegate:self];
            if([MFMailComposeViewController canSendMail]) {
                [comp setToRecipients:[NSArray arrayWithObjects:@"enquiries@jetseta.com", nil]];
                [comp setSubject:@"Contact Us"];
                
                //                [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                [self.revealViewController.frontViewController.view setUserInteractionEnabled:YES];

                [self presentViewController:comp animated:YES completion:nil];
                
            }
//            else {
//                UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"" message:@"" delegate:nil cancelButtonTitle:@"" otherButtonTitles:nil, nil];
//                [alrt show];
//            }
            //            wvc.strUrl=@"http://202.157.76.19/jetseta/pages/contact_us";
            //            wvc.strNavigationTitle=@"Contact Us";
            //
            //            [revealController pushFrontViewController:navController animated:YES];
            
            break;
        }
        case 6:
        {
            TutorialVC *wvc=[[TutorialVC alloc]initWithNibName:NSStringFromClass([TutorialVC class]) bundle:nil];
            UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:wvc];
            
            [revealController pushFrontViewController:navController animated:YES];
            
            break;
        }
        case 7:
        {
            UIAlertController *alertCntroller=[UIAlertController alertControllerWithTitle:@"Are you sure?" message:@"Do you want to logout from Jetseta?" preferredStyle:UIAlertControllerStyleAlert];
            [alertCntroller addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self logoutUser];
                
                
            }]];
            [alertCntroller addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [self presentViewController:alertCntroller animated:YES completion:nil];
            break;
            
        }
            
        default:
            break;
    }
}

#pragma mark - MFMailComposer Method

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    if(error) {
//        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"" message:@"" delegate:nil cancelButtonTitle:@"" otherButtonTitles:nil, nil];
//        [alrt show];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    SWRevealViewController *revealController = [self revealViewController];
    [revealController tapGestureRecognizer];
    [revealController panGestureRecognizer];
    CharterListVC *wvc=[[CharterListVC alloc]initWithNibName:NSStringFromClass([CharterListVC class]) bundle:nil];
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:wvc];
    [tblViewMenuLeft reloadData];
        [revealController pushFrontViewController:APPDELEGATE.mainTapBarCntroller animated:YES];
}

#pragma mark - WebService API 
-(void)logoutUser
{
    
    NSString *url = [NSString stringWithFormat:@"logout/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
    //    http://192.168.0.131:7684/POST /logout/{SID}
    
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
    
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
            
        {
            if ([[responseDict valueForKey:@"type"]boolValue])
            {
                LoginVC *fufs = [[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil];
                APPDELEGATE.navigationController=[[UINavigationController alloc]initWithRootViewController:fufs];
                self.navigationController.navigationBarHidden=YES;
                [APPDELEGATE.window.window setRootViewController:self.navigationController];
                [NSUSERDEFAULTS removeObjectForKey:@"previousSelectedVC"];
                [NSUSERDEFAULTS removeObjectForKey:kUSERNAME];
                [NSUSERDEFAULTS removeObjectForKey:kOperatorName];
                [NSUSERDEFAULTS removeObjectForKey:kUSERTOKEN];
                [NSUSERDEFAULTS removeObjectForKey:kPUBLISH_AIRCRAFT];
                [NSUSERDEFAULTS removeObjectForKey:kPUBLISH_CHARTER];
                [NSUSERDEFAULTS synchronize];
                [CommonFunction changeRootViewController:NO aircraftPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue] charterPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_CHARTER]boolValue]];
            }
            else
            {
                [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
                self.navigationController.navigationBar.userInteractionEnabled = YES;
            }
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
            self.navigationController.navigationBar.userInteractionEnabled = YES;
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          self.navigationController.navigationBar.userInteractionEnabled = YES;
                                          [CommonFunction removeActivityIndicator];
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  
                                                  [self logoutUser];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}



@end
