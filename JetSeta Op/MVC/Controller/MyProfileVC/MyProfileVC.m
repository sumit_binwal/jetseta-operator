//
//  LoginVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 12/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "MyProfileVC.h"
#import "LoginVC.h"
@interface MyProfileVC ()
{

}
@end

@implementation MyProfileVC

#pragma mark - ViewController Life Cycle Method

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view from its nib.
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Button methods
- (IBAction)logoutBtnClicked:(id)sender {
    LoginVC *fufs=[[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil];
    APPDELEGATE.navigationController=[[UINavigationController alloc]initWithRootViewController:fufs];
    self.navigationController.navigationBarHidden=YES;
    [APPDELEGATE.window.window setRootViewController:self.navigationController];
    [NSUSERDEFAULTS removeObjectForKey:@"previousSelectedVC"];
    [NSUSERDEFAULTS removeObjectForKey:kUSERTOKEN];

    [CommonFunction changeRootViewController:NO aircraftPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue] charterPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_CHARTER]boolValue]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
