//
//  LoginVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 12/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "NotificationVC.h"
#import "LoginVC.h"
#import "NotificationCell.h"
#import "LegListingVC.h"
#import "NotificationAddCharterVC.h"
@interface NotificationVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrNotificatinFeeds;
    IBOutlet UITableView *tblVw;
    int currentPage,totalpost;
    IBOutlet UILabel *lblMsgError;
    BOOL isNextPage;
}
@end

@implementation NotificationVC

#pragma mark - ViewController Life Cycle Method

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupView];
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
    tblVw.estimatedRowHeight=90*SCREEN_YScale;
     tblVw.rowHeight=UITableViewAutomaticDimension;
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:YES];
    [APPDELEGATE.mainTapBarCntroller.tabBar setHidden:NO];
    
    if ([CommonFunction reachabiltyCheck]) {
        currentPage=1;
        isNextPage=1;

       // [CommonFunction showActivityIndicatorWithText:@""];
        [self getNotificationList];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection. "];
    }
    
}

#pragma mark - SetupView

-(void)setupView
{
    [CommonFunction setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Notification"];
    

    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"AL_Menu"]style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;

    }


#pragma mark - IBACtion BtnClicked
-(IBAction)acceptNotificationClicked:(UIButton *)sender
{
    
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        [self acceptNotificationRequest:arrNotificatinFeeds[sender.tag][@"id"]];
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection. " onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
}
-(IBAction)declineNotificationClicked:(UIButton *)sender
{
    NSLog(@"Accepted");
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        [self declineNotificationRequest:arrNotificatinFeeds[sender.tag][@"id"]];
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection. " onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
}
#pragma mark - TableView Delegate Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return   arrNotificatinFeeds.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if ([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"interest"])
//    {
//        tableView.estimatedRowHeight=90*SCREEN_YScale;
//        return tableView.rowHeight=UITableViewAutomaticDimension;
//    }
//    else if([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"booked"])
//    {
//        tableView.estimatedRowHeight=90*SCREEN_YScale;
//        return tableView.rowHeight=UITableViewAutomaticDimension;
////        return 107*SCREEN_YScale;
//    }
//    else if([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"admin"])
//        
//    {
////        return 70*SCREEN_YScale;
//
//    }
//    else
//    {
//        return 107*SCREEN_YScale;
//    }
    tblVw.estimatedRowHeight=90*SCREEN_YScale;
    return tblVw.rowHeight=UITableViewAutomaticDimension;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSInteger cellValue = 0;
    NSString *cellIdentifier=@"NotificationCell";

    
     if ([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"booked"])
    {
        cellIdentifier=@"BookedCell";
        cellValue=1;
    }
    else if([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"admin"])
        
    {
        cellValue = 2;
        cellIdentifier=@"AdminCell";
    }
    else if ([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"interest"])
    {
        cellValue=3;
        cellIdentifier=@"InterestCell";
    }

    
    NotificationCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell)
    {
        cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([NotificationCell class]) owner:self options:nil][cellValue];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    
    if ([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"booked"]||[arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"interest"])
    {
        if ([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"booked"]) {
            cell.lblBookType.text=@"Booking Confirmed!";
            
        }
        else if([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"interest"])
        {
            cell.lblBookType.text=@"Interest of Customer!";
            cell.lblAircraftType.text=arrNotificatinFeeds[indexPath.row][@"charter_type"];
            
        }
        
    }
    
    if ([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"interest"])
    {
        NSString *curentDateString=arrNotificatinFeeds[indexPath.row][@"departure_datetime"];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        //2016-05-15
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        //                        [dateFormat setDateFormat:@"MMM dd yyyy"];
        NSDate *date = [dateFormat dateFromString:curentDateString];
        NSLocale *deLocale = [NSLocale currentLocale];
        dateFormat.locale=deLocale;
        // Convert date object to desired output format
        
        [dateFormat setDateFormat:@"MMM dd yyyy"];
        curentDateString = [dateFormat stringFromDate:date];
        
//        NSLog(@"%@",arrNotificatinFeeds[indexPath.row][@"max_price"]);
//        
        cell.lblDepartureDate.text=[NSString stringWithFormat:@"Departure : %@ EST",arrNotificatinFeeds[indexPath.row][@"departure_datetime"]];
        
//        cell.lblArrivalDate.text=[NSString stringWithFormat:@"Arrival : %@ EST",arrNotificatinFeeds[indexPath.row][@"arrival_datetime"]];
//
        if ([[[arrNotificatinFeeds objectAtIndex:indexPath.row]objectForKey:@"charter_type"]isEqualToString:@"Full Charter"])
        {
            cell.lblArrivalDate.text=[NSString stringWithFormat:@"Return : %@ EST",arrNotificatinFeeds[indexPath.row][@"arrival_datetime"]];
        }
        else
        {
            cell.lblArrivalDate.text = [NSString stringWithFormat:@"Return : -"];
        }
        
        NSInteger maxPrice = 0;
        NSInteger minPrice = 0;
        
        maxPrice = [arrNotificatinFeeds[indexPath.row][@"max_price"]integerValue];
        minPrice = [arrNotificatinFeeds[indexPath.row][@"min_price"]integerValue];
        
        cell.lblPriceunit.text=[NSString stringWithFormat:@"Price Range : $%ld to $%ld",(long)minPrice,(long)maxPrice];
//        cell.lblPriceunit.text=[NSString stringWithFormat:@"Price Range : $%@ to $%@",arrNotificatinFeeds[indexPath.row][@"min_price"],arrNotificatinFeeds[indexPath.row][@"max_price"]];

    }
    else
    {
        cell.lblDepartureTime.text=[CommonFunction convertCustomeTime:arrNotificatinFeeds[indexPath.row][@"departure_datetime"]];
        cell.lblDepartureDate.text=[CommonFunction convertCustomeDate:arrNotificatinFeeds[indexPath.row][@"departure_datetime"]];
        cell.lblAircraftType.text=arrNotificatinFeeds[indexPath.row][@"aircraft_type"];
        
        NSInteger maxPrice = 0;
        NSInteger minPrice = 0;
        
        maxPrice = [arrNotificatinFeeds[indexPath.row][@"max_price"]integerValue];
        minPrice = [arrNotificatinFeeds[indexPath.row][@"min_price"]integerValue];
        
//        cell.lblPriceunit.text=[NSString stringWithFormat:@"Price Range : $%@ to $%@",arrNotificatinFeeds[indexPath.row][@"min_price"],arrNotificatinFeeds[indexPath.row][@"max_price"]];
        
        if ([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"booked"])
        {
            NSDictionary *dict = (NSDictionary *)[arrNotificatinFeeds objectAtIndex:indexPath.row];
            
            NSMutableString *strPriceLbl = [[NSMutableString alloc]init];
            
            if ([[dict objectForKey:@"charter_type"]isEqualToString:@"Shared Charter"])
            {
                if ([[dict objectForKey:@"passengers"]integerValue] > 0)
                {
                    [strPriceLbl appendString:[NSString stringWithFormat:@"Number of Passenger: %@\n",[dict objectForKey:@"passengers"]]];
                }
            }
            
            if ([dict objectForKey:@"price_per_hour"])
            {
                [strPriceLbl appendString:[NSString stringWithFormat:@"Cost per hour: $%@",[dict objectForKey:@"price_per_hour"]]];
            }
            
            if ([dict objectForKey:@"price"])
            {
                [strPriceLbl appendString:[NSString stringWithFormat:@"\nTotal Cost paid: $%@",[dict objectForKey:@"price"]]];
            }
            
            cell.lblPriceunit.text = strPriceLbl;
//            [NSString stringWithFormat:@"Cost per hour: $%@ \nTotal Cost paid: $%@",[dict objectForKey:@"price_per_hour"],[dict objectForKey:@"price"]];
        
        }
        else
        {
            if ([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"request"])
            {
                NSDictionary *dict = (NSDictionary *)[arrNotificatinFeeds objectAtIndex:indexPath.row];
                
                NSMutableString *strPriceLbl = [[NSMutableString alloc]init];
                
                if ([dict objectForKey:@"price_per_passenger"])
                {
                    [strPriceLbl appendString:[NSString stringWithFormat:@"Cost per customer: $%@\n",[dict objectForKey:@"price_per_passenger"]]];
                }
                
                if ([dict objectForKey:@"price_per_hour"])
                {
                    [strPriceLbl appendString:[NSString stringWithFormat:@"Cost per hour: $%@",[dict objectForKey:@"price_per_hour"]]];
                }
                
                if ([dict objectForKey:@"price"])
                {
                    [strPriceLbl appendString:[NSString stringWithFormat:@"\nTotal Cost for the charter: $%@",[dict objectForKey:@"price"]]];
                }
                cell.lblPriceunit.text = strPriceLbl;
            }
            else
            {
                cell.lblPriceunit.text=[NSString stringWithFormat:@"Price Range : $%ld to $%ld",(long)minPrice,(long)maxPrice];
            }
        }
        
    }
    cell.btnCross.tag=indexPath.row;
    cell.btnCheck.tag=indexPath.row;
    [cell.btnCheck addTarget:self action:@selector(acceptNotificationClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnCross addTarget:self action:@selector(declineNotificationClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.lblAdminMsg.text=arrNotificatinFeeds[indexPath.row][@"message"];
    cell.lblUserCount.text=[NSString stringWithFormat:@"%@ Passenger",arrNotificatinFeeds[indexPath.row][@"passengers"]];
    
    cell.lblAIrcraftName.text=arrNotificatinFeeds[indexPath.row][@"aircraft_name"];
    
    
    cell.lblTime.text= [NSDate prettyTimestampSinceTime:[NSDate dateWithTimeIntervalSince1970:[arrNotificatinFeeds[indexPath.row][@"created"] doubleValue]]];
    cell.lblDate.text= [NSDate prettyTimestampSinceDate:[NSDate dateWithTimeIntervalSince1970:[arrNotificatinFeeds[indexPath.row][@"created"] doubleValue]]];
    
    if ([self isNotNull:arrNotificatinFeeds[indexPath.row][@"aircraft_source"]] && [self isNotNull:arrNotificatinFeeds[indexPath.row][@"aircraft_destination"]])
    {
        NSString *strDestination = arrNotificatinFeeds[indexPath.row][@"aircraft_destination"];
        NSString *strSource = arrNotificatinFeeds[indexPath.row][@"aircraft_source"];
        NSString *strTo=@"to";
        NSString *strFrom=@"from";
        NSString *strComplete=[NSString stringWithFormat:@"%@ %@ %@ %@",strFrom,strSource,strTo,strDestination];
        
        NSMutableAttributedString *attributeStr=[[NSMutableAttributedString alloc]initWithString:strComplete];
        [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:125.0f/255.0f green:125.0f/255.0f blue:125.0f/255.0f alpha:1] range:[strComplete rangeOfString:strFrom]];
        
        [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:[strComplete rangeOfString:strSource]];
        [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:125.0f/255.0f green:125.0f/255.0f blue:125.0f/255.0f alpha:1] range:[strComplete rangeOfString:strTo]];
        
        [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:[strComplete rangeOfString:strDestination]];
        
        cell.lblFrom.attributedText=attributeStr;
    }
    else
    {
        cell.lblSource.text=@"";
    }
    if (indexPath.row == [arrNotificatinFeeds count]-1)
    {
        // [CommonFunction showActivityIndicatorWithText:@""];
        [self getNotificationList];
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //strcharterId
    if([arrNotificatinFeeds[indexPath.row][@"type"]isEqualToString:@"interest"])
    {
//        APPDELEGATE.mainTapBarCntroller.selectedIndex=0;
        
        NotificationAddCharterVC *acvc=[[NotificationAddCharterVC alloc]init];
        acvc.dictData=[arrNotificatinFeeds objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:acvc animated:YES];
        
    }
}

#pragma mark - WebService API
-(void)getNotificationList
{
    
    if (currentPage==1) {
        if (!isNextPage) {
            [CommonFunction removeActivityIndicator];
            
            return;
        }
    }
    else
    {
        if(totalpost==[arrNotificatinFeeds count])
        {
            [CommonFunction removeActivityIndicator];
            return;
        }
    }
    
    NSString *url = [NSString stringWithFormat:@"notifications/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
    //    http://192.168.0.131:7684/notifications/83a595e384926272ad5cf8a48208d486
    
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    NSMutableDictionary *param =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInt:currentPage],@"page", nil];
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
        {
                UITabBar *tabBar = APPDELEGATE.mainTapBarCntroller.tabBar;
                UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
                [tabBarItem2 setImage:[UIImage imageNamed:@"TB_Notification"]];
                [tabBarItem2 setSelectedImage:[UIImage imageNamed:@"TB_NotificationActive"]];
                
                if ([[responseDict objectForKey:@"type"] boolValue]) {
                    
                    NSArray *arrListing = [responseDict objectForKey:@"data"];
                    //                arrNotificatinFeeds=[responseDict objectForKey:@"data"];
                    
                    if (arrListing.count > 0)
                    {
                        lblMsgError.text=@"";
                        if (currentPage == 1)
                        {
                            if (arrNotificatinFeeds.count > 0)
                            {
                                [arrNotificatinFeeds removeAllObjects];
                            }
                            if (arrNotificatinFeeds == nil)
                            {
                                arrNotificatinFeeds = [NSMutableArray array];
                            }
                            arrNotificatinFeeds = [arrListing mutableCopy];
                        }
                        else
                        {
                            [arrNotificatinFeeds addObjectsFromArray:arrListing];
                        }
                    }
                    else
                    {
                        lblMsgError.text=@"Notification not found.";
                    }
                    
                    if (arrNotificatinFeeds.count>0) {
                        lblMsgError.text=@"";
                    }
                    else
                    {
                        lblMsgError.text=@"Notification not found.";
                    }
                    tblVw.estimatedRowHeight=90*SCREEN_YScale;
                    tblVw.rowHeight=UITableViewAutomaticDimension;
                    [tblVw layoutIfNeeded];
                    
                    [tblVw reloadData];
                    
                    isNextPage=[[responseDict objectForKey:@"nextpage"]boolValue];
                    if ([[responseDict objectForKey:@"nextpage"]boolValue]) {
                        currentPage++;
                    }
                    else
                    {
                        totalpost=(int)arrNotificatinFeeds.count;
                        return;
                    }
                }
                else
                {
                    [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
                }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;

                                                  [self getNotificationList];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}
-(void)acceptNotificationRequest:(NSString *)strNotificationID
{
    NSString *url = [NSString stringWithFormat:@"confirm_booking/%@/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN],strNotificationID];
    //    http://192.168.0.131:7684/confirm_booking/SID/notification_id
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];

    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        else if([operation.response statusCode]==200)
        {
            
                [CommonFunction showAlertWithTitle:@"" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:YES dismissBlock:^{
                    currentPage=1;
                    isNextPage=1;

                    [self getNotificationList];
                }];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  
                                                  [self getNotificationList];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

-(void)declineNotificationRequest:(NSString *)strNotificationID
{
    NSString *url = [NSString stringWithFormat:@"reject_booking/%@/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN],strNotificationID];
    //    http://192.168.0.131:7684/reject_booking/SID/notification_id
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
        {
                [CommonFunction showAlertWithTitle:@"" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:YES dismissBlock:^{
                    currentPage=1;
                    isNextPage=1;

                    [self getNotificationList];
                }];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];

                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  
                                                  [self getNotificationList];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                          }
                                      }];
}

#pragma mark - UITableView Delegate For Editing

// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        //add code here for when you hit delete
//        [arrNotificatinFeeds removeObjectAtIndex:indexPath.row];
//        [tblVw deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        if ([CommonFunction reachabiltyCheck])
        {
            [CommonFunction showActivityIndicatorWithText:@""];
            [self requestForDeleteNotification:indexPath];
        }
        else
        {
            [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection. " onViewController:self useAsDelegate:NO dismissBlock:nil];
        }
        
    }
}

-(void)requestForDeleteNotification:(NSIndexPath *)selectedIndexPath
{
//    delete_notification/{SID}
    NSString *url = [NSString stringWithFormat:@"delete_notification/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    NSString *strNotificationId = @"";
    strNotificationId = [NSString stringWithFormat:@"%@",[[arrNotificatinFeeds objectAtIndex:selectedIndexPath.row]objectForKey:@"id"]];
    [param setObject:strNotificationId forKey:@"notification_id"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        
        if(responseDict==Nil)
        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
            [tblVw reloadRowsAtIndexPaths:@[selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else if([operation.response statusCode]==200)
        {
           
                [arrNotificatinFeeds removeObjectAtIndex:selectedIndexPath.row];
                [tblVw deleteRowsAtIndexPaths:@[selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];
                                          
                                          [tblVw reloadRowsAtIndexPaths:@[selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                          
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 )
                                          {
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                          }
                                          else{
                                              
                                               [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              
                                          }
                                          
                                      }];
}

@end
