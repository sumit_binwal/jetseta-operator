//
//  SettingVC.m
//  JetSeta
//
//  Created by Suchita Bohra on 16/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import "SettingVC.h"
#import "SWRevealViewController.h"
#import "SettingsCell.h"
//#import "JetSeta_Constants.h"
#import "ChangePassVC.h"
#import "UtilityClass.h"
#import "UpdateProfileVC.h"
#import "LoginVC.h"

@interface SettingVC ()<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView *tblViewSettings;
    NSArray *arrSettings;
    BOOL switchOn;
}

@end

@implementation SettingVC

#pragma mark - init Methods

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        UIImage *normalImage = nil;
        UIImage *selectedImage = nil;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            normalImage=[CommonFunction imageResize:[UIImage imageNamed:@"senting_active"] andResizeTo:CGSizeMake(25, 25)];
            selectedImage=[CommonFunction imageResize:[UIImage imageNamed:@"senting_deactive"] andResizeTo:CGSizeMake(25, 25)];
        }
        else
        {
            normalImage = [UIImage imageNamed:@"senting_active"];
            selectedImage = [UIImage imageNamed:@"senting_deactive"];
        }
        
        normalImage = [normalImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@""
                                                        image:selectedImage
                                                selectedImage:normalImage];
        
        self.tabBarItem.title = @"";
        self.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        self.navigationItem.title = @"Settings";
    }
    return self;
}

#pragma mark - ViewLifeCycle Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    FIX_IOS_7_LAY_OUT_ISSUE;
    [CommonFunction setNavigationBar:self.navigationController];
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];

    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"AL_Menu"]style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    
    
        arrSettings = [[NSArray alloc]initWithObjects:@"My Profile",@"Change password",@"Notifications", nil];
    
//    NSLog(@"%@", [UtilityClass getUserInfoDictionary]);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([[UtilityClass getUserInfoDictionary]objectForKey:@"notification"])
    {
        switchOn = [[[UtilityClass getUserInfoDictionary]objectForKey:@"notification"]boolValue];
    }
}

#pragma mark - MemoryManagement Method

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDelegate and DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0.0f;
    rowHeight = 45 * SCREEN_YScale;
    return rowHeight;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return arrSettings.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *settingsCell   = @"SettingsCell";
    
    SettingsCell *cell = (SettingsCell *)[tableView dequeueReusableCellWithIdentifier:settingsCell];
    
    if (cell == nil)
    {
        cell = (SettingsCell*) [[[NSBundle mainBundle] loadNibNamed:@"SettingsCell" owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if (indexPath.row == (arrSettings.count-1))
    {
        cell.switchNoti.hidden = NO;
        cell.imgViewArrow.hidden = YES;
        cell.lblTxt.textColor = [UIColor colorWithRed:28/255.0 green:126/255.0 blue:231/255.0 alpha:1.0];
        [cell.switchNoti addTarget:self action:@selector(notificationToggleClicked:) forControlEvents:UIControlEventValueChanged];
        [cell.switchNoti setOn:switchOn animated:YES];
        if (IPAD) {
        [cell.lblTxt setFont:[UIFont fontWithName:cell.lblTxt.font.fontName size:22.0f]];
        }
        else
        {
        [cell.lblTxt setFont:[UIFont fontWithName:cell.lblTxt.font.fontName size:12.0f*SCREEN_XScale]];
        }
    }
    else
    {
        cell.switchNoti.hidden = YES;
        cell.imgViewArrow.hidden = NO;
    }
    cell.lblTxt.text = [arrSettings objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    //    SWRevealViewController *revealController = self.revealViewController;
    
//    arrSettings = [[NSArray alloc]initWithObjects:@"My Profile",@"Change password",@"Payment Details",@"Enter Security Question",@"Notifications", nil];
    
    switch (indexPath.row)
    {
        case 0:
        {
            //My Profile
            UpdateProfileVC *obj = [[UpdateProfileVC alloc]initWithNibName:@"UpdateProfileVC" bundle:[NSBundle mainBundle]];
            [self.navigationController pushViewController:obj animated:YES];
        }
            break;
        case 1:
        {
            if ([[[UtilityClass getUserInfoDictionary]objectForKey:@"registration_type"]isEqualToString:@"facebook"])
            {
                //Payment Details
               
            }
            else
            {
                //Change password
                ChangePassVC *obj = [[ChangePassVC alloc]initWithNibName:@"ChangePassVC" bundle:[NSBundle mainBundle]];
                [self.navigationController pushViewController:obj animated:YES];
            }
           break;
        }
        case 2:
        {
            if ([[[UtilityClass getUserInfoDictionary]objectForKey:@"registration_type"]isEqualToString:@"facebook"])
            {
                //Enter Security Question
            
            }
            else
            {
                //Payment Details
            
            }
            break;
        }
        case 3:
        {
            if (![[[UtilityClass getUserInfoDictionary]objectForKey:@"registration_type"]isEqualToString:@"facebook"])
            {
                //Enter Security Question
               
            }
            break;
        }
        default:
            break;
    }
    
}

-(void)notificationToggleClicked:(id)sender
{
    if (![CommonFunction reachabiltyCheck])
    {
        [tblViewSettings reloadData];
        return;
    }
    
    UISwitch *switchNoti = (UISwitch *) sender;
    if (switchNoti.isOn)
    {
        switchOn = YES;
    }
    else
    {
        switchOn = NO;
    }
    [self sendRequestForNotificationToggleToServer:switchOn];
}

-(void)sendRequestForNotificationToggleToServer:(BOOL)isSwitchOn
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    if (isSwitchOn)
    {
        [param setObject:@"1" forKey:@"notification"];
    }
    else
    {
        [param setObject:@"0" forKey:@"notification"];
    }
    
    
    NSString *url = [NSString stringWithFormat:@"update_profile/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        if(responseDict == Nil)        {

            [CommonFunction showAlertWithTitle:@"Jetseta" message:@"Server error" onViewController:self useAsDelegate:NO dismissBlock:nil];
        }
        
        else if([operation.response statusCode]==200)
        {
             if ([[responseDict valueForKey:@"type"]boolValue])
            {
                /*
                 //successfully registered here... show alert here..
                 if (userDefaults == nil)
                 {
                 userDefaults = [NSUserDefaults standardUserDefaults];
                 }
                 
                 [UtilityClass saveUserInfoWithDictionary:[responseDict objectForKey:@"data"]];
                 [userDefaults synchronize];
                 */
                
                [UtilityClass showAlertWithTitle:@"" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:YES dismissBlock:^
                 {
//                     if (userDefaults == nil)
//                     {
//                         userDefaults = [NSUserDefaults standardUserDefaults];
//                     }
                     [UtilityClass saveUserInfoWithDictionary: [responseDict objectForKey:@"data"]];
                     [NSUSERDEFAULTS synchronize];
                     
                     switchOn = [[[UtilityClass getUserInfoDictionary]objectForKey:@"notification"]boolValue];
                     
                    [tblViewSettings reloadData];
                 }];
            }
            else
            {
             
                [CommonFunction showAlertWithTitle:@"" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:NO dismissBlock:nil];
            }
            
        }
        else
        {
                            [CommonFunction showAlertWithTitle:@"" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:NO dismissBlock:nil];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [CommonFunction removeActivityIndicator];
         if([operation.response statusCode]  == 426)
         {
             NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
             NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
             [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                 
                 NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
             }];
         }

         else if([operation.response statusCode]  == 400)
         {
             NSLog(@"impo response %@",operation.response);
             
           //  [CommonFunctions messageAlert:[operation.responseObject objectForKey:@"response"] title:@"" delegate:nil];
             [CommonFunction showAlertWithTitle:@"" message:[operation.responseObject objectForKey:@"response"] onViewController:self useAsDelegate:NO dismissBlock:nil];
         }
     }];
}

@end
