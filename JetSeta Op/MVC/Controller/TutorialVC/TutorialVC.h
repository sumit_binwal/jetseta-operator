//
//  TutorialVC.h
//  JetSeta
//
//  Created by Suchita Bohra on 11/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialVC : UIViewController<UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (assign, nonatomic) NSInteger index;
@property (nonatomic, strong)UIPageViewController *pageController;

@end
