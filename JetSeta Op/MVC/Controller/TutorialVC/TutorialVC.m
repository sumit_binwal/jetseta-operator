//
//  TutorialVC.m
//  JetSeta
//
//  Created by Suchita Bohra on 11/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import "TutorialVC.h"
#import "TutorialChildVC.h"
#import "LoginVC.h"
//#import "JetSeta_Constants.h"

@interface TutorialVC ()
{
    NSInteger pageCtrlCurrentPage;
    BOOL isViewDidLoaded;
    
}
@end

@implementation TutorialVC
#pragma mark - ViewLifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    FIX_IOS_7_LAY_OUT_ISSUE;
    self.navigationItem.title = @"J e t s e t a";
    isViewDidLoaded = YES;
    [self updateDots];
    [CommonFunction setNavigationBar:self.navigationController];
    [self setNavigationForTutorialScreen];
    
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    if ([NSUSERDEFAULTS valueForKey:kUSERTOKEN])
    {
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"AL_Menu"]style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    }
    
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view]setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 7)];
    
    TutorialChildVC *initialViewController = (TutorialChildVC *)[self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
    
    if (isViewDidLoaded == NO)
    {
        TutorialChildVC *startingViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingViewController];
        [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
    }
    isViewDidLoaded = NO;
    
    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - MemoryManagement Method
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPageViewController Methods

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 5;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(TutorialVC *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
   
    NSUInteger index = [(TutorialVC *)viewController index];
        index++;
    if (index == 5) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (TutorialChildVC *)viewControllerAtIndex:(NSUInteger)index {
    
    TutorialChildVC *childViewController = [[TutorialChildVC alloc] initWithNibName:@"TutorialChildVC" bundle:nil];
    childViewController.index = index;
    
   
    pageCtrlCurrentPage = index;
    [self updateDots];
    
    return childViewController;
    
}

#pragma mark - UserDefined Methods

-(void) updateDots
{
    for (int j = 0; j < [self.pageController.view.subviews count]; j++)
    {
        if ([[self.pageController.view.subviews objectAtIndex:j] isKindOfClass:[UIPageControl class]])
        {
            UIPageControl *pageCtrl = (UIPageControl *) [self.pageController.view.subviews objectAtIndex:j];
            //0000
            NSLog(@"%@",pageCtrl.subviews);
//            pageCtrl.pageIndicatorTintColor = [UIColor colorWithRed:26/255.0 green:120/255.0 blue:194/255.0 alpha:1.0];
//            pageCtrl.currentPageIndicatorTintColor = [UIColor colorWithRed:28/255.0 green:126/255.0 blue:231/255.0 alpha:1.0];
//            pageCtrl.backgroundColor = [UIColor clearColor];
            
            //             dot.frame = CGRectMake(dot.frame.origin.x, dot.frame.origin.y, 10, 10);
            
            //             pageCtrl.currentPageIndicatorTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sliderSolid"]];
            //             pageCtrl.pageIndicatorTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sliderHollow"]];
            
        }
    }
}

-(void)setNavigationForTutorialScreen
{
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"close_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(skipBtnClicked)];

//    
//    [[UIBarButtonItem alloc]initWithTitle:@"Skip" style:UIBarButtonItemStylePlain target:self action:@selector(skipBtnClicked)];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBAction Methods
-(void)skipBtnClicked
{

    if ([[NSUSERDEFAULTS valueForKey:kTUTORIALVISITED]boolValue]) {
        
        
        SWRevealViewController *revealController = [self revealViewController];
        [revealController panGestureRecognizer];
        [revealController tapGestureRecognizer];
        
        [revealController revealToggleAnimated:YES];
        
//        self.navigationItem.leftBarButtonItem = revealButtonItem;

    }
   else
   {
       [NSUSERDEFAULTS setObject:[NSNumber numberWithBool:YES] forKey:kTUTORIALVISITED];
       [NSUSERDEFAULTS synchronize];
       
       [CommonFunction changeRootViewController:NO aircraftPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue] charterPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_CHARTER]boolValue]];
   }
}
- (IBAction)skipBtnClicked:(id)sender {
    [self skipBtnClicked];
}

@end
