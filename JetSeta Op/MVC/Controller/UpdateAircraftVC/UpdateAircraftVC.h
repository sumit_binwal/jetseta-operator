//
//  AddAircraftVC.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 13/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateAircraftVC : UIViewController


@property(nonatomic,strong)NSMutableDictionary *dataDict;
@property(nonnull,strong)NSMutableArray *arrImages;
@property(nonnull,strong)NSMutableArray *arrImagesIndex;
@end
