//
//  AddAircraftVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 13/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "UpdateAircraftVC.h"
#import "AddAircraftCell.h"
#import "KIPLPickerView.h"
#import "AddAircraftImageCell.h"
#import "CropperVC.h"
#import "RSKImageCropViewController.h"
#import <GoogleMaps/GMSServices.h>
#import <GoogleMaps/GMSPlacePicker.h>
#import <Photos/Photos.h>
#import "LoginVC.h"

@interface UpdateAircraftVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,KIPLDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,RSKImageCropViewControllerDelegate,RSKImageCropViewControllerDataSource>
{
    IBOutlet UILabel *lblAddAircraft;
    NSInteger selectedIndexPath;
    IBOutlet UINavigationBar *navBar;
    NSMutableArray *arrTitles;
    NSMutableArray *arrAirCraftType;
    NSMutableArray *arrAircraftCapacity;
    NSMutableDictionary *dictParam;
    IBOutlet UITableView *tblVw;
    NSString *strAirCraftTxt;
    NSString *strAirCraftID;
    NSString *strAirCraftCapacity;
    NSString *strAirCraftCapacityID;
    UITextField *currentActiveTextField;
    UITextView *currentActiveTextView;
    IBOutlet UIView *vwToolbar;
    NSMutableArray *arrDeletedImg;
    NSString *strAircraftLocation;
    IBOutlet UIButton *btnAddAircraft;
    
    IBOutlet UIBarButtonItem *btnToolbarNext;
    IBOutlet UIBarButtonItem *btnToolbarCancle;
    
    IBOutlet UITableView *tbleAirportVw;
    
    NSMutableArray *arrAirportList;
}
@property(strong,nonatomic)GMSPlacePicker *placePicker;
@end

@implementation UpdateAircraftVC
@synthesize dataDict,arrImages,arrImagesIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    NSLog(@"%@",arrImages);
    // Do any additional setup after loading the view from its nib.
    
   NSArray *tempArray= [arrImages sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES]]];
    [arrImages removeAllObjects];
    [arrImages addObjectsFromArray:[tempArray mutableCopy]];
    NSLog(@"%@",arrImages);

}
-(void)setUpView
{
    
    navBar.barTintColor=[UIColor colorWithRed:205.0f/255.0f green:4.0f/255.0f blue:121.0f/255.0f alpha:1];
    navBar.tintColor=[UIColor whiteColor];
    
    UIImage *img=[UIImage imageNamed:@"top_navigation_bg"];
    
    [navBar setBackgroundImage:img forBarMetrics:UIBarMetricsDefault];
    
    [navBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Lato-Regular" size:18],NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil]];

    [navBar setTranslucent:NO];
    
    if (IPAD) {
        [lblAddAircraft setFont:[UIFont fontWithName:lblAddAircraft.font.fontName size:32]];
    }
    
    //Fill Titles Array Value
    arrTitles = [[NSMutableArray alloc]initWithObjects:@"Type of aircraft",@"Aircraft model",@"Aircraft registration (No ID)",@"Manufacturer’s serial number",@"Select number of passengers",@"Enter average speed of aircraft",@"Enter base location of aircraft",@"Enter price per hour",@"Aircraft description",@"Aircraft photos", nil];
    
    dictParam=[[NSMutableDictionary alloc]init];
    

    
    //Register Tap Gesture
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapClicked:)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    
    arrDeletedImg=[[NSMutableArray alloc]init];
    
    [dictParam setObject:dataDict[@"aircraft_speed"] forKey:@"speed"];
    [dictParam setObject:dataDict[@"aircraft_address"] forKey:@"address"];
    [dictParam setObject:dataDict[@"price_per_hour"] forKey:@"price_per_hour"];
    [dictParam setObject:dataDict[@"aircraft_description"] forKey:@"description"];

    
//    arrImages = [[[arrImages reverseObjectEnumerator] allObjects] mutableCopy];
//    aircraft_id
//    NSMutableArray *tempArry=[[NSMutableArray alloc]init];
//    arrImages=[[NSMutableArray alloc]init];
//    arrImagesIndex=[[NSMutableArray alloc]init];
//    tempArry=[dataDict[@"images"] mutableCopy];
//    for (int i=0; i<tempArry.count; i++) {
//        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",tempArry[i][@"image"]]]];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                // Update the UI
//                UIImage *img = [UIImage imageWithData:imageData];
//                [arrImages addObject:img];
//                [arrImagesIndex addObject:tempArry[i][@"id"]];
//                
//                AddAircraftCell *cell = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
//                
//                [cell.collectionImageVw reloadData];
//            });
//        });
//    }

    
}
-(IBAction)singleTapClicked:(id)sender
{
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [btnToolbarNext setTintColor:[UIColor blueColor]];
    [btnToolbarCancle setTintColor:[UIColor blueColor]];
    
    [self registerForKeyboardNotifications];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self unregisterForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UIGesture Delegate Method
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    NSLog(@"%@",NSStringFromClass([touch.view class]));
    return [NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"];
}


#pragma mark - IBAction Button Methods
-(IBAction)otherBtnClicked :(UIButton*)sender
{
    [self getUserLocation];
}
- (IBAction)toolbarCancelBtnClicked:(id)sender {
    [self.view endEditing:YES];
}
- (IBAction)toolbarDoneBtnClicked:(id)sender {
    
    for (NSInteger i=0; i<arrTitles.count; i++)
    {
        AddAircraftCell *cell = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if(cell.txtDiscriptionView)
        {
            if (cell.txtDiscriptionView.tag == currentActiveTextField.tag+1)
            {
                [cell.txtDiscriptionView becomeFirstResponder];
                return;
            }
        }

            if (cell.txtField.tag == currentActiveTextField.tag+1)
            {
                [cell.txtField becomeFirstResponder];
                return;
            }
     }
}

-(IBAction)crossBtnClicked:(UIButton*)sender
{
    
    AddAircraftCell *cell = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];

    if (arrImages.count>0) {
      
         if ([[arrImages objectAtIndex:sender.tag] isKindOfClass:[NSDictionary class]])
         {
             [arrDeletedImg addObject:arrImages[sender.tag][@"id"]];
             [arrImages removeObjectAtIndex:sender.tag];
         }
        else
        {
             [arrImages removeObjectAtIndex:sender.tag];
        }
    }
    [cell.collectionImageVw reloadData];
    
    
    
}
-(IBAction)btnImagePickerClicked:(UIButton*)sender
{

    selectedIndexPath=sender.tag;
    
    [self.view endEditing:YES];
    UIAlertController *actionSheet=[UIAlertController alertControllerWithTitle:@"Add aircraft images" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:    [UIAlertAction actionWithTitle:@"Take photos with camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Jetseta"
                                                                  message:@"Device has no camera"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
            
            [myAlertView show];
            
        }else
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = NO;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
            
        }

        
    }]];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose from gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = NO;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    


    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        UIPopoverPresentationController *alertPopoverPresentationController = actionSheet.popoverPresentationController;
        CGPoint p = [sender.superview convertPoint:sender.center toView:self.view];
        AddAircraftCell *cell = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
        
        CGRect frame = CGRectMake(p.x-cell.frame.size.width/2,
                                  p.y,
                                  cell.frame.size.width,
                                  cell.frame.size.height);
        alertPopoverPresentationController.sourceRect = frame;
        
        
        alertPopoverPresentationController.sourceView = self.view;
        
        [self showDetailViewController:actionSheet sender:sender];
    }
    else
    {
            [self presentViewController:actionSheet animated:YES completion:nil];
    }

}
- (IBAction)cancelBtnClicked:(id)sender {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)nextBtnClicked:(id)sender {
    [self.view endEditing:YES ];
    if ([self validateTextField]) {
    
    [dictParam setObject:[NSString stringWithFormat:@"%f",CurrentLatitude] forKey:@"latitude"];
    [dictParam setObject:[NSString stringWithFormat:@"%f",CurrentLongitude] forKey:@"longitude"];
        
        NSString *joinedComponents = [arrDeletedImg componentsJoinedByString:@","];
    [dictParam setObject:joinedComponents forKey:@"delete_images"];
        NSLog(@"%@",dictParam);
        
        if ([CommonFunction reachabiltyCheck]) {
            [CommonFunction showActivityIndicatorWithText:@""];
            btnAddAircraft.userInteractionEnabled=NO;
            [self uploadImageBySessionUserMethod:@"" withParameters:dictParam withCompletion:^(NSArray *data) {
                NSLog(@"%@",dictParam);
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"%@",data);
                });
                
            } WithFailure:^(NSString *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"%@",error);
                });
            }];
        }
        else
        {
        [CommonFunction showAlertWithTitle:@"" message:@"Please check your network connection. " onViewController:self useAsDelegate:NO dismissBlock:nil];
        }

   }
}

#pragma mark -
#pragma mark - UIImage Controller Delegate Methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{

       [[UIApplication sharedApplication] setStatusBarHidden:NO];
    RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:[info objectForKey:UIImagePickerControllerOriginalImage]cropMode:RSKImageCropModeCustom];
    
    imageCropVC.delegate = self;
    imageCropVC.dataSource=self;
    [picker pushViewController:imageCropVC animated:YES];

}

- (CGRect)imageCropViewControllerCustomMaskRect:(RSKImageCropViewController *)controller
{
    CGSize maskSize;
    if ([controller isPortraitInterfaceOrientation]) {
        maskSize = CGSizeMake(APPDELEGATE.window.bounds.size.width,APPDELEGATE.window.bounds.size.width/2);
    } else {
        maskSize = CGSizeMake(220, 220);
    }
    
    CGFloat viewWidth = CGRectGetWidth(controller.view.frame);
    CGFloat viewHeight = CGRectGetHeight(controller.view.frame);
    
    CGRect maskRect = CGRectMake((viewWidth - maskSize.width) * 0.5f,
                                 (viewHeight - maskSize.height) * 0.5f,
                                 maskSize.width,
                                 maskSize.height);
    
    return maskRect;
}


// Returns a custom path for the mask.
- (UIBezierPath *)imageCropViewControllerCustomMaskPath:(RSKImageCropViewController *)controller
{
    CGRect rect = controller.maskRect;
    CGPoint point1 = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));
    CGPoint point2 = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    //CGPoint point3 = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint point3 = CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect));
    CGPoint point4 = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));
    
    UIBezierPath *triangle = [UIBezierPath bezierPath];
    [triangle moveToPoint:point1];
    [triangle addLineToPoint:point2];
    [triangle addLineToPoint:point3];
     [triangle addLineToPoint:point4];
    [triangle closePath];
    
    return triangle;
}

- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller
{
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}




- (void)imageCropViewController:(RSKImageCropViewController *)controller
                   didCropImage:(UIImage *)croppedImage
                  usingCropRect:(CGRect)cropRect
{
    AddAircraftCell *cell = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:9 inSection:0]];
    
    AddAircraftImageCell *cellCollection = [cell.collectionImageVw cellForItemAtIndexPath:[NSIndexPath indexPathForRow:selectedIndexPath inSection:0]];
    
    if (cellCollection.btnAircraftImg.imageView.image&&selectedIndexPath<arrImages.count) {
        
        if ([[arrImages objectAtIndex:selectedIndexPath] isKindOfClass:[NSDictionary class]])
        {
            [arrDeletedImg addObject:arrImages[selectedIndexPath][@"id"]];
        }
        [arrImages replaceObjectAtIndex:selectedIndexPath withObject:croppedImage];
    }
    else
    {
        [arrImages addObject:croppedImage];
    }
    NSLog(@"Array Deleted Image : %@",arrDeletedImg);
        [cell.collectionImageVw reloadData];
        [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark UIPickerView Delegate & Datasource
-(void)kiplPickerViewDidDismissWithSelectedRow:(NSInteger)selectedRow PickerVwTag:(NSInteger)tag
{
    if (tag==4) {
        NSString *storeName=[NSString stringWithFormat:@"%@",arrAircraftCapacity[selectedRow]];
        strAirCraftCapacity = storeName;
        [dictParam setObject:strAirCraftCapacity forKey:@"capacity"];
        [tblVw reloadData];
        for (NSInteger i=1; i<arrTitles.count; i++)
        {
            AddAircraftCell *cell = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            
            if (cell.txtField.tag == currentActiveTextField.tag+1)
            {
                [cell.txtField becomeFirstResponder];
                return;
            }
        }
    }
    else
    {
        NSString *storeName=[NSString stringWithFormat:@"%@ (%@-%@ Passenger)",arrAirCraftType[selectedRow][@"name"],arrAirCraftType[selectedRow][@"min_capacity"],arrAirCraftType[selectedRow][@"max_capacity"]];
        strAirCraftTxt = storeName;
        strAirCraftID = [NSString stringWithFormat:@"%@",arrAirCraftType[selectedRow][@"id"]];
           [dictParam setObject:strAirCraftID forKey:@"aircraft_type_id"];
        arrAircraftCapacity=[[NSMutableArray alloc]init];
        NSInteger minValue=[arrAirCraftType[selectedRow][@"min_capacity"]integerValue];
        NSInteger MaxValue=[arrAirCraftType[selectedRow][@"max_capacity"]integerValue];
        
        for (minValue=[arrAirCraftType[selectedRow][@"min_capacity"]integerValue]; minValue<=MaxValue; minValue++) {
            [arrAircraftCapacity addObject:[NSNumber numberWithInteger:minValue]];
        }
        strAirCraftCapacity=nil;

        [dictParam removeObjectForKey:@"capacity"];
        
        [tblVw reloadData];
        for (NSInteger i=0; i<arrTitles.count; i++)
        {
            AddAircraftCell *cell = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            
            if (cell.txtField.tag == currentActiveTextField.tag+1)
            {
                [cell.txtField becomeFirstResponder];
                return;
            }
        }
    }
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag==0) {
        return arrAirCraftType.count;
    }
    else
    {
        return arrAircraftCapacity.count;
    }

}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{

    if (pickerView.tag==0) {
        NSString *storeName=[NSString stringWithFormat:@"%@ (%@-%@ Passenger)",arrAirCraftType[row][@"name"],arrAirCraftType[row][@"min_capacity"],arrAirCraftType[row][@"max_capacity"]];
        NSLog(@"store name: %@",storeName);
        return storeName;
    }
    else
    {
        NSString *storeName=[NSString stringWithFormat:@"%@",[arrAircraftCapacity objectAtIndex:row]];
        return storeName;

    }

}

#pragma mark - TextFieldValidate
-(BOOL)validateTextField
{
    NSString *strAddress=[NSString stringWithFormat:@"%@",dictParam[@"address"]];
    NSString *strSpeed=[NSString stringWithFormat:@"%@",dictParam[@"speed"]];
    NSString *strDescription=[NSString stringWithFormat:@"%@",dictParam[@"description"]];
    NSString *strPrice=[NSString stringWithFormat:@"%@",dictParam[@"price_per_hour"]];

     if (!dictParam[@"speed"]||!(strSpeed.length>0))
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please enter average speed of aircraft." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    else if (!dictParam[@"address"]||!(strAddress.length>0))
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please enter base location of aircraft." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    else if (!dictParam[@"price_per_hour"]||!(strPrice.length>0))
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please enter price per hour." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    else if (!dictParam[@"description"]||!(strDescription.length>0))
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please enter price per hour." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    else if (arrImages.count<2)
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please select at least 2 photos." onViewController:self useAsDelegate:NO dismissBlock:nil];
        return  NO;
    }
    
    return YES;
}



#pragma mark - UICollection View Delegate Method
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return 6;

    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectioView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AddAircraftImageCell   *cell = [collectioView dequeueReusableCellWithReuseIdentifier:@"AddAircraftImageCell" forIndexPath:indexPath];
    
    
    return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(AddAircraftImageCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{

    if (arrImages.count>indexPath.row) {
        if ([[arrImages objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
            [cell.btnCross setHidden:NO];
//            [cell.btnAircraftImg setImage:arrImages[indexPath.row][@"image"] forState:UIControlStateNormal];
            
            NSString *strImgUrl=[NSString stringWithFormat:@"%@",arrImages[indexPath.row][@"image"]];
//            [cell.btnAircraftImg.imageView setImageWithURL:[NSURL URLWithString:strImgUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//            [cell.btnAircraftImg setImage:arrImages[indexPath.row]forState:UIControlStateNormal];
            UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyleWhiteLarge)];
            [activity startAnimating];
            [activity setCenter:cell.btnAircraftImg.center];
            [cell.btnAircraftImg addSubview:activity];
            [cell.btnAircraftImg sd_setImageWithURL:[NSURL URLWithString:strImgUrl] forState:UIControlStateNormal completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [activity removeFromSuperview];
            }];
        }
        else
        {
            [cell.btnCross setHidden:NO];
            [cell.btnAircraftImg setImage:arrImages[indexPath.row]forState:UIControlStateNormal];
        }
        

    }
    else
    {
    [cell.btnCross setHidden:YES];
    [cell.btnAircraftImg setImage:nil forState:UIControlStateNormal];
    }
    cell.btnAircraftImg.tag=indexPath.row;
    cell.btnCross.tag=indexPath.row;
    [cell.btnAircraftImg addTarget:self action:@selector(btnImagePickerClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnCross addTarget:self action:@selector(crossBtnClicked:) forControlEvents:UIControlEventTouchUpInside];

}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

}

#pragma mark - UICollectionViewDelegateLeftAlignedLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        return CGSizeMake(145*SCREEN_XScale,72*SCREEN_YScale);
    
}


#pragma mark - TableView Delegate Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==tbleAirportVw) {
        return arrAirportList.count;
    }
    else
    {
    return   arrTitles.count;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tbleAirportVw) {
        return 40.0f;
    }
    else
    {
    if (indexPath.row==9) {
            return 286*SCREEN_YScale;
    }
    else if (indexPath.row==8)
    {
    return 118*SCREEN_YScale;
    }
    else
    {
    return 72*SCREEN_YScale;
    }
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==tbleAirportVw) {
        
        NSInteger cellValue = 4;
        NSString *cellIdentifier=@"AirportCell";
        
        AddAircraftCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AddAircraftCell class]) owner:self options:nil][cellValue];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    else
    {
        NSInteger cellValue = 0;
        NSString *cellIdentifier=@"AddAircraftCell";
        if (indexPath.row==8)
        {
            cellIdentifier=@"AddAircraftTxtVwCell";
            cellValue=2;
        }
        else if (indexPath.row==9) {
            cellIdentifier=@"AddAircraftImageCell";
            cellValue = 1;
        }
        
        AddAircraftCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AddAircraftCell class]) owner:self options:nil][cellValue];
            if (cellValue == 1)
            {
                [cell adjustCellWithType:CELL_TYPE_COLLECTION];
                if (!cell.collectionImageVw.delegate)
                {
                    cell.collectionImageVw.delegate=self;
                    cell.collectionImageVw.dataSource = self;
                    [cell.collectionImageVw reloadData];
                }
            }
        }
        
        
        
        return cell;

    }
    }
-(void)tableView:(UITableView *)tableView willDisplayCell:(AddAircraftCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tbleAirportVw)
    {
        cell.lblTextFieldName.text=arrAirportList[indexPath.row][@"name"];
        
    }
    else
    {
        cell.lblTextFieldName.text=[arrTitles objectAtIndex:indexPath.row];
        [cell.imgBottomArrow setImage:nil];
        [cell.btnOther setHidden:YES];
        [cell.lblKm setText:@""];
        switch (indexPath.row) {
            case 0:
            {
                [cell.txtField setText:dataDict[@"aircraft_type"]];
                [cell.imgBottomArrow setImage:[UIImage imageNamed:@"AAC_bottomArrow"]];
                cell.txtField.placeholder=@"Select";
                
                
                
                cell.userInteractionEnabled=NO;
                
                break;
            }
            case 1:
            {
                [cell.txtField setText:dataDict[@"aircraft_name"]];
                cell.userInteractionEnabled=NO;
                
                break;
            }
            case 2:
            {
                [cell.txtField setText:dataDict[@"registration_id"]];
                cell.userInteractionEnabled=NO;
                
                break;
            }
            case 3:
            {
                [cell.txtField setText:dataDict[@"manufacturer"]];
                cell.userInteractionEnabled=NO;
                
                break;
            }
            case 4:
            {
                cell.txtField.placeholder=@"Select";
                [cell.txtField setText:[NSString stringWithFormat:@"%@",dataDict[@"capacity"]]];
                [cell.imgBottomArrow setImage:[UIImage imageNamed:@"AAC_bottomArrow"]];
                cell.userInteractionEnabled=NO;
                
                break;
            }
                
            case 5:
            {
                [cell.txtField setText:[NSString stringWithFormat:@"%@",dictParam[@"speed"]]];
                break;
            }
            case 6:
            {
                [cell.txtField setText:[NSString stringWithFormat:@"%@",dictParam[@"address"]]];
                [cell.btnOther setHidden:NO];
                cell.btnOther.tag=indexPath.row;
                [cell.btnOther addTarget:self action:@selector(otherBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                break;
            }
            case 7:
            {
                [cell.txtField setText:[NSString stringWithFormat:@"%@",dictParam[@"price_per_hour"]]];
                break;
            }
            case 8:
            {
                [cell.txtDiscriptionView setText:[NSString stringWithFormat:@"%@",dictParam[@"description"]]];
                break;
            }
                
                
            default:
                break;
        }
        
        
        if (indexPath.row==5||indexPath.row==7)
        {
            cell.txtField.keyboardType=UIKeyboardTypeDecimalPad;
            cell.txtField.inputAccessoryView=vwToolbar;
            cell.txtField.placeholder=@"0.00";
            if (indexPath.row==5) {
                cell.lblKm.text=@"Km/hr";
            }
            else
            {
                cell.lblKm.text=@"USD";
            }
            
        }
        else
        {
            cell.txtField.placeholder=@"Enter";
        }
        
        cell.txtField.delegate=self;
        cell.txtDiscriptionView.delegate=self;
        cell.txtDiscriptionView.tag=indexPath.row;
        cell.txtField.tag=indexPath.row;
        
        
        if (indexPath.row==9) {
            
            
            cell.collectionImageVw.delegate=self;
            cell.collectionImageVw.dataSource=self;
            
        }
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tbleAirportVw) {
        [self.view endEditing:YES];
        strAircraftLocation=arrAirportList[indexPath.row][@"name"];
        
        [dictParam setObject:strAircraftLocation forKey:@"address"];
        
        [tbleAirportVw setHidden:YES];
        [tblVw setScrollEnabled:YES];
        [tblVw reloadData];
    }
}




#pragma mark - TextView Delegate Metods
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    if (textView) {
            return !([textView.text length]>1000-1 && [text length] > range.length);
    }
    return YES;

}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    NSIndexPath* path = [NSIndexPath indexPathForRow:textView.tag inSection:0];
    [self performSelector:@selector(scrollToCell:) withObject:path afterDelay:0.5f];
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    currentActiveTextView=textView;
    return YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    [currentActiveTextField resignFirstResponder];
    if ( textView.tag==8) {
        [dictParam setObject:textView.text forKey:@"description"];
    }
}

#pragma mark - TextField Delegate Metods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag==0||textField.tag==4) {
        if (textField.tag==0)
        {
            currentActiveTextField=textField;
            [self openPickerView:0];
            return NO;
        }
        if (textField.tag==4) {
            if (arrAircraftCapacity.count>0) {
                currentActiveTextField=textField;
                [self openPickerView:4];
                return NO;
            }
            else
            {
                [CommonFunction alertTitle:@"" withMessage:@"Please select aircraft type first."];
                return NO;
            }
        }
    }
    currentActiveTextField=textField;
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];

    switch (textField.tag) {
        case 1:
        {
            [dictParam setObject:textField.text forKey:@"name"];
            break;
        }
        case 2:
        {
            [dictParam setObject:textField.text forKey:@"registration_id"];
            break;
        }
        case 3:
        {
            [dictParam setObject:textField.text forKey:@"manufacturer"];
            break;
        }
        case 5:
        {
            if ([textField.text integerValue]>0) {
            [dictParam setObject:textField.text forKey:@"speed"];
                
            }
            else
            {
                textField.text=@"";
            }
            
            break;
        }
        case 6:
        {
            [dictParam setObject:textField.text forKey:@"address"];
            break;
        }
        case 7:
        {
            if ([textField.text integerValue]>0) {
            [dictParam setObject:textField.text forKey:@"price_per_hour"];
                
            }
            else
            {
                textField.text=@"";
            }
            
            break;
        }
    }
    NSLog(@"%@",dictParam);
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    for (NSInteger i=0; i<arrTitles.count; i++)
    {
        AddAircraftCell *cell = [tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (cell.txtField.tag == textField.tag+1)
        {
            [cell.txtField becomeFirstResponder];
            return YES;
        }
    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    if (textField.tag==6) {
        [tbleAirportVw setHidden:NO];
        CGRect cellRect = [tblVw rectForRowAtIndexPath:[NSIndexPath indexPathForRow:textField.tag inSection:0]];
        
        
        cellRect.size.height=90;
        [tblVw setScrollEnabled:NO];
        textField.alpha=1;
        cellRect = CGRectOffset(cellRect, -tblVw.contentOffset.x, -tblVw.contentOffset.y+140);
        if (IPAD) {
            cellRect.origin.y=cellRect.origin.y+50;
        }
        [tbleAirportVw setFrame:cellRect];
        
        NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        [self getAirportName:searchStr];

    }
    if(textField.tag==5||textField.tag==7)
    {
        return !([textField.text length]>20-1 && [string length] > range.length);
        
    }
    
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage])
        {
            return NO;
        }
    return YES;
}

#pragma mark UIKeyBoard Activity Handling
// Call this method somewhere in your view controller setup code.
-(void) scrollToCell:(NSIndexPath*) path {
    [tblVw scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionNone animated:YES];
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    
}


// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
       NSDictionary* info = [aNotification userInfo];
    
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets;
    if (currentActiveTextField.tag==6) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+100, 0.0);
    }
    else
    {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    }
        tblVw.contentInset = contentInsets;
        tblVw.scrollIndicatorInsets = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        
        if (!CGRectContainsPoint(aRect, currentActiveTextField.frame.origin) )
        {
            [tblVw scrollRectToVisible:currentActiveTextField.frame animated:YES];
        }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    tblVw.contentInset = contentInsets;
   tblVw.scrollIndicatorInsets = contentInsets;
    [tblVw scrollRectToVisible:CGRectZero animated:YES];
    
}

- (void)openPickerView:(int)tag
{
    [self.view endEditing:YES];
    KIPLPickerView *pickerview;
    if (tag==0) {
        if (strAirCraftID.length>0) {
            
            
            NSUInteger selectedIndex = -1;
            
            for (NSDictionary *dict in arrAirCraftType) {
                
                if ([[dict valueForKey:@"id"] intValue]==[strAirCraftID intValue]) {
                    
                    selectedIndex = [arrAirCraftType indexOfObject:dict];
                    break;
                }
                
              
                
            }
            
            pickerview= [[KIPLPickerView alloc]initWithFrame:self.view.frame delegate:self datasource:self pickrVwTag:tag selectedInde:(int)selectedIndex];
            
        }
        else
        {
            pickerview= [[KIPLPickerView alloc]initWithFrame:self.view.frame delegate:self datasource:self pickrVwTag:tag selectedInde:0];
        }
    }
    else
    {
        if (strAirCraftCapacity.length>0) {
            NSUInteger selectedIndex = -1;
            for (int i=0; i<arrAircraftCapacity.count; i++) {
                if ([strAirCraftCapacity integerValue] ==[[arrAircraftCapacity objectAtIndex:i] integerValue]) {
                    selectedIndex=i;                }
            }
            
            pickerview= [[KIPLPickerView alloc]initWithFrame:self.view.frame delegate:self datasource:self pickrVwTag:tag selectedInde:(int)selectedIndex];
        }
        else
        {
            pickerview= [[KIPLPickerView alloc]initWithFrame:self.view.frame delegate:self datasource:self pickrVwTag:tag selectedInde:0];
        }
    }
  
    [pickerview updatePickerView];
    [self.view addSubview:pickerview];
    
}



#pragma mark - WebService API


-(void)uploadImageBySessionUserMethod:(NSString *)strMethodName withParameters:(NSDictionary*)dictParameter withCompletion:(void (^)(NSArray *data))completion WithFailure:(void (^)(NSString *error))failure
{
    NSString *strUrl = [NSString stringWithFormat:@"%@update_aircraft/%@/%@",serverURL,[NSUSERDEFAULTS objectForKey:kUSERTOKEN],dataDict[@"aircraft_id"]];
    NSLog(@"%@",strUrl);
    NSURL *url = [NSURL URLWithString:strUrl];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
//    [headers setObject:version forKey:@"version"];
    [manager.requestSerializer setValue:version forHTTPHeaderField:@"Accept"];
    
    NSLog(@"%@",dictParam);
    AFHTTPRequestOperation *op = [manager POST:@"" parameters:dictParam constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        

        
        for (NSInteger i=0; i<arrImages.count; i++) {
            
            if ([[arrImages objectAtIndex:i] isKindOfClass:[NSDictionary class]]) {
//                NSData *imageData1 = UIImageJPEGRepresentation([[arrImages objectAtIndex:i] objectForKey:@"image"],0.5f);
//                NSLog(@"0.8 %lu",(unsigned long)imageData1.length);
//                
//                NSString *fileImg1=[NSString stringWithFormat:@"file%ld",(long)i];
//                NSString *parameterNm1=[NSString stringWithFormat:@"file%ld",(long)i];
//                
//                [formData appendPartWithFileData:imageData1 name:parameterNm1 fileName:fileImg1 mimeType:@"image/jpeg"];
            }
            else
            {
                NSData *imageData1 = UIImageJPEGRepresentation([arrImages objectAtIndex:i],0.5f);
                NSLog(@"0.8 %lu",(unsigned long)imageData1.length);
                
                NSString *fileImg1=[NSString stringWithFormat:@"file%ld",(long)i];
                NSString *parameterNm1=[NSString stringWithFormat:@"file%ld",(long)i];
                
                [formData appendPartWithFileData:imageData1 name:parameterNm1 fileName:fileImg1 mimeType:@"image/jpeg"];

            }
            

        }
            

        
    } success:^(AFHTTPRequestOperation *operation, id responseObject)
                                  {
                                      [CommonFunction removeActivityIndicator];
                                      //                                      NSError* error;
                                      
                                      if (responseObject != nil)
                                      {
                                          
                                          NSDictionary *jsonDic = (NSDictionary *)responseObject;
                                          
                                          if ([[[jsonDic objectForKey:@"meta"]objectForKey:@"hasUpdate"]boolValue])
                                          {
                                              [CommonFunction showAlertWithTitle:@"" message:NSLocalizedString(@"New version is available on Appstore.\n you have to update Jetseta OP.", nil) onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  LoginVC *fufs = [[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil];
                                                  APPDELEGATE.navigationController=[[UINavigationController alloc]initWithRootViewController:fufs];
                                                  self.navigationController.navigationBarHidden=YES;
                                                  [APPDELEGATE.window.window setRootViewController:self.navigationController];
                                                  [NSUSERDEFAULTS removeObjectForKey:@"previousSelectedVC"];
                                                  [NSUSERDEFAULTS removeObjectForKey:kUSERNAME];
                                                  [NSUSERDEFAULTS removeObjectForKey:kOperatorName];
                                                  [NSUSERDEFAULTS removeObjectForKey:kUSERTOKEN];
                                                  [NSUSERDEFAULTS removeObjectForKey:kPUBLISH_AIRCRAFT];
                                                  [NSUSERDEFAULTS removeObjectForKey:kPUBLISH_CHARTER];
                                                  [NSUSERDEFAULTS synchronize];
                                                  [CommonFunction changeRootViewController:NO aircraftPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue] charterPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_CHARTER]boolValue]];
                                                  
                                              }];
                                              /*
                                               [UtilityClass showAlertWithTitle:NSLocalizedString(@"Generics", nil) message:NSLocalizedString(@"New version is available on Appstore.\n you have to update Generics.", nil) onViewController:self useAsDelegate:YES dismissBlock:^{
                                               //remove all userdefaults data...
                                               [UtilityClass moveToStartingDueToForceUpdate];
                                               }];
                                               */
                                          }
                                          
                                          else if ([jsonDic[@"type"]boolValue]) {
                                              [CommonFunction showAlertWithTitle:@"" message:@"Aircraft Updated Successfully." onViewController:self useAsDelegate:YES dismissBlock:^{
                                               [self dismissViewControllerAnimated:YES completion:nil];
                                                  }];
                                              btnAddAircraft.userInteractionEnabled=YES;
                                          }
                                          else
                                          {
                                              [CommonFunction showAlertWithTitle:@"" message:jsonDic[@"message"] onViewController:self useAsDelegate:NO dismissBlock:nil];
                                          }
                                              
                                          
                                          
                                          
                                          NSLog(@"Profile  JSON: %@",jsonDic);
                                          
                                      }
                                  } failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  {
                                      [CommonFunction removeActivityIndicator];
                                      NSLog(@"Error: %@ ***** %@", operation.responseString, error);
                                       btnAddAircraft.userInteractionEnabled=YES;
                                       [CommonFunction alertTitle:@"" withMessage:operation.responseString];
                                      
                                  }];
//    [op setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//        [vwHud setFrame:self.view.frame];
//        [self.view addSubview:vwHud];
//        prgresVw.hidden=NO;
//        prgresVw.progress=(float)totalBytesWritten/(float)totalBytesExpectedToWrite;
//        isAvtarImageSet=NO;
//        isCroppedImageSet=NO;
//        
//    }];
    
    
    [op start];
    
    
}
-(void)getUserLocation
{
    [self.view endEditing:YES];
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(CurrentLatitude, CurrentLongitude);
    
    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001);
    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001);
    GMSCoordinateBounds *viewport = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
                                                                         coordinate:southWest];
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:viewport];
    _placePicker = [[GMSPlacePicker alloc] initWithConfig:config];
    [CommonFunction setNavigationBar:self.navigationController];
    
    [_placePicker pickPlaceWithCallback:^(GMSPlace *place, NSError *error) {
        if (error != nil) {
            NSLog(@"Pick Place error %@", [error localizedDescription]);
            return;
        }
        
        if (place != nil)
        {
            
            //strAddress = place.formattedAddress;
            if (strAircraftLocation) {
                [dictParam setObject:strAircraftLocation forKey:@"address"];
            }
            strAircraftLocation = place.name;
            if (strAircraftLocation) {
                [dictParam setObject:strAircraftLocation forKey:@"address"];
            }
            
            [tblVw reloadData];
            NSLog(@"Place name %@", place.name);
            NSLog(@"Place address %@", place.formattedAddress);
            NSLog(@"Place attributions %@", place.attributions.string);
        } else {
            NSLog(@"No place selected");
        }
    }];
}
- (BOOL)hasGalleryPermission
{
    BOOL hasGalleryPermission = NO;
    PHAuthorizationStatus authorizationStatus = [PHPhotoLibrary authorizationStatus];
    
    if (authorizationStatus == PHAuthorizationStatusAuthorized) {
        hasGalleryPermission = YES;
    }
    return hasGalleryPermission;
}
-(void)getAirportName:(NSString *)strName;
{
    
    NSString *url = [NSString stringWithFormat:@"airports_list"];
    //http://192.168.0.131:7682/aircraft_list
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    NSMutableDictionary *param =[[NSMutableDictionary alloc]initWithObjectsAndKeys:strName,@"keyword", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server error"];
        }
        
        else if([operation.response statusCode]==200)
        {
            
                arrAirportList=[[NSMutableArray alloc]init];
                arrAirportList=[responseDict objectForKey:@"data"];
                [tblVw scrollsToTop];
                [tbleAirportVw reloadData];
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          
                                          if([operation.response statusCode]  == 426)
                                          {
                                              NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                                              NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                                              [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                                                  
                                                  NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                              }];
                                          }

                                          else if([operation.response statusCode]  == 400 )
                                          {
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  //[self getAircraftAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                                  
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network error..."];
                                              }
                                          }
                                      }];
}
@end
