//
//  AddCharterVC.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 14/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateCharterVC : UIViewController


@property(nonatomic,strong)NSMutableDictionary *dictData;
@property(nonatomic,strong)NSString *strCharterLegID;
@property(nonatomic,strong)NSString *strAircraftName;
@property(nonatomic,strong)NSString *strLegID;

-(void)removeKeyboard;
@end
