//
//  UpdateProfileVC.h
//  JetSeta
//
//  Created by Suchita Bohra on 22/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateProfileVC : UIViewController

@property (nonatomic, strong) NSMutableDictionary *dictFbProfileInfo;
@property (nonatomic, strong) NSString *strCountryCode;

-(void)removeKeyboard;

@end
