//
//  UpdateProfileVC.m
//  JetSeta
//
//  Created by Suchita Bohra on 22/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import "UpdateProfileVC.h"
//#import "JetSeta_Constants.h"
#import "SignupCellName.h"
#import <GoogleMaps/GMSServices.h>
#import <GoogleMaps/GMSPlacePicker.h>
#import "SignupCellPhone.h"
//#import "AppLocationManager.h"
#import "SignupCellText.h"
#import "CountryCodeVC.h"
#import "SignupCellPic.h"
#import "UtilityClass.h"
#import "ImageCapture.h"
#import "Config.h"
#import "LoginVC.h"

NSUserDefaults *userDefaultss;
@interface UpdateProfileVC ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    IBOutlet UITableView *tblViewUpdateProfile;
    UITextField *currentActiveTextField;
    
    NSString *strAddress;
    BOOL isImageCaptured;
    ImageCapture *imagePickerObject;
    
}
@property(strong,nonatomic)GMSPlacePicker *placePicker;

@end

@implementation UpdateProfileVC

#pragma mark - ViewLifeCycle Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //    FIX_IOS_7_LAY_OUT_ISSUE;
    _strCountryCode = @"";
    self.navigationItem.title = @"Update Profile";
    isImageCaptured = NO;
    
    NSLog(@"%@",_dictFbProfileInfo);
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftButtonClicked:)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"AC_imgCheck"] style:UIBarButtonItemStylePlain target:self action:@selector(createAccountBtnClicked)];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    _dictFbProfileInfo = [NSMutableDictionary dictionary];
    [self setUserProfileInfoData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    [self registerForKeyboardNotifications];
    
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(locationAllowed) name:@"locationAllowed" object:nil];
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(locationDenied) name:@"locationDenied" object:nil];
    
    [_dictFbProfileInfo setObject:_strCountryCode forKey:@"countrycode"];
    [tblViewUpdateProfile reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unRegisterForKeyboardNotifications];
    
    //    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"locationAllowed" object:nil];
    //    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"locationDenied" object:nil];
}

#pragma mark - Memory Menagement Method

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UITableView DataSource and Delegate.

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0.0f;
    //    rowHeight = (75 * [self appDelegate].window.bounds.size.width)/320;
    //    return rowHeight;
    if (indexPath.row == 7)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            rowHeight = 230.0f;
        }
        else
        {
            rowHeight = 141.0f;
        }
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            if (indexPath.row == 0)
            {
                rowHeight = 320.0f;
            }
            else
            {
                rowHeight = 136.0f;
            }
        }
        else
        {
            if (indexPath.row == 0)
            {
                rowHeight = 190.0f;
            }
            else
            {
                rowHeight = 74.0f;
            }
        }
    }
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 5;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        //for Pic..
        static NSString *signupCellPic   = @"SignupCellPic";
        SignupCellPic *cell = (SignupCellPic *)[tableView dequeueReusableCellWithIdentifier:signupCellPic];
        
        if (cell == nil)
        {
            cell = (SignupCellPic*)[[[NSBundle mainBundle] loadNibNamed:@"SignupCellPic" owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
        }
        cell.imgViewPic.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesturePic = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(picBtnClicked)];
        tapGesturePic.numberOfTapsRequired = 1;
        [cell.imgViewPic addGestureRecognizer:tapGesturePic];
        
        if ([[_dictFbProfileInfo objectForKey:@"picture"]isKindOfClass:[UIImage class]])
        {
            cell.imgViewPic.image = (UIImage *) [_dictFbProfileInfo objectForKey:@"picture"];
            
        }
        else if ([[_dictFbProfileInfo objectForKey:@"picture"]isKindOfClass:[NSString class]])
        {
            //need to download image from url.
            //****
            //set image of aircraft.
            __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            activityIndicator.center = CGPointMake(cell.imgViewPic.frame.size.width/2, cell.imgViewPic.frame.size.height/2);
            activityIndicator.hidesWhenStopped = YES;
            
            //            signup_pic
            [cell.imgViewPic sd_setImageWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@",[_dictFbProfileInfo objectForKey:@"picture"]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                if (error)
                {
                    //downloading failed.
                }
                [activityIndicator removeFromSuperview];
                activityIndicator = nil;
                if (cell.imgViewPic.image==nil) {
                    cell.imgViewPic.image = [UIImage imageNamed:@"signup_pic"];
                    
                }
            }];
            
            [cell.imgViewPic addSubview:activityIndicator];
            [activityIndicator startAnimating];
            //****
        }
        else
        {
            cell.imgViewPic.image = [UIImage imageNamed:@"signup_pic"];
        }
        //        self.userImage.layer.cornerRadius = self.userImage.frame.size.width/2 * ([APPDELEGATE window].bounds.size.width/320);
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            cell.imgViewPic.layer.cornerRadius = 270/2;
        }
        else
        {
            cell.imgViewPic.layer.cornerRadius = cell.imgViewPic.frame.size.width/2;
        }
        
        [cell.imgViewPic setClipsToBounds:YES];
        
        return cell;
    }
    else if (indexPath.row == 1)
    {
        //for name..
        static NSString *signupCellName   = @"SignupCellName";
        SignupCellName *cell = (SignupCellName *)[tableView dequeueReusableCellWithIdentifier:signupCellName];
        
        if (cell == nil)
        {
            cell = (SignupCellName*)[[[NSBundle mainBundle] loadNibNamed:@"SignupCellName" owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.txtFN.delegate = self;
        cell.txtLN.delegate = self;
        cell.txtFN.tag = -1;
        cell.txtLN.tag = indexPath.row;
        
        cell.txtFN.text = [_dictFbProfileInfo objectForKey:@"first_name"];
        cell.txtLN.text = [_dictFbProfileInfo objectForKey:@"last_name"];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            cell.lblPlaceHolderName.font = [UIFont fontWithName:@"Lato-Bold" size:24.0];
            
            cell.txtFN.font = [UIFont fontWithName:@"Lato-Regular" size:29.0];
            cell.txtLN.font = [UIFont fontWithName:@"Lato-Regular" size:29.0];
        }
        
        return cell;
    }
    else if (indexPath.row == 3)
    {
        //for phone
        static NSString *signupCellPhone   = @"SignupCellPhone";
        SignupCellPhone *cell = (SignupCellPhone *)[tableView dequeueReusableCellWithIdentifier:signupCellPhone];
        
        if (cell == nil)
        {
            cell = (SignupCellPhone*)[[[NSBundle mainBundle] loadNibNamed:@"SignupCellPhone" owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [cell.btnCountryCode addTarget:self action:@selector(countryCodeClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.txtPhone.delegate = self;
        cell.txtPhone.tag = indexPath.row;
        
        cell.lblCountryCode.text = _strCountryCode;
        cell.txtPhone.text = [self addSpaceOnPhoneNumber:[_dictFbProfileInfo objectForKey:@"phone"]];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            
            cell.lblPlaceHolderName.font = [UIFont fontWithName:@"Lato-Bold" size:24.0];
            cell.lblCountryCode.font= [UIFont fontWithName:@"Lato-Bold" size:24.0];
            cell.txtPhone.font = [UIFont fontWithName:@"Lato-Regular" size:29.0];
            
        }
        return cell;
    }
    else
    {
        static NSString *signupCellText   = @"SignupCellText";
        SignupCellText *cell = (SignupCellText *)[tableView dequeueReusableCellWithIdentifier:signupCellText];
        
        if (cell == nil)
        {
            cell = (SignupCellText*)[[[NSBundle mainBundle] loadNibNamed:@"SignupCellText" owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.txtTxt.delegate = self;
        if (indexPath.row == 2)
        {
            cell.lblPlaceHolderName.text = @"Location";
            cell.txtTxt.text = [_dictFbProfileInfo objectForKey:@"location"];
        }
        else if (indexPath.row == 4)
        {
            cell.lblPlaceHolderName.text = @"Email Id";
            cell.txtTxt.keyboardType = UIKeyboardTypeEmailAddress;
            cell.txtTxt.autocorrectionType = UITextAutocorrectionTypeNo;
            cell.txtTxt.text = [_dictFbProfileInfo objectForKey:@"email"];
            cell.txtTxt.returnKeyType = UIReturnKeyDone;
            cell.userInteractionEnabled=NO;
        }
        cell.txtTxt.tag = indexPath.row;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            cell.lblPlaceHolderName.font = [UIFont fontWithName:@"Lato-Bold" size:24.0];
            cell.txtTxt.font = [UIFont fontWithName:@"Lato-Regular" size:29.0];
        }
        return cell;
    }
    return nil;
}

#pragma mark -
#pragma mark UIKeyBoard Activity Handling
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unRegisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    tblViewUpdateProfile.contentInset = contentInsets;
    tblViewUpdateProfile.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, currentActiveTextField.frame.origin) ) {
        [tblViewUpdateProfile scrollRectToVisible:currentActiveTextField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    tblViewUpdateProfile.contentInset = contentInsets;
    tblViewUpdateProfile.scrollIndicatorInsets = contentInsets;
    [tblViewUpdateProfile scrollRectToVisible:CGRectZero animated:YES];
    
}

#pragma mark -
#pragma mark UITextField Delegate Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag == 2)
    {
        [currentActiveTextField resignFirstResponder];
    }
    
    currentActiveTextField = textField;
    //    [textField becomeFirstResponder];
    if (textField.tag == 2)
    {
        SignupCellName *cell = (SignupCellName *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        [_dictFbProfileInfo setObject:cell.txtFN.text forKey:@"first_name"];
        [_dictFbProfileInfo setObject:cell.txtLN.text forKey:@"last_name"];
        
                if ([CLLocationManager locationServicesEnabled])
                {
                    NSLog(@"globally enabled location service");
        
                          [self getUserLocation];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enable location access from device settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                }
                [currentActiveTextField resignFirstResponder];
        
            return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self manageSignupInfoDictionary:textField];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self manageSignupInfoDictionary:textField];
    
    NSInteger tag = textField.tag + 1;
    if (tag > 4)
    {
        [textField resignFirstResponder];
    }
    else
    {
        if (tag == 0)
        {
            SignupCellName *cell = (SignupCellName *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            [cell.txtLN becomeFirstResponder];
        }
        else if (tag == 3)
        {
            SignupCellPhone *cell = (SignupCellPhone *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:tag inSection:0]];
            [cell.txtPhone becomeFirstResponder];
        }
        else
        {
            SignupCellText *cell= (SignupCellText *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:tag inSection:0]];
            if (tag == 2)
            {
                [currentActiveTextField resignFirstResponder];
                //                [self getUserLocation];
                
            }
            else
            {
                [cell.txtTxt becomeFirstResponder];
            }
        }
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == 3)
    {
        int length = (int)[self getLength:textField.text];
        //NSLog(@"Length  =  %d ",length);
        
        if(length == 15)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"%@ ",num];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        else if(length == 6)
        {
            NSString *num = [self formatNumber:textField.text];
            //NSLog(@"%@",[num  substringToIndex:3]);
            //NSLog(@"%@",[num substringFromIndex:3]);
            textField.text = [NSString stringWithFormat:@"%@ %@ ",[num  substringToIndex:3],[num substringFromIndex:3]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@ %@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
    }
    
    if (!(([string isEqualToString:@""]))) {//not a backspace, else `characterAtIndex` will crash.
        unichar unicodevalue = [string characterAtIndex:0];
        if (unicodevalue == 55357) {
            return NO;
        }
        //        else if ([string isEqualToString:@" "])
        //        {
        //            return NO;
        //        }
    }
    return YES;
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    //    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    //    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 16)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-16];
        NSLog(@"%@", mobileNumber);
        
    }
    return mobileNumber;
}

- (int)getLength:(NSString *)mobileNumber
{
    //    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    //    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}


-(void)getUserLocation
{
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [self.view endEditing:YES];
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(CurrentLatitude, CurrentLongitude);
    
    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001);
    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001);
    GMSCoordinateBounds *viewport = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
                                                                         coordinate:southWest];
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:viewport];
    _placePicker = [[GMSPlacePicker alloc] initWithConfig:config];
    
    [_placePicker pickPlaceWithCallback:^(GMSPlace *place, NSError *error) {
        if (error != nil) {
            NSLog(@"Pick Place error %@", [error localizedDescription]);
            return;
        }
        
        if (place != nil)
        {
        [[UIBarButtonItem appearance] setTintColor:nil];
            strAddress = place.formattedAddress;
            if (strAddress) {
                [_dictFbProfileInfo setObject:strAddress forKey:@"location"];
            }
            strAddress = place.name;
            if (strAddress) {
                [_dictFbProfileInfo setObject:strAddress forKey:@"location"];
            }
            
            [tblViewUpdateProfile reloadData];
            NSLog(@"Place name %@", place.name);
            NSLog(@"Place address %@", place.formattedAddress);
            NSLog(@"Place attributions %@", place.attributions.string);
        } else {
            NSLog(@"No place selected");
        }
    }];
    
    
}

-(void)manageSignupInfoDictionary:(UITextField *)txtField
{
    if (txtField.tag == -1)
    {
        [_dictFbProfileInfo setObject:txtField.text forKey:@"first_name"];
    }
    else if (txtField.tag == 1)
    {
        [_dictFbProfileInfo setObject:txtField.text forKey:@"last_name"];
    }
    else if (txtField.tag == 2)
    {
        [_dictFbProfileInfo setObject:txtField.text forKey:@"location"];
    }
    else if (txtField.tag == 3)
    {
        SignupCellPhone *cell = (SignupCellPhone *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:txtField.tag inSection:0]];
        [_dictFbProfileInfo setObject:cell.lblCountryCode.text forKey:@"countrycode"];
        [_dictFbProfileInfo setObject:txtField.text forKey:@"phone"];
    }
    else if (txtField.tag == 4)
    {
        [_dictFbProfileInfo setObject:txtField.text forKey:@"email"];
    }
}

-(BOOL)validateUpdateProfile
{
    NSString *errormsg;
    BOOL error=FALSE;
    
    NSString *strFN = [_dictFbProfileInfo objectForKey:@"first_name"];
    NSString *strLN = [_dictFbProfileInfo objectForKey:@"last_name"];
    NSString *strLoc = [_dictFbProfileInfo objectForKey:@"location"];
    NSString *strCC = [_dictFbProfileInfo objectForKey:@"countrycode"];
    NSString *strPhone = [_dictFbProfileInfo objectForKey:@"phone"];
    strPhone = [strPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *strEmail = [_dictFbProfileInfo objectForKey:@"email"];
    
    UITextField *tf;
    if (![CommonFunction isValueNotEmpty:strFN])
    {
        errormsg = @"Please enter your first name to continue.";
        error=TRUE;
        
        SignupCellName *cell = (SignupCellName *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        
        tf = cell.txtFN;
        [tf becomeFirstResponder];
    }
    else if (!error && ![CommonFunction isValueNotEmpty:strLN])
    {
        errormsg = @"Please enter your last name to continue.";
        error=TRUE;
        SignupCellName *cell = (SignupCellName *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        
        tf = cell.txtLN;
        [tf becomeFirstResponder];
    }
    else if (!error && ![CommonFunction isValueNotEmpty:strLoc])
    {
        errormsg = @"Please enter your location to continue.";
        error=TRUE;
        SignupCellText *cell = (SignupCellText *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
        
        tf = cell.txtTxt;
        //        [tf becomeFirstResponder];
    }
    else if (!error && ![CommonFunction isValueNotEmpty:strCC])
    {
        errormsg = @"Please enter your country code to continue.";
        error=TRUE;
        //        SignupCellPhone *cell = (SignupCellPhone *) [tblViewSignup cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
        
        //        tf = cell.btnCountryCode;
        //        [tf becomeFirstResponder];
    }
    else if (!error && ![CommonFunction isValueNotEmpty:strPhone])
    {
        errormsg = @"Please enter your phone no. to continue.";
        error=TRUE;
        SignupCellPhone *cell = (SignupCellPhone *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        
        tf = cell.txtPhone;
        [tf becomeFirstResponder];
    }
    else if (!error && !(strPhone.length >= 5 || strPhone.length <= 15))
    {
        errormsg = @"Please enter your correct phone no. to continue.";
        error=TRUE;
        SignupCellPhone *cell = (SignupCellPhone *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        
        tf = cell.txtPhone;
        [tf becomeFirstResponder];
    }
    else if (!error && ![UtilityClass isValidMobileNumber:strPhone])
    {
        errormsg = @"Please enter your correct phone no. to continue.";
        error=TRUE;
        SignupCellPhone *cell = (SignupCellPhone *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        
        tf = cell.txtPhone;
        [tf becomeFirstResponder];
    }
    else if (!error && ![CommonFunction isValueNotEmpty:strEmail])
    {
        errormsg = @"Please enter your email address to continue.";
        error=TRUE;
        SignupCellPhone *cell = (SignupCellPhone *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        
        tf = cell.txtPhone;
        [tf becomeFirstResponder];
    }
    else if (!error && ![CommonFunction IsValidEmail:strEmail])
    {
        errormsg = @"Enter your correct email address";
        error=TRUE;
        SignupCellText *cell = (SignupCellText *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
        
        tf = cell.txtTxt;
        [tf becomeFirstResponder];
    }
    
    if(error)
    {
        [CommonFunction showAlertWithTitle:@"" message:errormsg onViewController:self useAsDelegate:NO dismissBlock:^{
            
        }];
        return FALSE;
    }
    return TRUE;
}

-(void)countryCodeClicked:(id)sender
{
    NSLog(@"countryCodeClicked");
    //    _strCountryCode = @"";
    CountryCodeVC *obj = [[CountryCodeVC alloc]initWithNibName:@"CountryCodeVC" bundle:nil];
    [self.navigationController pushViewController:obj animated:YES];
}


-(void)createAccountBtnClicked
{
    [currentActiveTextField resignFirstResponder];
    
    NSLog(@"createAccountBtnClicked");
    NSLog(@"btnSignupClicked");
    if (![CommonFunction reachabiltyCheck])
    {
        return;
    }
    
    [currentActiveTextField resignFirstResponder];
    
    if ([self validateUpdateProfile])
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self sendRequestForUpdateProfileToServer];
    }
}

-(void)sendRequestForUpdateProfileToServer
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    [param setObject:[_dictFbProfileInfo objectForKey:@"email"] forKey:@"email"];
    [param setObject:[_dictFbProfileInfo objectForKey:@"first_name"] forKey:@"first_name"];
    [param setObject:[_dictFbProfileInfo objectForKey:@"last_name"] forKey:@"last_name"];
    [param setObject:[_dictFbProfileInfo objectForKey:@"countrycode"] forKey:@"country_id"];
    [param setObject:[_dictFbProfileInfo objectForKey:@"phone"] forKey:@"phone"];
    [param setObject:[_dictFbProfileInfo objectForKey:@"location"] forKey:@"address"];
    
    
    if (isImageCaptured)
    {
        [self uplaodImageAndDataInfoOnServer:[param copy]];
    }
    else
    {
        
        NSString *url = [NSString stringWithFormat:@"update_profile/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
        NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
        
        ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
        
        [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
            NSLog(@"responce dict %@",responseDict);
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            if(responseDict == Nil)        {
                
                [CommonFunction alertTitle:@"" withMessage:@"Server error"];
            }
            
            else if([operation.response statusCode]==200)
            {
                if ([[responseDict valueForKey:@"type"]boolValue])
                {
                    /*
                     //successfully registered here... show alert here..
                     if (userDefaults == nil)
                     {
                     userDefaults = [NSUserDefaults standardUserDefaults];
                     }
                     
                     [UtilityClass saveUserInfoWithDictionary:[responseDict objectForKey:@"data"]];
                     [userDefaults synchronize];
                     */
                    
                    [CommonFunction showAlertWithTitle:@"" message:[responseDict objectForKey:@"message"] onViewController:self useAsDelegate:YES dismissBlock:^
                     {
                         if (userDefaultss == nil)
                         {
                             userDefaultss = [NSUserDefaults standardUserDefaults];
                         }
                         [UtilityClass saveUserInfoWithDictionary: [responseDict objectForKey:@"data"]];
                         [userDefaultss synchronize];
                         
                         [self.navigationController popViewControllerAnimated:YES];
                     }];
                }
                else
                {
                    [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
                }
                
            }
            else
            {
                [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"message"]];
            }
        }
                                          withFailure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [CommonFunction removeActivityIndicator];
             if([operation.response statusCode]  == 400 )
             {
                 NSLog(@"impo response %@",operation.response);
                 
                 if([operation.response statusCode]  == 426)
                 {
                     NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                     NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                     [CommonFunction showAlertWithTitle:@"Jetseta" message:serializedData[@"data"][@"messageOperator"] onViewController:self useAsDelegate:NO dismissBlock:^{
                         
                         NSString *iTunesLink = serializedData[@"data"][@"appLinkOperator"];
                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                     }];
                 }

                 else
                 {
                 
                     [CommonFunction showAlertWithTitle:@"Jetseta" message:[operation.responseObject objectForKey:@"response"] onViewController:self useAsDelegate:NO dismissBlock:nil];
                 }
             }
         }];
    }
}

-(void)uplaodImageAndDataInfoOnServer:(NSDictionary *)params
{
    if (userDefaultss)
    {
        userDefaultss = [NSUserDefaults standardUserDefaults];
    }
    
    NSString *strUrl = [NSString stringWithFormat:@"update_profile/%@",[NSUSERDEFAULTS objectForKey:kUSERTOKEN]];
    NSString *serviceUrl = [serverURL stringByAppendingString:strUrl];
    NSURL *url = [NSURL URLWithString:serviceUrl];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
//    [headers setObject:version forKey:@"version"];
    [manager.requestSerializer setValue:version forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"os"];
    if (isImageCaptured)
    {
        SignupCellPic *cell = (SignupCellPic *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        UIImage *capturedImage = cell.imgViewPic.image;
        
        AFHTTPRequestOperation *op = [manager POST:@"" parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                      {
                                          NSData *imageData1 = UIImageJPEGRepresentation(capturedImage,0.5f);
                                          NSLog(@"0.8 %lu",(unsigned long)imageData1.length);
                                          
                                          NSString *fileImg1 = [NSString stringWithFormat:@"picture"];
                                          NSString *parameterNm1 = [NSString stringWithFormat:@"picture"];
                                          
                                          [formData appendPartWithFileData:imageData1 name:parameterNm1 fileName:fileImg1 mimeType:@"image/jpeg"];
                                      }
                                           success:^(AFHTTPRequestOperation *operation, id responseObject)
                                      {
                                          [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

                                          if (responseObject != nil)
                                          {
                                              [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                              NSDictionary *jsonDic = (NSDictionary *)responseObject;
                                              
                                              if ([[[jsonDic objectForKey:@"meta"]objectForKey:@"hasUpdate"]boolValue])
                                              {
                                                  [CommonFunction showAlertWithTitle:@"" message:NSLocalizedString(@"New version is available on Appstore.\n you have to update Jetseta OP.", nil) onViewController:self useAsDelegate:NO dismissBlock:^{
                                                      
                                                      LoginVC *fufs = [[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil];
                                                      APPDELEGATE.navigationController=[[UINavigationController alloc]initWithRootViewController:fufs];
                                                      self.navigationController.navigationBarHidden=YES;
                                                      [APPDELEGATE.window.window setRootViewController:self.navigationController];
                                                      [NSUSERDEFAULTS removeObjectForKey:@"previousSelectedVC"];
                                                      [NSUSERDEFAULTS removeObjectForKey:kUSERNAME];
                                                      [NSUSERDEFAULTS removeObjectForKey:kOperatorName];
                                                      [NSUSERDEFAULTS removeObjectForKey:kUSERTOKEN];
                                                      [NSUSERDEFAULTS removeObjectForKey:kPUBLISH_AIRCRAFT];
                                                      [NSUSERDEFAULTS removeObjectForKey:kPUBLISH_CHARTER];
                                                      [NSUSERDEFAULTS synchronize];
                                                      [CommonFunction changeRootViewController:NO aircraftPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_AIRCRAFT]boolValue] charterPublish:[[NSUSERDEFAULTS valueForKey:kPUBLISH_CHARTER]boolValue]];
                                                      
                                                  }];
                                                  /*
                                                   [UtilityClass showAlertWithTitle:NSLocalizedString(@"Generics", nil) message:NSLocalizedString(@"New version is available on Appstore.\n you have to update Generics.", nil) onViewController:self useAsDelegate:YES dismissBlock:^{
                                                   //remove all userdefaults data...
                                                   [UtilityClass moveToStartingDueToForceUpdate];
                                                   }];
                                                   */
                                              }
                                              
                                              else if ([jsonDic[@"type"]boolValue])
                                              {
                                                  [UtilityClass showAlertWithTitle:@"" message:[jsonDic objectForKey:@"message"] onViewController:self useAsDelegate:YES dismissBlock:^
                                                   {
                                                       if (userDefaultss == nil)
                                                       {
                                                           userDefaultss = [NSUserDefaults standardUserDefaults];
                                                       }
                                                       [UtilityClass saveUserInfoWithDictionary: [jsonDic objectForKey:@"data"]];
                                                       [userDefaultss synchronize];
                                                       
                                                       [self.navigationController popViewControllerAnimated:YES];
                                                   }];
                                              }
                                              else
                                              {
                                                  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                                  [UtilityClass showAlertWithTitle:@"" message:jsonDic[@"message"] onViewController:self useAsDelegate:NO dismissBlock:nil];
                                              }
                                          }
                                      }
                                           failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                      {
                                          [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

                                          NSLog(@"Error: %@ ***** %@", operation.responseString, error);
                                      }];
        
        [op start];
    }
}

-(void)removeKeyboard
{
    [currentActiveTextField resignFirstResponder];
}

-(void)locationAllowed
{
    //    [self getUserLocation];
}

-(void)locationDenied
{
    //    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enable location access from device settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    //    [alert show];
}

-(void)picBtnClicked
{
    [self removeKeyboard];
    SignupCellPic *cell = (SignupCellPic *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    UIAlertController *view = [UIAlertController
                               alertControllerWithTitle:@"Select picture"
                               message:@""
                               preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* btnCapturePhoto = [UIAlertAction
                                      actionWithTitle:@"Take a photo"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          //Do some thing here
                                          [self imageCaptureFromCamera];
                                          [view dismissViewControllerAnimated:YES completion:^{
                                              [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
                                          }];
                                          
                                      }];
    UIAlertAction* btnSelectPhoto = [UIAlertAction
                                     actionWithTitle:@"Choose from existing photos"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self imageCaptureFromPhotoLibrary];
                                         [view dismissViewControllerAnimated:YES completion:^{
                                             [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
                                         }];
                                         
                                     }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:^{
                                     [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
                                 }];
                                 
                             }];
    
    
    [view addAction:btnCapturePhoto];
    [view addAction:btnSelectPhoto];
    [view addAction:cancel];
    
    if ( IPAD)
    {
        
        // show action sheet
        view.popoverPresentationController.sourceView = self.view;
        view.popoverPresentationController.sourceRect = CGRectMake(240,0, cell.imgViewPic.frame.size.width, cell.imgViewPic.frame.size.height);
        view.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
        
        [self presentViewController:view animated:YES completion:^{
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        }];
    }
    else
    {
        [self presentViewController:view animated:YES completion:^{
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        }];
    }
    
    
}

-(void)imageCaptureFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
          imagePickerObject = [[ImageCapture alloc]initWithSourceType:kSourceTypeCamera];
        [imagePickerObject presentImagePickerOnController:self onDidFinish:^(NSDictionary *imageInfo)
         {
             isImageCaptured = YES;
             
             UIImage *capturedImage = [imageInfo valueForKey:UIImagePickerControllerEditedImage];
             NSLog(@"success");
             
             //            SignupCellPic *cell = (SignupCellPic *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
             //            cell.imgViewPic.image = capturedImage;
             [_dictFbProfileInfo setObject:capturedImage forKey:@"picture"];
             
             [tblViewUpdateProfile reloadData];
             
         } onCancel:^{
             NSLog(@"failure");
             //isImageCaptured = NO;
            // [_dictFbProfileInfo removeObjectForKey:@"picture"];
            // [tblViewUpdateProfile reloadData];
         }];
    }
    else
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"No camera"
                                      message:@"Camera is unavailable on this device."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //Do some thing here
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)imageCaptureFromPhotoLibrary
{
    imagePickerObject = [[ImageCapture alloc]initWithSourceType:kSourceTypeLibrary];
    [imagePickerObject presentImagePickerOnController:self onDidFinish:^(NSDictionary *imageInfo)
     {
         isImageCaptured = YES;
         UIImage *capturedImage = [imageInfo valueForKey:UIImagePickerControllerOriginalImage];
         
         //        SignupCellPic *cell = (SignupCellPic *) [tblViewUpdateProfile cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
         //        cell.imgViewPic.image = capturedImage;
         [_dictFbProfileInfo setObject:capturedImage forKey:@"picture"];
         [tblViewUpdateProfile reloadData];
         NSLog(@"success");
         
     } onCancel:^
     {
         NSLog(@"failure");
//         isImageCaptured = NO;
//         [_dictFbProfileInfo removeObjectForKey:@"picture"];
//         [tblViewUpdateProfile reloadData];
     }];
}

-(void)setUserProfileInfoData
{
    /*
     [param setObject:appDeviceToken forKey:@"device_token"];
     [param setObject:[_dictFbProfileInfo objectForKey:@"email"] forKey:@"email"];
     [param setObject:[_dictFbProfileInfo objectForKey:@"first_name"] forKey:@"first_name"];
     [param setObject:[_dictFbProfileInfo objectForKey:@"last_name"] forKey:@"last_name"];
     [param setObject:[_dictFbProfileInfo objectForKey:@"countrycode"] forKey:@"country_id"];
     [param setObject:[_dictFbProfileInfo objectForKey:@"phone"] forKey:@"phone"];
     [param setObject:[_dictFbProfileInfo objectForKey:@"location"] forKey:@"address"];
     [param setObject:[NSString stringWithFormat:@"%f", core_latitude] forKey:@"latitude"];
     [param setObject:[NSString stringWithFormat:@"%f", core_longitude] forKey:@"longitude"];
     [param setObject:[_dictFbProfileInfo objectForKey:@"id"] forKey:@"facebook_id"];
     */
    NSLog(@"%@", [UtilityClass getUserInfoDictionary]);
    NSDictionary *dictUserInfo = [UtilityClass getUserInfoDictionary];
    
    if (_dictFbProfileInfo)
    {
        [_dictFbProfileInfo removeAllObjects];
        _dictFbProfileInfo = nil;
    }
    _dictFbProfileInfo = [NSMutableDictionary dictionary];
    
    if ([dictUserInfo objectForKey:@"email"])
    {
        [_dictFbProfileInfo setObject:[dictUserInfo objectForKey:@"email"] forKey:@"email"];
    }
    if ([dictUserInfo objectForKey:@"first_name"])
    {
        [_dictFbProfileInfo setObject:[dictUserInfo objectForKey:@"first_name"] forKey:@"first_name"];
    }
    if ([dictUserInfo objectForKey:@"last_name"])
    {
        [_dictFbProfileInfo setObject:[dictUserInfo objectForKey:@"last_name"] forKey:@"last_name"];
    }
    if ([dictUserInfo objectForKey:@"last_name"])
    {
        [_dictFbProfileInfo setObject:[dictUserInfo objectForKey:@"last_name"] forKey:@"last_name"];
    }
    if ([dictUserInfo objectForKey:@"phone"])
    {
        [_dictFbProfileInfo setObject:[dictUserInfo objectForKey:@"phone"] forKey:@"phone"];
    }
    if ([dictUserInfo objectForKey:@"country_id"])
    {
        [_dictFbProfileInfo setObject:[dictUserInfo objectForKey:@"country_id"] forKey:@"countrycode"];
        _strCountryCode = [dictUserInfo objectForKey:@"country_id"];
    }
    if ([dictUserInfo objectForKey:@"address"])
    {
        [_dictFbProfileInfo setObject:[dictUserInfo objectForKey:@"address"] forKey:@"location"];
    }
    if ([dictUserInfo objectForKey:@"picture"])
    {
        [_dictFbProfileInfo setObject:[dictUserInfo objectForKey:@"picture"] forKey:@"picture"];
    }
    
    NSLog(@"_dictFbProfileInfo : %@", _dictFbProfileInfo);
    [tblViewUpdateProfile reloadData];
}

- (NSString *)addSpaceOnPhoneNumber:(NSString*)originalString {
    NSMutableString *liS=[[NSMutableString alloc]init];
    for (int i=0; i < [originalString length]; i++)
    {
        NSString *ichar  = [NSString stringWithFormat:@"%c", [originalString characterAtIndex:i]];
        [liS appendString:ichar];
        if (i==2||i==6)
        {
            [liS appendString:@" "];
        }
    }
    
    originalString=liS;
    NSLog(@"updated string is %@",originalString);
    return originalString;
}

-(IBAction)leftButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
