//
//  CropperVC.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 19/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "CropperVC.h"

@interface CropperVC ()<UIScrollViewDelegate>
{
    
    IBOutlet UIScrollView *imgScrollVw;
    IBOutlet UIImageView *imgFull;
}
@end

@implementation CropperVC
@synthesize ImgSelectedImage;
- (void)viewDidLoad {
    [super viewDidLoad];
    imgScrollVw.minimumZoomScale = 0.5;
    imgScrollVw.maximumZoomScale = 6.0;
    imgScrollVw.contentSize = imgFull.frame.size;
    imgScrollVw.delegate = self;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [imgFull setImage:ImgSelectedImage];
}
- (IBAction)donebtnClicked:(id)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ScrollVwDelegate Method
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return imgFull;
}
-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    
}

@end
