//
//  AddAircraftCell.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 13/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum : NSUInteger {
    CELL_TYPE_LABEL,
    CELL_TYPE_COLLECTION
} CELLTYPE;
@interface AddAircraftCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAIrcraftName;
@property (weak, nonatomic) IBOutlet UITextView *txtDiscriptionView;
@property (weak, nonatomic) IBOutlet UILabel *lblMsg;
@property (weak, nonatomic) IBOutlet UILabel *lblUpdateAIrcraftTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnOther;

@property (strong, nonatomic) IBOutlet UILabel *lblTextFieldName;
@property (strong, nonatomic) IBOutlet UITextField *txtField;
@property (strong, nonatomic) IBOutlet UIImageView *imgBottomArrow;
@property (strong, nonatomic) IBOutlet UILabel *lblKm;
@property (weak, nonatomic) IBOutlet UILabel *lblAircraftDiscription;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionImageVw;
@property(assign)CELLTYPE cellType;

@property (weak, nonatomic) IBOutlet UILabel *lblAircraftPhoto;
-(void)adjustCellWithType:(CELLTYPE)type;
@end
