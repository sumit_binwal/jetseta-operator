//
//  AddAircraftCell.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 13/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "AddAircraftCell.h"
#import "AddAircraftImageCell.h"
@implementation AddAircraftCell


- (void)awakeFromNib {
    // Initialization code
    
    if (IPAD) {
        [self.lblTextFieldName setFont:[UIFont fontWithName:self.lblTextFieldName.font.fontName  size:22.0f]];
        [self.lblUpdateAIrcraftTitle setFont:[UIFont fontWithName:self.lblUpdateAIrcraftTitle.font.fontName  size:22.0f]];
        [self.lblAIrcraftName setFont:[UIFont fontWithName:self.lblTextFieldName.font.fontName  size:22.0f]];
        [self.lblKm setFont:[UIFont fontWithName:self.lblKm.font.fontName  size:27.0f]];
        [self.txtDiscriptionView setFont:[UIFont fontWithName:self.txtDiscriptionView.font.fontName  size:27.0f]];
        [self.txtField setFont:[UIFont fontWithName:self.txtField.font.fontName  size:27.0f]];
        [self.lblAircraftPhoto setFont:[UIFont fontWithName:self.lblAircraftPhoto.font.fontName  size:22.0f]];
        [self.lblMsg setFont:[UIFont fontWithName:self.lblMsg.font.fontName  size:18.0f]];
        [self.lblAircraftDiscription setFont:[UIFont fontWithName:self.lblAircraftDiscription.font.fontName  size:22.0f]];
        [self.btnOther.titleLabel setFont:[UIFont fontWithName:self.btnOther.titleLabel.font.fontName size:27.0f]];
    }
    else
    {
        [self.lblUpdateAIrcraftTitle setFont:[UIFont fontWithName:self.lblUpdateAIrcraftTitle.font.fontName  size:12*SCREEN_XScale]];
        [self.lblTextFieldName setFont:[UIFont fontWithName:self.lblTextFieldName.font.fontName  size:12*SCREEN_XScale]];
        [self.lblAIrcraftName setFont:[UIFont fontWithName:self.lblTextFieldName.font.fontName  size:12*SCREEN_XScale]];
        [self.lblKm setFont:[UIFont fontWithName:self.lblKm.font.fontName  size:15*SCREEN_XScale]];
        [self.txtDiscriptionView setFont:[UIFont fontWithName:self.txtDiscriptionView.font.fontName  size:15*SCREEN_XScale]];
        [self.btnOther.titleLabel setFont:[UIFont fontWithName:self.btnOther.titleLabel.font.fontName size:12.0f*SCREEN_XScale]];
        [self.txtField setFont:[UIFont fontWithName:self.txtField.font.fontName  size:15*SCREEN_XScale]];
        
        [self.lblAircraftDiscription setFont:[UIFont fontWithName:self.lblAircraftDiscription.font.fontName  size:12*SCREEN_XScale]];
        [self.lblAircraftPhoto setFont:[UIFont fontWithName:self.lblAircraftPhoto.font.fontName  size:12*SCREEN_XScale]];
        
        [self.lblMsg setFont:[UIFont fontWithName:self.lblMsg.font.fontName  size:10*SCREEN_XScale]];


        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)adjustCellWithType:(CELLTYPE)type
{
    switch (type) {
        case CELL_TYPE_COLLECTION:
            [self.collectionImageVw registerNib:[UINib nibWithNibName:NSStringFromClass([AddAircraftImageCell class]) bundle:nil] forCellWithReuseIdentifier:@"AddAircraftImageCell"];
            break;
        default:
            break;
    }
}

@end
