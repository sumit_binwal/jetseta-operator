//
//  AddAircraftImageCell.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 14/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAircraftImageCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnAircraftImg;
@property (strong, nonatomic) IBOutlet UIButton *btnCross;
@end
