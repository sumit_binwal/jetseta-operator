//
//  AddAircraftCell.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 13/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "AircraftDetailCell.h"

@implementation AircraftDetailCell


- (void)awakeFromNib {
    // Initialization code
    
    if (IPAD) {

    [self.lblTextFieldName setFont:[UIFont fontWithName:self.lblTextFieldName.font.fontName  size:14.0f]];
    [self.lblDiscription setFont:[UIFont fontWithName:self.lblDiscription.font.fontName  size:22.0f]];


    }
    else
    {
        [self.lblTextFieldName setFont:[UIFont fontWithName:self.lblTextFieldName.font.fontName  size:8*SCREEN_XScale]];
       [self.lblDiscription setFont:[UIFont fontWithName:self.lblDiscription.font.fontName  size:12*SCREEN_XScale]];

        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




@end
