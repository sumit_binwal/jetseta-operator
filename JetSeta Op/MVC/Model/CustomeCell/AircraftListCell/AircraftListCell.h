//
//  AircraftListCell.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 13/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AircraftListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UIImageView *imgAircraftImg;
@property (strong, nonatomic) IBOutlet UIView *vwLegBg;
@property (strong, nonatomic) IBOutlet UILabel *lblLeg;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDiscription;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblNumberofUser;
@property (strong, nonatomic) IBOutlet UILabel *lblUsd;

@end
