//
//  AircraftListCell.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 13/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "AircraftListCell.h"

@implementation AircraftListCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    return self=[[[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AircraftListCell class]) owner:self options:nil]objectAtIndex:0];
}
- (void)awakeFromNib {
    // Initialization code
    if (IPAD) {
    [self.lblTitle setFont:[UIFont fontWithName:self.lblTitle.font.fontName size:27]];
    [self.lblDiscription setFont:[UIFont fontWithName:self.lblDiscription.font.fontName size:19]];
    [self.lblPrice setFont:[UIFont fontWithName:self.lblPrice.font.fontName size:22]];
    [self.lblNumberofUser setFont:[UIFont fontWithName:self.lblNumberofUser.font.fontName size:15]];
    [self.lblLocation setFont:[UIFont fontWithName:self.lblLocation.font.fontName size:19]];
    [self.lblLeg setFont:[UIFont fontWithName:self.lblLeg.font.fontName size:15]];
    [self.lblUsd setFont:[UIFont fontWithName:self.lblUsd.font.fontName size:22]];
        
    }
    else
    {
        [self.lblTitle setFont:[UIFont fontWithName:self.lblTitle.font.fontName size:15*SCREEN_XScale]];
        [self.lblLeg setFont:[UIFont fontWithName:self.lblLeg.font.fontName size:8*SCREEN_XScale]];
        [self.lblDiscription setFont:[UIFont fontWithName:self.lblDiscription.font.fontName size:10*SCREEN_XScale]];
        [self.lblUsd setFont:[UIFont fontWithName:self.lblUsd.font.fontName size:12*SCREEN_XScale]];
        [self.lblPrice setFont:[UIFont fontWithName:self.lblPrice.font.fontName size:12*SCREEN_XScale]];
        
        [self.lblNumberofUser setFont:[UIFont fontWithName:self.lblNumberofUser.font.fontName size:9*SCREEN_XScale]];
        [self.lblLocation setFont:[UIFont fontWithName:self.lblLocation.font.fontName size:9*SCREEN_XScale]];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
