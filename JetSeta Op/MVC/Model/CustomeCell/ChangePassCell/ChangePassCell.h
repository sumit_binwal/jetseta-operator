//
//  ChangePassCell.h
//  JetSeta
//
//  Created by Suchita Bohra on 22/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePassCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblPlaceHolderName;
@property (nonatomic, strong)IBOutlet UITextField *txtTxt;

@end
