//
//  CountryCodeCell.h
//  JetSeta
//
//  Created by Suchita Bohra on 15/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryCodeCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblCountryName;
@property (nonatomic, strong) IBOutlet UILabel *lblCountryCode;
@property (nonatomic, strong) IBOutlet UIImageView *imgChkMark;

@property (nonatomic, strong) IBOutlet UIImageView *imgViewChkMarkAirPortSearch;

@end
