//
//  ImageCollectionCell.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 18/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgVw;

@end
