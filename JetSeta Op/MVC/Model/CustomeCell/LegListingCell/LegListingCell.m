//
//  LegListingCell.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 14/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "LegListingCell.h"

@implementation LegListingCell

- (void)awakeFromNib {
    // Initialization code
    
    if (IPAD) {
        [self.lblAvlble setFont:[UIFont fontWithName:self.lblAvlble.font.fontName size:15]];
        [self.lblTotalCost setFont:[UIFont fontWithName:self.lblTotalCost.font.fontName size:19]];
        [self.lblTitle setFont:[UIFont fontWithName:self.lblTitle.font.fontName size:27]];
        [self.lblCharter setFont:[UIFont fontWithName:self.lblCharter.font.fontName size:15]];
        [self.lblMnth setFont:[UIFont fontWithName:self.lblMnth.font.fontName size:15]];
        [self.lbltime setFont:[UIFont fontWithName:self.lbltime.font.fontName size:15]];
        [self.lblPrice setFont:[UIFont fontWithName:self.lblPrice.font.fontName size:19]];
        [self.lblUsd setFont:[UIFont fontWithName:self.lblUsd.font.fontName size:19]];
    }
    else
    {
        [self.lblAvlble setFont:[UIFont fontWithName:self.lblAvlble.font.fontName size:9*SCREEN_XScale]];
        [self.lblTitle setFont:[UIFont fontWithName:self.lblTitle.font.fontName size:15*SCREEN_XScale]];
        [self.lblCharter setFont:[UIFont fontWithName:self.lblCharter.font.fontName size:8*SCREEN_XScale]];
        
        [self.lblMnth setFont:[UIFont fontWithName:self.lblMnth.font.fontName size:11*SCREEN_XScale]];
        
        [self.lbltime setFont:[UIFont fontWithName:self.lbltime.font.fontName size:10*SCREEN_XScale]];
        [self.lblPrice setFont:[UIFont fontWithName:self.lblPrice.font.fontName size:11*SCREEN_XScale]];
        [self.lblTotalCost setFont:[UIFont fontWithName:self.lblTotalCost.font.fontName size:11*SCREEN_XScale]];
        
        [self.lblUsd setFont:[UIFont fontWithName:self.lblUsd.font.fontName size:11*SCREEN_XScale]];
    }
    
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
        return self=[[[NSBundle mainBundle]loadNibNamed:NSStringFromClass([LegListingCell class]) owner:self options:nil]objectAtIndex:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
