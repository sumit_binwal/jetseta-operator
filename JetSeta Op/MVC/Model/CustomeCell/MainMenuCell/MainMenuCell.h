//
//  MainMenuCell.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 27/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenuCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblMenuItem;

@end
