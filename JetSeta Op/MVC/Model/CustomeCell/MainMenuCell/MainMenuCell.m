//
//  MainMenuCell.m
//  JetSeta Op
//
//  Created by Sumit Sharma on 27/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import "MainMenuCell.h"

@implementation MainMenuCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    return self=[[[NSBundle mainBundle]loadNibNamed:NSStringFromClass([MainMenuCell class]) owner:self options:nil]objectAtIndex:0];
}
- (void)awakeFromNib {
    // Initialization code
    if (IPAD) {
              [self.lblMenuItem setFont:[UIFont fontWithName:@"Lato-Regular" size:26.0f]];
    }
    else
    {
              [self.lblMenuItem setFont:[UIFont fontWithName:@"Lato-Regular" size:15.0f*SCREEN_XScale]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
