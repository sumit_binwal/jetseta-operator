//
//  NotificationCell.h
//  JetSeta Op
//
//  Created by Sumit Sharma on 25/04/16.
//  Copyright © 2016 Sumit Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblBookType;

@property (strong, nonatomic) IBOutlet UILabel *lblFor;
@property (strong, nonatomic) IBOutlet UILabel *lblFrom;
@property (strong, nonatomic) IBOutlet UILabel *lblTo;
@property (strong, nonatomic) IBOutlet UILabel *lblAt;


@property (weak, nonatomic) IBOutlet UILabel *lblAdminMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblAIrcraftName;
@property (strong, nonatomic) IBOutlet UILabel *lblAircraftType;
@property (strong, nonatomic) IBOutlet UILabel *lblSource;
@property (strong, nonatomic) IBOutlet UILabel *lblDestination;
@property (strong, nonatomic) IBOutlet UILabel *lblDepartureDate;
@property (strong, nonatomic) IBOutlet UILabel *lblDepartureTime;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UIButton *btnCheck;
@property (strong, nonatomic) IBOutlet UIButton *btnCross;
@property (strong, nonatomic) IBOutlet UILabel *lblUserCount;
@property (weak, nonatomic) IBOutlet UILabel *lblArrivalDate;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceunit;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureDatee;
@property (weak, nonatomic) IBOutlet UILabel *lblCnfirmTime;

@end
