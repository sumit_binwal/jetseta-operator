//
//  SettingsCell.h
//  JetSeta
//
//  Created by Suchita Bohra on 21/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblTxt;
@property (nonatomic, strong) IBOutlet UISwitch *switchNoti;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewArrow;

@end
