//
//  SignupCellDone.h
//  JetSeta
//
//  Created by Suchita Bohra on 14/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface SignupCellDone : UITableViewCell

@property (nonatomic, strong) IBOutlet UIButton *btnCreate;
@property (nonatomic, weak) IBOutlet TTTAttributedLabel *lblTC;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *btnWidthCons;

@end
