//
//  SignupCellDone.m
//  JetSeta
//
//  Created by Suchita Bohra on 14/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import "SignupCellDone.h"

@implementation SignupCellDone

- (void)awakeFromNib
{
    // Initialization code
    
//    self.lblTC.delegate = self;
    
    if ([AppDelegate getSharedInstance].window.frame.size.width == 375)
    {
        CGRect frameBtn = self.btnCreate.frame;
        frameBtn.size.width = 306;
        self.btnCreate.frame = frameBtn;
        self.btnWidthCons.constant = 306.0f;
        [self layoutSubviews];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
