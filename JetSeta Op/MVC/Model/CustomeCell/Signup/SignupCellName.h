//
//  SignupCellName.h
//  JetSeta
//
//  Created by Suchita Bohra on 14/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupCellName : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lblPlaceHolderName;
@property (nonatomic,strong) IBOutlet UITextField *txtFN;
@property (nonatomic, strong)IBOutlet UITextField *txtLN;

@end
