//
//  SignupCellPic.h
//  JetSeta
//
//  Created by Suchita Bohra on 22/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupCellPic : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *imgViewPic;

@end
