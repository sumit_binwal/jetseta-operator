//
//  KIPLPickerView.h
//  Tabco App
//
//  Created by Santosh on 08/02/16.
//  Copyright © 2016 Vaibhav Khatri. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KIPLDelegate;
@interface KIPLPickerView : UIView
{
    
}
@property (nonatomic,weak)id<KIPLDelegate>delegate;

@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
- (instancetype)initWithFrame:(CGRect)frame delegate:(id)delegate datasource:(id)datasource pickrVwTag:(int)tag selectedInde:(int)slctedIndex;
- (void)updatePickerView;
@end

@protocol KIPLDelegate <NSObject>

@required
- (void)kiplPickerViewDidDismissWithSelectedRow:(NSInteger)selectedRow PickerVwTag:(NSInteger)tag;

@end
