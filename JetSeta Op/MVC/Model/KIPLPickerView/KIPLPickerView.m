//
//  KIPLPickerView.m
//  Tabco App
//
//  Created by Santosh on 08/02/16.
//  Copyright © 2016 Vaibhav Khatri. All rights reserved.
//

#import "KIPLPickerView.h"

@interface KIPLPickerView ()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *dnBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cnclBtn;


- (IBAction)onCancel:(id)sender;
- (IBAction)onDone:(id)sender;
@end
@implementation KIPLPickerView


- (instancetype)initWithFrame:(CGRect)frame delegate:(id)delegate datasource:(id)datasource pickrVwTag:(int)tag selectedInde:(int)slctedIndex
{
    if (self = [super initWithFrame:frame])
    {
        
        
        NSArray *array=[[NSBundle mainBundle]loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        UIView *theView = array[0];
        theView.frame = frame;
        theView.tag = 100;
        [self addSubview:theView];
        
        self.pickerView.delegate=delegate;
        self.pickerView.dataSource=datasource;
        self.delegate=delegate;
        self.pickerView.tag=tag;
        
        [_dnBtn setTintColor:[UIColor blueColor]];
        [_cnclBtn setTintColor:[UIColor blueColor]];

        [self.pickerView selectRow:slctedIndex inComponent:0 animated:YES];
        self.containerView.transform = CGAffineTransformMakeTranslation(0,self.containerView.frame.size.height);

        self.containerView.alpha = 0.0f;
//        self.containerView.transform = CGAffineTransformMakeScale(0.85, 0.85);
        [UIView animateWithDuration:0.15
                              delay:0
                            options:(7 << 16) // note: this curve ignores durations
                         animations:^{
                             self.containerView.alpha = 1.0;
                             // set transform before frame here...
                             self.containerView.transform = CGAffineTransformIdentity;
                         }
                         completion:^(BOOL finished) {
                             [UIView animateWithDuration:.1 animations:^{
                                 [self.pickerView reloadAllComponents];
//                                 self.containerView.transform = CGAffineTransformIdentity;
                             }];
                         }];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)onCancel:(id)sender {
    [UIView animateWithDuration:0.15
                          delay:0
                        options:(7 << 16) // note: this curve ignores durations
                     animations:^{
                         //                         self.scrollerContentView.alpha = 0.0;
                         self.containerView.alpha = 0.0f;
                         self.alpha=0.0f;
                         // set transform before frame here...
                         self.containerView.transform = CGAffineTransformMakeTranslation(0,self.containerView.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
    
}

- (IBAction)onDone:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(kiplPickerViewDidDismissWithSelectedRow:PickerVwTag:)])
    {
        [self.delegate kiplPickerViewDidDismissWithSelectedRow:[self.pickerView selectedRowInComponent:0]PickerVwTag:self.pickerView.tag];

    }
    
    [UIView animateWithDuration:0.15
                          delay:0
                        options:(7 << 16) // note: this curve ignores durations
                     animations:^{
                         //                         self.scrollerContentView.alpha = 0.0;
                         self.containerView.alpha = 0.0f;
                         self.alpha=0.0f;
                         // set transform before frame here...
                         self.containerView.transform = CGAffineTransformMakeTranslation(0,self.containerView.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

- (void)updatePickerView
{
    [self.pickerView reloadAllComponents];
}

@end
