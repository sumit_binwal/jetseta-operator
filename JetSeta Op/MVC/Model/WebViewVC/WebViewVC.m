//
//  WebViewVC.m
//  JetSeta
//
//  Created by Suchita Bohra on 20/04/16.
//  Copyright © 2016 Suchita Bohra. All rights reserved.
//

#import "WebViewVC.h"
#import "CommonFunction.h"

@interface WebViewVC ()
{
    UIBarButtonItem *btnRightItem;
}

@end

@implementation WebViewVC

@synthesize strNavigationTitle, strUrl;

#pragma mark - ViewLifeCycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   [CommonFunction setNavigationBar:self.navigationController];
    self.navigationItem.title = strNavigationTitle;
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"AL_Menu"]style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    self.navigationItem.title = strNavigationTitle;
    
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    btnRightItem = [[UIBarButtonItem alloc]initWithCustomView:indicator];
    self.navigationItem.rightBarButtonItem = btnRightItem;

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationController.navigationBarHidden = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (![CommonFunction reachabiltyCheck])
    {
        NSString *alertTitle = @"Alert";
        NSString *msgText = @"It seems there is no internet connection available on your device at the moment, try later";
        
        [CommonFunction showAlertWithTitle:alertTitle message:msgText onViewController:self useAsDelegate:NO dismissBlock:^{
        }];
    }
    
    [self setWebViewRequest];
}

#pragma mark - MemoryManagement Method

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UserDefined Methods
-(void)setWebViewRequest
{
    //    self.navigationItem.title = @"About Us";
    
    //    NSString *linkString = [@"https://www.google.co.in/" stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    webViewBrowse.opaque = NO;
    webViewBrowse.backgroundColor = [UIColor whiteColor];
    NSString *linkString = @"";
    
    if (strUrl.length > 0)
    {
        linkString = [strUrl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    else
    {
        linkString = [@"https://www.google.co.in/" stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    
    NSURL *url = [NSURL URLWithString:linkString];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [webViewBrowse loadRequest:request];
    
    self.navigationItem.rightBarButtonItem = btnRightItem;
    [indicator startAnimating];
    
}

#pragma mark - UIWebViewDelegate Methods
//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
//{
//
//}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    self.navigationItem.rightBarButtonItem = btnRightItem;
    [indicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
//    NSString *pageTitle = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.navigationItem.titleView = nil;
    //    if (pageTitle.length > 0)
    //    {
    //        self.navigationItem.title = pageTitle;
    //    }
    //    else
    //    {
    
    //    }
    [indicator stopAnimating];
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [indicator stopAnimating];
    self.navigationItem.rightBarButtonItem = nil;
}

@end
