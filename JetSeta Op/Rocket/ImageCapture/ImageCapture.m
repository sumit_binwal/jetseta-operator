//
//  ImageCapture.m
//  emptyApp
//
//  Created by Suchita on 06/09/14.
//  Copyright (c) 2014 Suchita. All rights reserved.
//

#import "ImageCapture.h"

@interface ImageCapture ()
{
    
    void (^didFinishBlock) (NSDictionary *dictImageInfo);
    void (^didCancelBlock) ();
    
    kSourceType pickerSourceType;
}

@property (nonatomic, copy)void (^didFinishBlock) (NSDictionary *dictImageInfo);

@end
@implementation ImageCapture


- (id) initWithSourceType:(kSourceType)sourceType
{
    self = [super init];
    
    if (self)
    {
        pickerSourceType = sourceType;
    }
    
    return self;
}

- (void) presentImagePickerOnController:(id) controller onDidFinish:(void (^) (NSDictionary *imageInfo)) finish onCancel:(void (^) (void)) cancel{
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    if (finish != nil)
    {
        didFinishBlock = finish;
    }
    if (cancel != nil)
    {
        didCancelBlock = cancel;
    }
    
    switch (pickerSourceType)
    {
        case kSourceTypeCamera:
        {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
               imagePicker.allowsEditing = YES;
        }
            break;
        case kSourceTypeLibrary:
        {
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
               imagePicker.allowsEditing = NO;
        }
            break;
    }
    imagePicker.delegate = self;
 
    [controller presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate Methods Implementation
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    if (didFinishBlock != nil) {
        didFinishBlock (info);
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    if (didCancelBlock != nil) {
        didCancelBlock ();
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}
@end
