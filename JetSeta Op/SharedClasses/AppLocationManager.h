//
//  AppLocationManager.h
//  Tabco App
//
//  Created by Santosh on 21/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface AppLocationManager : NSObject<CLLocationManagerDelegate>

+ (AppLocationManager *)sharedInstance;

@property(nonatomic,strong)CLLocationManager *userLocationManager;

@end
