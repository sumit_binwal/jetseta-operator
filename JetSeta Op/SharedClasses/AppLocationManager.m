//
//  AppLocationManager.m
//  Tabco App
//
//  Created by Santosh on 21/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//

#import "AppLocationManager.h"
#import "AppDelegate.h"
//#import "JetSeta_Constants.h"

@implementation AppLocationManager

+ (AppLocationManager *)sharedInstance
{
    static AppLocationManager *_locationManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _locationManager = [[self alloc]init];
    });
    return _locationManager;
}

- (instancetype)init
{
    if (self = [super init])
    {
        _userLocationManager = [[CLLocationManager alloc]init];
        _userLocationManager.delegate = self;
        if ([_userLocationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [_userLocationManager requestWhenInUseAuthorization];
        }
        [_userLocationManager startUpdatingLocation];
    }
    return self;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", [locations lastObject]);
    CLLocation *loc = [locations lastObject];
    NSLog(@"latitude : %f longitude : %f",loc.coordinate.latitude, loc.coordinate.longitude);
    
    CurrentLatitude = loc.coordinate.latitude;
    CurrentLongitude = loc.coordinate.longitude;
    
    [_userLocationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    NSLog(@"%@",error);
    
    if (status == kCLAuthorizationStatusNotDetermined) {
        NSLog(@"kCLAuthorizationStatusNotDetermined");
    }
    else if (status == kCLAuthorizationStatusRestricted || status == kCLAuthorizationStatusDenied)
    {
        NSLog(@"kCLAuthorizationStatusRestricted");
        NSLog(@"kCLAuthorizationStatusDenied");
        [[AppDelegate getSharedInstance] showLocationEnableAlert];
        
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"locationDenied" object:nil];
    }
    else if (status == kCLAuthorizationStatusAuthorized){
        NSLog(@"kCLAuthorizationStatusAuthorized");
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"locationAllowed" object:nil];
    }
    else if (status == kCLAuthorizationStatusAuthorizedAlways){
        NSLog(@"kCLAuthorizationStatusAuthorizedAlways");
    }
    else if (status == kCLAuthorizationStatusAuthorizedWhenInUse){
        NSLog(@"kCLAuthorizationStatusAuthorizedWhenInUse");
    }
}

@end
