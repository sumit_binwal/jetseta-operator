//
//  UtilityClass.m
//  Tabco App
//
//  Created by Santosh on 17/12/15.
//  Copyright © 2015 Vaibhav Khatri. All rights reserved.
//

#import "UtilityClass.h"
#import "AppDelegate.h"

#pragma mark -
#pragma mark - CLASS IMPLEMENTATION
@implementation UtilityClass

#pragma mark -
#pragma mark String To Image
+ (UIImage *)convertBase64StringToImage:(NSString *)base64String
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:base64String options:NSDataBase64DecodingIgnoreUnknownCharacters];
    UIImage *image = [UIImage imageWithData:data];
    return image;
}

#pragma mark -
#pragma mark Image To String
+ (NSString *)convertImageToBase64String:(UIImage *)image
{
    NSData *data = UIImageJPEGRepresentation(image, .5);
    NSString *base64String = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return base64String;
}

#pragma mark -
#pragma mark UIAlertView
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController useAsDelegate:(BOOL)isDelegate dismissBlock:(void(^)())dismissBlock
{
    
    if (NSClassFromString(@"UIAlertController"))
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:title
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           if (dismissBlock)
                                                           {
                                                               dismissBlock();
                                                           }
                                                           [alertController dismissViewControllerAnimated:YES completion:^{
                                                           }];
                                                           
                                                       }];
        
        [alertController addAction:ok];
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        if (isDelegate)
        {
            [[[UIAlertView alloc]initWithTitle:title message:message delegate:viewController cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        else
        {
            [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
    }
    
}

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message onViewController:(UIViewController *)viewController useAsDelegate:(BOOL)isDelegate withButtonsArray:(NSArray *)buttonsArray dismissBlock:(void(^)(NSInteger buttonIndex))dismissBlock
{
   
    if (NSClassFromString(@"UIAlertController"))
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:title
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        for (NSInteger i=0; i<buttonsArray.count; i++)
        {
            UIAlertAction* ok = [UIAlertAction actionWithTitle:buttonsArray[i] style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           if (dismissBlock)
                                                           {
                                                               dismissBlock(i);
                                                           }
                                                           [alertController dismissViewControllerAnimated:YES completion:^{
                                                           }];
                                                           
                                                       }];
            
            [alertController addAction:ok];
        }
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert;
        if (isDelegate)
        {
            alert=[[UIAlertView alloc]initWithTitle:title message:message delegate:viewController cancelButtonTitle:nil otherButtonTitles:nil];
        }
        else
        {
            alert=[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
        }
        for (NSInteger i=0; i<buttonsArray.count; i++)
        {
            [alert addButtonWithTitle:buttonsArray[i]];
        }
    }
}

#pragma mark -
#pragma mark Save User Info
+ (void)saveUserInfoWithDictionary:(NSDictionary *)dictionary
{
    NSData *userInfoData = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    
    [NSUSERDEFAULTS setObject:userInfoData forKey:@"userinfo"];
    [NSUSERDEFAULTS synchronize];
    
}

#pragma mark -
#pragma mark Get User Info
+ (NSDictionary *)getUserInfoDictionary
{
    NSData *userInfoData = [NSUSERDEFAULTS objectForKey:@"userinfo"];
    NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:userInfoData];
    return dict;
}

#pragma mark -
#pragma mark Delete User Info
+ (void)deleteUserInfoDictionary
{
    [NSUSERDEFAULTS removeObjectForKey:@"userinfo"];
//    [userDefaults removeObjectForKey:@"userImageUrl"];
     [NSUSERDEFAULTS synchronize];
}

#pragma mark -
#pragma mark Image Bytes To Array
+(NSMutableArray *)convertImageBytesToArrayUsingImage:(UIImage *)image
{
    NSData *data = UIImageJPEGRepresentation(image, .3);
    const unsigned char *bytes = [data bytes]; // no need to copy the data
    NSUInteger length = [data length];
    NSMutableArray *byteArray = [NSMutableArray array];
    for (NSUInteger i = 0; i < length; i++) {
        [byteArray addObject:[NSNumber numberWithUnsignedChar:bytes[i]]];
    }
    return byteArray;
}

#pragma mark -
#pragma mark Resize Image
+(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
    CGFloat scale = [[UIScreen mainScreen]scale];
    /*You can remove the below comment if you dont want to scale the image in retina   device .Dont forget to comment UIGraphicsBeginImageContextWithOptions*/
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark -
#pragma mark Country ISD Codes
+ (NSArray *)getCountryCodesWithName
{
    NSString *jsonPath = [[NSBundle mainBundle]pathForResource:@"Countries" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonPath];
    NSDictionary *countryDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    NSArray *countryarray = countryDict[@"countries"];
    NSMutableArray *finalArray = [[NSMutableArray alloc]init];
    [finalArray addObjectsFromArray:countryarray];
//    for (NSInteger i=0; i<countryarray.count; i++)
//    {
//        NSString *name = countryarray[i][@"name"];
//        NSString *code = countryarray[i][@"code"];
//        [finalArray addObject:[NSString stringWithFormat:@"%@ - %@",name,code]];
//    }
    
    return finalArray;
}

#pragma mark -
#pragma mark Email Validation
+(BOOL)IsValidEmail:(NSString *)checkString
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (BOOL)isValueNotEmpty:(NSString*)aString{
    if (aString == nil || [[aString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""] ){
        //[CommonFunctions alertTitle:@"Server Response Error"
        //               withMessage:@"Please try again, server is not responding."];
        return NO;
    }
    return YES;
}

+ (BOOL)isValidMobileNumber:(NSString*)number
{
//    number=[number stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    if ([number length]<5 || [number length]>17) {
//        return FALSE;
//    }
//    NSString *Regex = @"^([0-9]*)$";
//    NSPredicate *mobileTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex];
//    
//    return [mobileTest evaluateWithObject:number];
    NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:number];
    
    BOOL stringIsValid = [numbersOnly isSupersetOfSet:characterSetFromTextField];
    return stringIsValid;
}

@end
